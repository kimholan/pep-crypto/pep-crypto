PEP decryption component
------------------------

- [Documentation](pep-crypto-documentation/README.md)
  - [References](pep-crypto-documentation/10-Reference)
  - [Architecture](pep-crypto-documentation/30-Architecture)
  - [Schema](pep-crypto-documentation/35-Schema)
  - [Design](pep-crypto-documentation/40-Design)
  - [Development](pep-crypto-documentation/50-Development)
  - [Implementation](pep-crypto-documentation/55-Implementation)
  - [Testing](pep-crypto-documentation/60-Test/)

Prerequisites
-------------

Documentation assumes the examples are run using:
- Linux environment
- JDK11
- Online connectivity due to building dependencies with [srcdeps](https://github.com/srcdeps/srcdeps)
  - srcdeps builds the binding generator for the ASN.1-structues which is located at https://gitlab.com/kimholan/pep-crypto/asn1-compiler

Regression tests requires:
- Language set to en_US.utf-8 due to assertions on localized messages, some tests may fail otherwise
  - Language can be set to en_US.utf-8 in a shell, for example by:
      ```bash
      # Language can be configured by setting it for the current shell session 
      export LANG=en_US.utf-8 
      ```

Quick start (Docker)
--------------------

The decryption component is exposes its functionality as a MicroProfile application packaged for the Quarkus runtime
and includes an OpenAPI UI. This is published to the gitlab container registry and can be run as in the snippet below which 
also shows the console output

```bash
# Check the container registry for the available tags, latest is used for example purposes only, Quarkus shows the internal version on startup based on branch and commit sha of the build
$ docker run --rm --network host registry.gitlab.com/kimholan/pep-crypto/pep-crypto/quarkus:latest 
[COMMAND]
__  ____  __  _____   ___  __ ____  ______ 
 --/ __ \/ / / / _ | / _ \/ //_/ / / / __/ 
 -/ /_/ / /_/ / __ |/ , _/ ,< / /_/ /\ \   
--\___\_\____/_/ |_/_/|_/_/|_|\____/___/   
2021-11-07 11:23:00,945 INFO  [io.quarkus] (main) main_fce0dc54260a4630e3fd83932a56b099470cc5c0 on JVM (powered by Quarkus 1.13.3.Final) started in 1.574s. Listening on: http://0.0.0.0:8080
2021-11-07 11:23:00,946 INFO  [io.quarkus] (main) Profile prod activated. 
2021-11-07 11:23:00,946 INFO  [io.quarkus] (main) Installed features: [cdi, hibernate-validator, resteasy, resteasy-jackson, smallrye-openapi, swagger-ui]
```

Navigate to __http://localhost:8080/pep-crypto/api/v1/openapi-ui/index.html__ to view the available endpoints
and example requests.

Quick start (CLI)
-----------------

_Build the project and start it with the Quarkus runtime_

```bash  
mvn clean install -pl ':pep-crypto-quarkus' -am
LANG=en_US.utf-8  mvn -f pep-crypto-quarkus/pom.xml quarkus:dev 
```

_Running the tests (requires installing Gauge)_

See https://docs.gauge.org/getting_started/installing-gauge.html for installing the Gauge runtime.

```
mvn clean install -Pgauge -pl ':pep-crypto-test'  -Dtags='!quarkus'
```

Note:
- The example excludes the Microprofile runtime configuration tests which requires multiple JVM instances 
  by supplying the tag filter '!quarkus' for the specs to run


GitLab CI
---------

The project includes a .gitlab-ci.yml that does the following:
- build the code
- package the pep-crypto quarkus application as a docker image
- run the function regression tests on containers using the quarkus app container images
- push the quarkus app container imge to gitlab container registry

---
