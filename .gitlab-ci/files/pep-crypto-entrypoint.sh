#!/bin/bash
#-----------------------------------------------------------------------------------------------------------------------
# Skip options unless -- was provided as first argument
#-----------------------------------------------------------------------------------------------------------------------
if [[ $# -gt 0 ]] && [[ "$1" != "--"* ]]; then
    exec "$@"
fi

echo "[COMMAND]" ${@}

################################################################################
# quarkus
################################################################################

JAR_FILE=${JAR_FILE:-/quarkus-run.jar}
JAVA_HOME=${JAVA_HOME:-/usr/lib/jvm/default-java}
JAVA_OPTS=${JAVA_OPTS:--XX:+UseParallelGC -XX:+UseParallelOldGC -XX:-TieredCompilation -XX:ActiveProcessorCount=2 -Xms128M -Xmx384M -Djava.net.preferIPv4Stack=true -Dquarkus.http.port=8080}
JAR_OPTS=${JAR_OPTS:-}
PATH=${JAVA_HOME}/bin:${PATH}


if [[ " $@ " == *" jacoco "* ]];
then
    export JAVA_TOOL_OPTIONS="-javaagent:/jacocoagent.jar=includes=nl.logius.pepcrypto.*,output=tcpserver,address=*"
fi


if  [[ " $@ " == *" quarkus-max-post-size "* ]];
then
    JAVA_OPTS+=" -Dquarkus.http.limits.max-body-size=1"

    exec ${JAVA_HOME}/bin/java ${JAVA_OPTS} -jar ${JAR_FILE} ${JAR_OPTS}
fi


if [[  $# -eq 0 ]] || [[ " $@ " == *" quarkus "* ]];
then
    exec ${JAVA_HOME}/bin/java ${JAVA_OPTS} -jar ${JAR_FILE} ${JAR_OPTS}
fi
