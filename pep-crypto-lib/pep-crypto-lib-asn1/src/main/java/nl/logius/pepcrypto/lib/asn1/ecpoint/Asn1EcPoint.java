package nl.logius.pepcrypto.lib.asn1.ecpoint;

public interface Asn1EcPoint {

    byte[] getValue();

}
