package nl.logius.pepcrypto.lib.asn1.signedencryptedpseudonym;

import generated.asn1.EncryptedPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.encryptedpseudonym.Asn1EncryptedPseudonym;

public interface Asn1SignedEncryptedPseudonymSignedBody
        extends Asn1SignedBody {

    EncryptedPseudonym getEncryptedPseudonym();

    @Override
    default Asn1EncryptedPseudonym asn1Body() {
        return getEncryptedPseudonym();
    }

}
