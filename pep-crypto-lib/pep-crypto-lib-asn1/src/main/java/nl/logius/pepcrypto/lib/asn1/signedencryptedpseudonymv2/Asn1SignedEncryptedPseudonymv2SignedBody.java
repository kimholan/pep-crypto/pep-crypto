package nl.logius.pepcrypto.lib.asn1.signedencryptedpseudonymv2;

import generated.asn1.EncryptedPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyExtraElements;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyIssuance;
import nl.logius.pepcrypto.lib.asn1.encryptedpseudonym.Asn1EncryptedPseudonym;

public interface Asn1SignedEncryptedPseudonymv2SignedBody
        extends Asn1SignedBody,
                Asn1SignedBodyIssuance,
                Asn1SignedBodyExtraElements {

    EncryptedPseudonym getEncryptedPseudonym();

    @Override
    default Asn1EncryptedPseudonym asn1Body() {
        return getEncryptedPseudonym();
    }

}
