package nl.logius.pepcrypto.lib.asn1.directencryptedpseudonym;

import generated.asn1.DirectEncryptedPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1Body;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1DirectEncryptedPseudonym
        extends Asn1Body, Asn1Pseudonym {

    static Asn1DirectEncryptedPseudonym fromByteArray(byte[] bytes) {
        var asn1 = new DirectEncryptedPseudonym();

        PEP_SCHEMA_ASN1_DECODE.call(asn1::decodeByteArray, bytes);

        return asn1;
    }

    String getAuthorizedParty();

    default String asn1AuthorizedParty() {
        return getAuthorizedParty();
    }

}
