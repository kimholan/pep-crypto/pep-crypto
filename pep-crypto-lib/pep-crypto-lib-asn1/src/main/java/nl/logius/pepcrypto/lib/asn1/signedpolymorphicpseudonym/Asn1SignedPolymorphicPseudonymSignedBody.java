package nl.logius.pepcrypto.lib.asn1.signedpolymorphicpseudonym;

import generated.asn1.PolymorphicPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.polymorphicpseudnoym.Asn1PolymorphicPseudonym;

public interface Asn1SignedPolymorphicPseudonymSignedBody
        extends Asn1SignedBody {

    PolymorphicPseudonym getPolymorphicPseudonym();

    @Override
    default Asn1PolymorphicPseudonym asn1Body() {
        return getPolymorphicPseudonym();
    }

}
