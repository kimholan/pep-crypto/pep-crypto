package nl.logius.pepcrypto.lib.asn1.directencryptedidentity;

import generated.asn1.DirectEncryptedIdentityv2;
import nl.logius.pepcrypto.lib.asn1.Asn1Body;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1DirectEncryptedIdentity
        extends Asn1Body {

    static Asn1DirectEncryptedIdentity fromByteArray(byte[] bytes) {
        var asn1 = new DirectEncryptedIdentityv2();

        PEP_SCHEMA_ASN1_DECODE.call(asn1::decodeByteArray, bytes);

        return asn1;
    }

}
