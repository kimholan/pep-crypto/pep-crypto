package nl.logius.pepcrypto.lib.asn1.signedpolymorphicidentityv2;

import generated.asn1.PolymorphicIdentity;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyIssuance;
import nl.logius.pepcrypto.lib.asn1.polymorphicidentity.Asn1PolymorphicIdentity;

public interface Asn1SignedPolymorphicIdentityv2SignedBody
        extends Asn1SignedBody,
                Asn1SignedBodyIssuance {

    PolymorphicIdentity getPolymorphicIdentity();

    @Override
    default Asn1PolymorphicIdentity asn1Body() {
        return getPolymorphicIdentity();
    }

}
