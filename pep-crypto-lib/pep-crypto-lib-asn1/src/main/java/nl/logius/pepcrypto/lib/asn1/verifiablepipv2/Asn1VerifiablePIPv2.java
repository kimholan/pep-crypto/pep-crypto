package nl.logius.pepcrypto.lib.asn1.verifiablepipv2;

import generated.asn1.SignedPIPv2;
import generated.asn1.VerifiablePIPv2;
import generated.asn1.VerifiablePIPv2ProofOfConformity;
import nl.logius.pepcrypto.lib.asn1.Asn1PepType;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1VerifiablePIPv2
        extends Asn1PepType {

    static Asn1VerifiablePIPv2 fromByteArray(byte[] bytes) {
        var verifiablePip = new VerifiablePIPv2();

        PEP_SCHEMA_ASN1_DECODE.call(verifiablePip::decodeByteArray, bytes);

        return verifiablePip;
    }

    SignedPIPv2 getSignedPIP();

    VerifiablePIPv2ProofOfConformity getProofOfConformity();

}
