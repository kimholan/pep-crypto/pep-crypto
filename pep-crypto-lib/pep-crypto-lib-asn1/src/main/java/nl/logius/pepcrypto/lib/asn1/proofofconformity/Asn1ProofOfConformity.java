package nl.logius.pepcrypto.lib.asn1.proofofconformity;

import generated.asn1.ECPoint;

public interface Asn1ProofOfConformity {

    Asn1ProofOfConformityZp1 getZp1();

    Asn1ProofOfConformityZp2 getZp2();

    ECPoint getP1();

    ECPoint getT();

}
