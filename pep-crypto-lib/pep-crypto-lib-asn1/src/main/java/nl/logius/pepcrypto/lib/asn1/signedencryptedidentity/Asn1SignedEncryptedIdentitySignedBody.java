package nl.logius.pepcrypto.lib.asn1.signedencryptedidentity;

import generated.asn1.EncryptedIdentity;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.encryptedidentity.Asn1EncryptedIdentity;

public interface Asn1SignedEncryptedIdentitySignedBody
        extends Asn1SignedBody {

    EncryptedIdentity getEncryptedIdentity();

    @Override
    default Asn1EncryptedIdentity asn1Body() {
        return getEncryptedIdentity();
    }

}
