package nl.logius.pepcrypto.lib.asn1.signedpipv2;

import generated.asn1.ECDSASignature;
import generated.asn1.SignedPIPv2;
import generated.asn1.SignedPIPv2SignedPIP;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1Signature;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1SignedPIPv2Envelope
        extends Asn1Envelope {

    static Asn1SignedPIPv2Envelope fromByteArray(byte[] bytes) {
        var pepSignedPolymorphic = new SignedPIPv2();

        PEP_SCHEMA_ASN1_DECODE.call(pepSignedPolymorphic::decodeByteArray, bytes);

        return pepSignedPolymorphic;
    }

    SignedPIPv2SignedPIP getSignedPIP();

    ECDSASignature getSignatureValue();

    @Override
    default Asn1SignedPIPv2SignedBody asn1SignedBody() {
        return getSignedPIP();
    }

    @Override
    default Asn1Signature asn1Signature() {
        return getSignatureValue();
    }

}

