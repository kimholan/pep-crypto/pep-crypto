package nl.logius.pepcrypto.lib.asn1.signedpolymorphicpseudonymv2;

import generated.asn1.ECDSASignature;
import generated.asn1.SignedPolymorphicPseudonymv2;
import generated.asn1.SignedPolymorphicPseudonymv2SignedPP;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1Signature;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1SignedPolymorphicPseudonymv2Envelope
        extends Asn1Envelope {

    static Asn1SignedPolymorphicPseudonymv2Envelope fromByteArray(byte[] bytes) {
        var pepSignedPolymorphic = new SignedPolymorphicPseudonymv2();

        PEP_SCHEMA_ASN1_DECODE.call(pepSignedPolymorphic::decodeByteArray, bytes);

        return pepSignedPolymorphic;
    }

    SignedPolymorphicPseudonymv2SignedPP getSignedPP();

    ECDSASignature getSignatureValue();

    @Override
    default Asn1SignedPolymorphicPseudonymv2SignedBody asn1SignedBody() {
        return getSignedPP();
    }

    @Override
    default Asn1Signature asn1Signature() {
        return getSignatureValue();
    }

}

