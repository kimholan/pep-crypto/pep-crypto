package nl.logius.pepcrypto.lib.asn1.polymorphicidentity;

import generated.asn1.PolymorphicIdentity;
import nl.logius.pepcrypto.lib.asn1.Asn1Body;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1PolymorphicIdentity
        extends Asn1Body {

    static Asn1PolymorphicIdentity fromByteArray(byte[] bytes) {
        var asn1 = new PolymorphicIdentity();

        PEP_SCHEMA_ASN1_DECODE.call(asn1::decodeByteArray, bytes);

        return asn1;
    }

}
