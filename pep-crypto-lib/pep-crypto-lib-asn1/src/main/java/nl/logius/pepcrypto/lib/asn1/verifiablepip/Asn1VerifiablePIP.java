package nl.logius.pepcrypto.lib.asn1.verifiablepip;

import generated.asn1.SignedPIP;
import generated.asn1.VerifiablePIP;
import generated.asn1.VerifiablePIPProofOfConformity;
import nl.logius.pepcrypto.lib.asn1.Asn1PepType;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1VerifiablePIP
        extends Asn1PepType {

    static Asn1VerifiablePIP fromByteArray(byte[] bytes) {
        var verifiablePip = new VerifiablePIP();

        PEP_SCHEMA_ASN1_DECODE.call(verifiablePip::decodeByteArray, bytes);

        return verifiablePip;
    }

    SignedPIP getSignedPIP();

    VerifiablePIPProofOfConformity getProofOfConformity();

}
