package nl.logius.pepcrypto.lib.asn1.signedpolymorphicpseudonymv2;

import generated.asn1.PolymorphicPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyIssuance;
import nl.logius.pepcrypto.lib.asn1.polymorphicpseudnoym.Asn1PolymorphicPseudonym;

public interface Asn1SignedPolymorphicPseudonymv2SignedBody
        extends Asn1SignedBody,
                Asn1SignedBodyIssuance {

    PolymorphicPseudonym getPolymorphicPseudonym();

    @Override
    default Asn1PolymorphicPseudonym asn1Body() {
        return getPolymorphicPseudonym();
    }

}
