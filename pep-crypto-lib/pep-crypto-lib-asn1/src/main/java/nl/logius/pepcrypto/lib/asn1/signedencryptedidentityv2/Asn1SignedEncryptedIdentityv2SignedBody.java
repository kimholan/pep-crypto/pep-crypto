package nl.logius.pepcrypto.lib.asn1.signedencryptedidentityv2;

import generated.asn1.EncryptedIdentity;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyExtraElements;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyIssuance;
import nl.logius.pepcrypto.lib.asn1.encryptedidentity.Asn1EncryptedIdentity;

public interface Asn1SignedEncryptedIdentityv2SignedBody
        extends Asn1SignedBody,
                Asn1SignedBodyIssuance,
                Asn1SignedBodyExtraElements {

    EncryptedIdentity getEncryptedIdentity();

    @Override
    default Asn1EncryptedIdentity asn1Body() {
        return getEncryptedIdentity();
    }

}
