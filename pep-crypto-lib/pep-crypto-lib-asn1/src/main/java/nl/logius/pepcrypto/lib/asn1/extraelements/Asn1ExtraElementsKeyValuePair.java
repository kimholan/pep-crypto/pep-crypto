package nl.logius.pepcrypto.lib.asn1.extraelements;

import generated.asn1.VariableValueType;

public interface Asn1ExtraElementsKeyValuePair {

    String getKey();

    VariableValueType getValue();

    default Object asn1Value() {
        return getValue().asn1Value();
    }

}
