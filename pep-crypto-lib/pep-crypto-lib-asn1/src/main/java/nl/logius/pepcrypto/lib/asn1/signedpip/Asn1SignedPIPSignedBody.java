package nl.logius.pepcrypto.lib.asn1.signedpip;

import generated.asn1.PIP;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.pip.Asn1Pip;

public interface Asn1SignedPIPSignedBody
        extends Asn1SignedBody {

    PIP getPip();

    @Override
    default Asn1Pip asn1Body() {
        return getPip();
    }

}

