package nl.logius.pepcrypto.lib.asn1;

public interface Asn1SignedBodyIssuance {

    String getIssuanceDate();

}
