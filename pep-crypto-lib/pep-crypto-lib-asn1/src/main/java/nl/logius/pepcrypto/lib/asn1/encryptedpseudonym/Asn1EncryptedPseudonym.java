package nl.logius.pepcrypto.lib.asn1.encryptedpseudonym;

import generated.asn1.EncryptedPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1Body;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1EncryptedPseudonym
        extends Asn1Body, Asn1Pseudonym {

    static Asn1EncryptedPseudonym fromByteArray(byte[] bytes) {
        var asn1 = new EncryptedPseudonym();

        PEP_SCHEMA_ASN1_DECODE.call(asn1::decodeByteArray, bytes);

        return asn1;
    }

}
