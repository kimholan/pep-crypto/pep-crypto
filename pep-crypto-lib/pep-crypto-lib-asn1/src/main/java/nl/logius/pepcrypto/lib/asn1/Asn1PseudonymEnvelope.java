package nl.logius.pepcrypto.lib.asn1;

import generated.asn1.Diversifier;
import nl.logius.pepcrypto.lib.asn1.diversifier.Asn1Diversifier;

import java.math.BigInteger;
import java.util.Optional;

public interface Asn1PseudonymEnvelope
        extends Asn1Envelope {

    Asn1Pseudonym asn1Pseudonym();

    default BigInteger asn1PseudonymType() {
        return asn1Pseudonym().getType();
    }

    default Diversifier asn1PseudonymDiversifier() {
        return asn1Pseudonym().getDiversifier();
    }

    @Override
    default Asn1RecipientKeyId asn1RecipientKeyId() {
        var recipientKeyId = asn1Body().asn1RecipientKeyId();
        var diversifierString = Optional.ofNullable(asn1PseudonymDiversifier())
                                        .map(Asn1Diversifier::asn1DiversifierString)
                                        .orElse(null);
        return recipientKeyId.diversifier(diversifierString);
    }

}
