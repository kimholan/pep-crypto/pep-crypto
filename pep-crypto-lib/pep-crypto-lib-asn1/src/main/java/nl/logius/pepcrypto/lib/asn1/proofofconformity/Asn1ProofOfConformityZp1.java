package nl.logius.pepcrypto.lib.asn1.proofofconformity;

import nl.logius.pepcrypto.lib.crypto.PepEcSignature;

import java.math.BigInteger;

public interface Asn1ProofOfConformityZp1
        extends PepEcSignature {

    BigInteger getR1();

    BigInteger getS1();

    @Override
    default BigInteger getR() {
        return getR1();
    }

    @Override
    default BigInteger getS() {
        return getS1();
    }

}
