package nl.logius.pepcrypto.lib.asn1.migrationintermediarypseudonym;

import generated.asn1.MigrationIntermediaryPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1PepType;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1RecipientKeyId;
import nl.logius.pepcrypto.lib.asn1.decryptedpseudonym.Asn1DecryptedPseudonym;
import nl.logius.pepcrypto.lib.asn1.diversifier.Asn1Diversifier;
import nl.logius.pepcrypto.lib.crypto.PepCrypto;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.PepSchemeKeySetId;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.util.Optional;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1MigrationIntermediaryPseudonym
        extends PepSchemeKeySetId, Asn1Pseudonym, Asn1PepType {

    static Asn1MigrationIntermediaryPseudonym fromByteArray(byte[] bytes) {
        var pepPseudonym = new MigrationIntermediaryPseudonym();

        PEP_SCHEMA_ASN1_DECODE.call(pepPseudonym::decodeByteArray, bytes);

        return pepPseudonym;
    }

    static Asn1MigrationIntermediaryPseudonym forMigration(Asn1DecryptedPseudonym source, ECPoint targetEcPoint, PepRecipientKeyId targetMigrant, String migrationId) {
        var pseudonymValue = PepCrypto.encodeEcPointAsBase64(targetEcPoint);
        var returnValue = new MigrationIntermediaryPseudonym();
        returnValue.setMigrationID(migrationId);
        returnValue.setPseudonymValue(pseudonymValue);

        forMigrationSource(returnValue, source);
        forMigrationTarget(returnValue, targetMigrant);

        return returnValue;
    }

    private static void forMigrationSource(MigrationIntermediaryPseudonym returnValue, Asn1DecryptedPseudonym source) {
        returnValue.setDiversifier(source.getDiversifier());
        returnValue.setSchemeVersion(source.getSchemeVersion());
        returnValue.setSchemeKeySetVersion(source.getSchemeKeySetVersion());
        returnValue.setSource(source.getRecipient());
        returnValue.setSourceKeySetVersion(source.getRecipientKeySetVersion());
        returnValue.setType(source.getType());
    }

    private static void forMigrationTarget(MigrationIntermediaryPseudonym returnValue, PepRecipientKeyId targetMigrant) {
        returnValue.setTarget(targetMigrant.getRecipient());
        returnValue.setTargetKeySetVersion(targetMigrant.getRecipientKeySetVersion());
    }

    default Asn1RecipientKeyId asTargetAsn1RecipientKeyId() {
        var diversifier = Optional.of(this)
                                  .map(Asn1MigrationIntermediaryPseudonym::getDiversifier)
                                  .map(Asn1Diversifier::asn1DiversifierString)
                                  .orElse(null);

        return new Asn1RecipientKeyId(this, getTarget(), getTargetKeySetVersion())
                       .diversifier(diversifier)
                       .migrationId(getMigrationID());
    }

    String getSource();

    BigInteger getSourceKeySetVersion();

    String getTarget();

    BigInteger getTargetKeySetVersion();

    String getMigrationID();

    String getPseudonymValue();

}

