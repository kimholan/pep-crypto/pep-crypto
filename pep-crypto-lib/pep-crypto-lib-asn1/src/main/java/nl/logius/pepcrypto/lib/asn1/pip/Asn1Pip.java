package nl.logius.pepcrypto.lib.asn1.pip;

import generated.asn1.PIP;
import nl.logius.pepcrypto.lib.asn1.Asn1Body;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1Pip
        extends Asn1Body {

    static Asn1Pip fromByteArray(byte[] bytes) {
        var asn1 = new PIP();

        PEP_SCHEMA_ASN1_DECODE.call(asn1::decodeByteArray, bytes);

        return asn1;
    }

}
