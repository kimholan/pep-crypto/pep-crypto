package nl.logius.pepcrypto.lib.asn1.diversifier;

public interface Asn1DiversifierKeyValuePair {

    String getKey();

    String getValue();

}
