package nl.logius.pepcrypto.lib.asn1.signeddirectencryptedidentityv2;

import generated.asn1.DirectEncryptedIdentityv2;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyExtraElements;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyIssuance;
import nl.logius.pepcrypto.lib.asn1.directencryptedidentity.Asn1DirectEncryptedIdentity;

public interface Asn1SignedDirectEncryptedIdentityv2SignedBody
        extends Asn1SignedBody,
                Asn1SignedBodyIssuance,
                Asn1SignedBodyExtraElements {

    DirectEncryptedIdentityv2 getDirectEncryptedIdentity();

    @Override
    default Asn1DirectEncryptedIdentity asn1Body() {
        return getDirectEncryptedIdentity();
    }

}
