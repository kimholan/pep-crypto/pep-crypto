package nl.logius.pepcrypto.lib.asn1.signedpip;

import generated.asn1.ECDSASignature;
import generated.asn1.SignedPIP;
import generated.asn1.SignedPIPSignedPIP;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1Signature;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1SignedPIPEnvelope
        extends Asn1Envelope {

    static Asn1SignedPIPEnvelope fromByteArray(byte[] bytes) {
        var pepSignedPolymorphic = new SignedPIP();

        PEP_SCHEMA_ASN1_DECODE.call(pepSignedPolymorphic::decodeByteArray, bytes);

        return pepSignedPolymorphic;
    }

    SignedPIPSignedPIP getSignedPIP();

    ECDSASignature getSignatureValue();

    @Override
    default Asn1SignedPIPSignedBody asn1SignedBody() {
        return getSignedPIP();
    }

    @Override
    default Asn1Signature asn1Signature() {
        return getSignatureValue();
    }

}

