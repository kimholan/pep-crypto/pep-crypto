package nl.logius.pepcrypto.lib.asn1.encryptedidentity;

import generated.asn1.EncryptedIdentity;
import nl.logius.pepcrypto.lib.asn1.Asn1Body;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1EncryptedIdentity
        extends Asn1Body {

    static Asn1EncryptedIdentity fromByteArray(byte[] bytes) {
        var asn1 = new EncryptedIdentity();

        PEP_SCHEMA_ASN1_DECODE.call(asn1::decodeByteArray, bytes);

        return asn1;
    }

}
