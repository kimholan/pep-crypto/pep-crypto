package nl.logius.pepcrypto.lib.asn1;

import nl.logius.pepcrypto.lib.crypto.PepEcPointTriplet;
import nl.logius.pepcrypto.lib.crypto.PepSchemeKeySetId;

import java.util.List;

public interface Asn1Envelope
        extends Asn1PepType {

    Asn1SignedBody asn1SignedBody();

    Asn1Signature asn1Signature();

    /**
     * Data signed by the signature in the envelope.
     */
    default byte[] asn1SignedData() {
        return asn1SignedBody().getRawInput();
    }

    default Asn1Body asn1Body() {
        return asn1SignedBody().asn1Body();
    }

    default byte[] asn1AuditElement() {
        return asn1SignedBody().getAuditElement();
    }

    default String asn1BodyOid() {
        return asn1Body().asn1Oid();
    }

    default PepSchemeKeySetId pepSchemeKeySetId() {
        return asn1RecipientKeyId();
    }

    default Asn1RecipientKeyId asn1RecipientKeyId() {
        return asn1Body().asn1RecipientKeyId();
    }

    default String asn1SignatureOid() {
        return asn1Signature().asn1SignatureOid();
    }

    default PepEcPointTriplet asn1EcPointTriplet() {
        return asn1Body();
    }

    default List<byte[]> asn1EcPointValues() {
        return asn1Body().asn1EcPointValues();
    }

}
