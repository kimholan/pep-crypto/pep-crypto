package nl.logius.pepcrypto.lib.asn1.signedpolymorphicidentity;

import generated.asn1.PolymorphicIdentity;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.polymorphicidentity.Asn1PolymorphicIdentity;

public interface Asn1SignedPolymorphicIdentitySignedBody
        extends Asn1SignedBody {

    PolymorphicIdentity getPolymorphicIdentity();

    @Override
    default Asn1PolymorphicIdentity asn1Body() {
        return getPolymorphicIdentity();
    }

}
