package nl.logius.pepcrypto.lib.asn1.signedpipv2;

import generated.asn1.PIP;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyIssuance;
import nl.logius.pepcrypto.lib.asn1.pip.Asn1Pip;

public interface Asn1SignedPIPv2SignedBody
        extends Asn1SignedBody,
                Asn1SignedBodyIssuance {

    PIP getPip();

    @Override
    default Asn1Pip asn1Body() {
        return getPip();
    }

}

