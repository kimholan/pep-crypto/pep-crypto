package nl.logius.pepcrypto.lib.asn1.signedpolymorphicidentityv2;

import generated.asn1.ECDSASignature;
import generated.asn1.SignedPolymorphicIdentityv2;
import generated.asn1.SignedPolymorphicIdentityv2SignedPI;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1Signature;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1SignedPolymorphicIdentityv2Envelope
        extends Asn1Envelope {

    static Asn1SignedPolymorphicIdentityv2Envelope fromByteArray(byte[] bytes) {
        var pepSignedPolymorphic = new SignedPolymorphicIdentityv2();

        PEP_SCHEMA_ASN1_DECODE.call(pepSignedPolymorphic::decodeByteArray, bytes);

        return pepSignedPolymorphic;
    }

    SignedPolymorphicIdentityv2SignedPI getSignedPI();

    ECDSASignature getSignatureValue();

    @Override
    default Asn1SignedPolymorphicIdentityv2SignedBody asn1SignedBody() {
        return getSignedPI();
    }

    @Override
    default Asn1Signature asn1Signature() {
        return getSignatureValue();
    }

}

