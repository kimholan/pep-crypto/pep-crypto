package nl.logius.pepcrypto.lib.asn1;

public interface Asn1DirectPseudonymEnvelope
        extends Asn1PseudonymEnvelope, Asn1DirectEncrypted {

}
