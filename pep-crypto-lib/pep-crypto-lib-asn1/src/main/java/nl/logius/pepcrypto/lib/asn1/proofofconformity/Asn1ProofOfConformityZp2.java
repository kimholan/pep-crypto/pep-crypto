package nl.logius.pepcrypto.lib.asn1.proofofconformity;

import nl.logius.pepcrypto.lib.crypto.PepEcSignature;

import java.math.BigInteger;

public interface Asn1ProofOfConformityZp2
        extends PepEcSignature {

    BigInteger getR2();

    BigInteger getS2();

    @Override
    default BigInteger getR() {
        return getR2();
    }

    @Override
    default BigInteger getS() {
        return getS2();
    }

}
