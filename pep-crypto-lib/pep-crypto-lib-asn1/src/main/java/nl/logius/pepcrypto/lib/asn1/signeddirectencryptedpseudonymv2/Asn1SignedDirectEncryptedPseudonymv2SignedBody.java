package nl.logius.pepcrypto.lib.asn1.signeddirectencryptedpseudonymv2;

import generated.asn1.DirectEncryptedPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectEncrypted;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBody;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyExtraElements;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyIssuance;
import nl.logius.pepcrypto.lib.asn1.directencryptedpseudonym.Asn1DirectEncryptedPseudonym;

import java.math.BigInteger;

public interface Asn1SignedDirectEncryptedPseudonymv2SignedBody
        extends Asn1SignedBody,
                Asn1DirectEncrypted,
                Asn1SignedBodyIssuance,
                Asn1SignedBodyExtraElements {

    DirectEncryptedPseudonym getDirectEncryptedPseudonym();

    BigInteger getSigningKeyVersion();

    @Override
    default Asn1DirectEncryptedPseudonym asn1Body() {
        return getDirectEncryptedPseudonym();
    }

    @Override
    default BigInteger asn1SigningKeyVersion() {
        return getSigningKeyVersion();
    }

    @Override
    default String asn1AuthorizedParty() {
        return asn1Body().asn1AuthorizedParty();
    }

}
