package nl.logius.pepcrypto.lib.asn1;

import generated.asn1.ExtraElements;

import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Map.entry;
import static java.util.stream.Collectors.toList;

public interface Asn1SignedBodyExtraElements {

    ExtraElements getExtraElements();

    default List<Map.Entry<String, Object>> asn1ExtraElementsEntryList() { // ???
        return Optional.ofNullable(getExtraElements())
                       .map(ExtraElements::getExtraelementskeyvaluepair)
                       .stream().flatMap(Collection::stream)
                       .map(it -> entry(it.getKey(), it.asn1Value()))
                       .collect(toList());
    }

    default List<String> getSameIds() {
        return asn1ExtraElementsEntryList()
                       .stream()
                       .filter(it -> "SameID".equals(it.getKey())) // 0..n
                       .map(Map.Entry::getValue)
                       .filter(byte[].class::isInstance)
                       .map(byte[].class::cast)
                       .map(Base64.getEncoder()::encodeToString)
                       .collect(Collectors.toList());
    }

}
