package nl.logius.pepcrypto.lib.asn1.polymorphicpseudnoym;

import generated.asn1.PolymorphicPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1Body;

import static nl.logius.pepcrypto.lib.asn1.Asn1ExceptionDetail.PEP_SCHEMA_ASN1_DECODE;

public interface Asn1PolymorphicPseudonym
        extends Asn1Body {

    static Asn1PolymorphicPseudonym fromByteArray(byte[] bytes) {
        var asn1 = new PolymorphicPseudonym();

        PEP_SCHEMA_ASN1_DECODE.call(asn1::decodeByteArray, bytes);

        return asn1;
    }

}
