package nl.logius.pepcrypto.lib.crypto.key;

import java.math.BigInteger;

public interface PepEpDecryptionPrivateKey
        extends PepPrivateKey {

    static PepEpDecryptionPrivateKey epDecryptionPrivateKey(BigInteger value) {
        return () -> value;
    }

}
