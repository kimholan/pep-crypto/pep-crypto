package nl.logius.pepcrypto.lib.crypto.key;

import java.math.BigInteger;

public interface PepEpMigrationSourcePrivateKey
        extends PepPrivateKey {

    static PepEpMigrationSourcePrivateKey epMigrationSourcePrivateKey(BigInteger value) {
        return () -> value;
    }

}
