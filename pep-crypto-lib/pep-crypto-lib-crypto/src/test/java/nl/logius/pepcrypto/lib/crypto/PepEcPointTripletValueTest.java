package nl.logius.pepcrypto.lib.crypto;

import nl.logius.pepcrypto.lib.TestPepEcPointTriplets;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PepEcPointTripletValueTest {

    /**
     * EC-Point coordinates for a well-known SEI.
     * Does not assert the encoded form, only the coordinate values.
     */
    @Test
    public void signedEncryptedIdentityEcPointCoordinates() {
        var ecPointTriplet = TestPepEcPointTriplets.fromSignedEncryptedIdentityResource("/v1/happy_flow/ei02.asn1");
        String[] expected = {
                "(664b77f95e5df0c8b4e9218909a8391e66abed6e707b84be5417b06cd349f94989ef3f3c13022fb4,74e498f34b3c4b01ae6044bec56baea594da140e78768f571fa72fede8ab94caba6c0422b22408bc,1,3ee30b568fbab0f883ccebd46d3f3bb8a2a73513f5eb79da66190eb085ffa9f492f375a97d860eb4)",
                "(33156201cd386cbfd75edd1694987a28d03fef810b84a6e133bab0d97ede2a85c259aaef3c9bd575,b36a9829404c295056673ce1ea817ad8241a558688b22d256651d4b0fd2bdcd428f744d3540f1dd2,1,3ee30b568fbab0f883ccebd46d3f3bb8a2a73513f5eb79da66190eb085ffa9f492f375a97d860eb4)",
                "(6086fab048b9bca4d235f1fa11f226a98c685621be92c5eb6245de9ad223dd0dc802acc744b5ddda,73744fd617140eb43ed38fdfddc216875280df91a54e6e806cbcbd9a28dc4eeb1c99ca3ee8c5e605,1,3ee30b568fbab0f883ccebd46d3f3bb8a2a73513f5eb79da66190eb085ffa9f492f375a97d860eb4)",
        };

        assertEquals(expected[0], ecPointTriplet.getEcPointA().toString());
        assertEquals(expected[1], ecPointTriplet.getEcPointB().toString());
        assertEquals(expected[2], ecPointTriplet.getEcPointC().toString());
    }

}
