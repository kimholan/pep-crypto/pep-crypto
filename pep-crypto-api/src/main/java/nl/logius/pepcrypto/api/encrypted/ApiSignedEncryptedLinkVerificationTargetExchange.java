package nl.logius.pepcrypto.api.encrypted;

import nl.logius.pepcrypto.api.ApiExchange;
import nl.logius.pepcrypto.api.oid.ApiOID;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;

public interface ApiSignedEncryptedLinkVerificationTargetExchange
        extends ApiExchange<Asn1Envelope> {

    boolean isLinked();

    void setLinked(boolean contains);

    byte[] getEncryptedHash();

    void setEncryptedHash(byte[] linkTargetHashBytes);

    ApiOID getApiOIDType();

}
