package nl.logius.pepcrypto.api.encrypted;

import nl.logius.pepcrypto.api.ApiDecryptedPseudonymResult;
import nl.logius.pepcrypto.api.ApiDirectEncryptedPseudonymDecryptable;
import nl.logius.pepcrypto.api.ApiExchange;
import nl.logius.pepcrypto.api.ApiSignatureVerifiable;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectPseudonymEnvelope;
import nl.logius.pepcrypto.lib.crypto.PepPublicVerificationKey;

/**
 * Input/output parameters for processing SignedDirectEncryptedPseudonym.
 */
public interface ApiSignedDirectEncryptedPseudonymExchange
        extends ApiExchange<Asn1DirectPseudonymEnvelope>,
                ApiSignatureVerifiable<PepPublicVerificationKey>,
                ApiDirectEncryptedPseudonymDecryptable,
                ApiDecryptedPseudonymResult {

}

