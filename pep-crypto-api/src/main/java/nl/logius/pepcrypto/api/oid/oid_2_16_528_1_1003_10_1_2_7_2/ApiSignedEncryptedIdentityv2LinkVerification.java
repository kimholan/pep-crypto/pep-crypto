package nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_7_2;

import nl.logius.pepcrypto.api.ApiAsn1Mapper;
import nl.logius.pepcrypto.api.ApiSignatureVerifier;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationExchange;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationService;
import nl.logius.pepcrypto.api.oid.ApiOID;
import nl.logius.pepcrypto.api.oid.ApiOID.Literal;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1IdentityEnvelope;
import nl.logius.pepcrypto.lib.asn1.Asn1PseudonymEnvelope;
import nl.logius.pepcrypto.lib.crypto.PepEcSchnorrVerificationKey;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Map;

import static nl.logius.pepcrypto.api.ApiLinkVerifiable.verifyLink;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_0_4_0_127_0_7_1_1_4_4_3;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_7_2;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_8_2;

/**
 * SignedEncryptedIdentity-v2 - Link verification.
 */
@ApplicationScoped
@ApiOID(OID_2_16_528_1_1003_10_1_2_7_2)
public class ApiSignedEncryptedIdentityv2LinkVerification
        implements ApiSignedEncryptedLinkVerificationService {

    @Inject
    @ApiOID(OID_2_16_528_1_1003_10_1_2_7_2)
    private ApiAsn1Mapper<Asn1IdentityEnvelope> seiv2Mapper;

    @Inject
    @ApiOID(OID_2_16_528_1_1003_10_1_2_8_2)
    private ApiAsn1Mapper<Asn1PseudonymEnvelope> sepv2Mapper;

    @Inject
    @ApiOID(OID_0_4_0_127_0_7_1_1_4_4_3)
    private ApiSignatureVerifier<PepEcSchnorrVerificationKey> signatureVerifier;

    private Map<ApiOID, ApiAsn1Mapper<? extends Asn1Envelope>> linkVerificationTargetMappers;

    @PostConstruct
    void postConstruct() {
        linkVerificationTargetMappers = Map.of(
                Literal.of(OID_2_16_528_1_1003_10_1_2_7_2), seiv2Mapper,
                Literal.of(OID_2_16_528_1_1003_10_1_2_8_2), sepv2Mapper
        );
    }

    @Override
    public void processExchange(ApiSignedEncryptedLinkVerificationExchange exchange) {
        seiv2Mapper.processRawInput(exchange);
        signatureVerifier.verify(exchange);

        verifyLink(exchange, linkVerificationTargetMappers);
    }

}
