package nl.logius.pepcrypto.api.oid;

import nl.logius.pepcrypto.api.ApiAsn1SequenceDecodable;
import org.apache.commons.lang3.ArrayUtils;

import java.util.function.Supplier;

public class Asn1SequenceDecodable
        implements ApiAsn1SequenceDecodable {

    private final Supplier<byte[]> rawInputSupplier;

    private String oid;

    public Asn1SequenceDecodable(Supplier<byte[]> rawInputSupplier) {
        this.rawInputSupplier = rawInputSupplier;
    }

    public Asn1SequenceDecodable(byte[] rawInput) {
        rawInputSupplier = () -> rawInput;
    }

    @Override
    public byte[] getRawInput() {
        return ArrayUtils.clone(rawInputSupplier.get());
    }

    @Override
    public String getOid() {
        return oid;
    }

    @Override
    public void setOid(String oid) {
        this.oid = oid;
    }

}
