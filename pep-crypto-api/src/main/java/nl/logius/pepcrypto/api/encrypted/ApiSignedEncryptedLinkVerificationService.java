package nl.logius.pepcrypto.api.encrypted;

import nl.logius.pepcrypto.api.ApiExchangeService;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;

public interface ApiSignedEncryptedLinkVerificationService
        extends ApiExchangeService<Asn1Envelope, ApiSignedEncryptedLinkVerificationExchange> {

}
