package nl.logius.pepcrypto.api;

import nl.logius.pepcrypto.lib.crypto.key.PepPrivateKey;

/**
 * Decryption of the EC-Point.
 */
public interface ApiDecryption<E extends ApiDecryptable<? extends PepPrivateKey>> {

    /**
     * Decrypts the PEP-encrypted data.
     *
     * @param decryptable Exchange containing the PEP-encrypted data.
     */
    void processDecryption(E decryptable);

}
