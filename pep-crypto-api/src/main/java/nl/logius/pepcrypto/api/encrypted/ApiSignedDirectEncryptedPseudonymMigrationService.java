package nl.logius.pepcrypto.api.encrypted;

import nl.logius.pepcrypto.api.ApiExchangeService;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectPseudonymEnvelope;

public interface ApiSignedDirectEncryptedPseudonymMigrationService
        extends ApiExchangeService<Asn1DirectPseudonymEnvelope, ApiSignedDirectEncryptedPseudonymMigrationExchange> {

}
