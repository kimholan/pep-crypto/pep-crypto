package nl.logius.pepcrypto.api.encrypted;

import nl.logius.pepcrypto.api.ApiExchangeService;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectPseudonymEnvelope;

/**
 * SignedDirectEncryptedPseudonym verification/decryption service.
 */
public interface ApiSignedDirectEncryptedPseudonymService
        extends ApiExchangeService<Asn1DirectPseudonymEnvelope, ApiSignedDirectEncryptedPseudonymExchange> {

}
