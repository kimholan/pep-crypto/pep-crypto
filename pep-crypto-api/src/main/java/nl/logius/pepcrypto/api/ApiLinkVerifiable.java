package nl.logius.pepcrypto.api;

import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationExchange;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationTargetExchange;
import nl.logius.pepcrypto.api.oid.ApiOID;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyExtraElements;

import java.util.Base64;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import static nl.logius.pepcrypto.api.exception.ApiExceptionDetail.LINK_VERIFICATION_TARGET_OID_UNSUPPORTED;
import static nl.logius.pepcrypto.lib.crypto.PepDigest.SHA384;

public interface ApiLinkVerifiable {

    static void verifyLink(ApiSignedEncryptedLinkVerificationExchange exchange, Map<ApiOID, ApiAsn1Mapper<? extends Asn1Envelope>> mappers) {
        var linkSourceMappedInput = exchange.getMappedInput();
        var linkSourceSignedBody = (Asn1SignedBodyExtraElements) linkSourceMappedInput.asn1SignedBody();
        var sameIds = linkSourceSignedBody.getSameIds();
        Predicate<String> containsSameId = exchange::containsSameId;

        exchange.setLinkSourceSameIds(sameIds);
        exchange.getLinkVerificationTargets().stream()
                .map(it -> mapLinkTarget(it, mappers))
                .forEach(it -> verifyLinkTarget(it, containsSameId));
    }

    private static void verifyLinkTarget(ApiSignedEncryptedLinkVerificationTargetExchange exchange, Predicate<String> containsSameId) {
        var linkTargetMappedInput = exchange.getMappedInput();
        var linkTargetBytes = linkTargetMappedInput.asn1Body().encodeByteArray();
        var linkTargetHashBytes = SHA384.digest(linkTargetBytes);
        exchange.setEncryptedHash(linkTargetHashBytes);

        var linkTargetHashBase64 = Base64.getEncoder().encodeToString(linkTargetHashBytes);
        var linked = containsSameId.test(linkTargetHashBase64);
        exchange.setLinked(linked);
    }

    private static ApiSignedEncryptedLinkVerificationTargetExchange mapLinkTarget(ApiSignedEncryptedLinkVerificationTargetExchange exchange, Map<ApiOID, ApiAsn1Mapper<? extends Asn1Envelope>> mappers) {
        var mapper = Optional.ofNullable(exchange)
                             .map(ApiSignedEncryptedLinkVerificationTargetExchange::getApiOIDType)
                             .map(mappers::get)
                             .orElseThrow(LINK_VERIFICATION_TARGET_OID_UNSUPPORTED);

        mapper.processRawInput(exchange);
        return exchange;
    }

}
