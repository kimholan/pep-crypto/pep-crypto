package nl.logius.pepcrypto.api.encrypted;

import nl.logius.pepcrypto.api.ApiExchange;
import nl.logius.pepcrypto.api.ApiLinkVerifiable;
import nl.logius.pepcrypto.api.ApiSignatureVerifiable;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.crypto.PepEcSchnorrVerificationKey;

import java.util.List;

public interface ApiSignedEncryptedLinkVerificationExchange
        extends ApiExchange<Asn1Envelope>,
                ApiSignatureVerifiable<PepEcSchnorrVerificationKey>,
                PepEcSchnorrVerificationKey,
                ApiLinkVerifiable {

    List<ApiSignedEncryptedLinkVerificationTargetExchange> getLinkVerificationTargets();

    default boolean isLinkSourceLinkedToAllLinkTargets() {
        var linkVerificationTargets = getLinkVerificationTargets();
        var expectedCount = linkVerificationTargets.size();
        var actualCount = linkVerificationTargets.stream()
                                                 .filter(ApiSignedEncryptedLinkVerificationTargetExchange::isLinked)
                                                 .count();
        return expectedCount != 0 && expectedCount == actualCount;

    }

    void setLinkSourceSameIds(List<String> sameIds);

    boolean containsSameId(String id);

}
