package nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_7_2;

import nl.logius.pepcrypto.api.ApiAsn1Mapper;
import nl.logius.pepcrypto.api.ApiDecryption;
import nl.logius.pepcrypto.api.ApiSignatureVerifier;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedIdentityExchange;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(StrictStubs.class)
public class ApiSignedEncryptedIdentityv2DecryptionTest {

    @InjectMocks
    private ApiSignedEncryptedIdentityv2Decryption service;

    @Mock
    private ApiAsn1Mapper asn1Mapper;

    @Mock
    private ApiSignatureVerifier signatureVerifier;

    @Mock
    private ApiDecryption signedEncryptedIdentity;

    @After
    public void after() {
        verifyNoMoreInteractions(asn1Mapper, signatureVerifier, signedEncryptedIdentity);
    }

    @Test
    public void decrypt() {
        var exchange = mock(ApiSignedEncryptedIdentityExchange.class);

        service.processExchange(exchange);

        verify(asn1Mapper).processRawInput(exchange);
        verify(signatureVerifier).verify(exchange);
        verify(signedEncryptedIdentity).processDecryption(exchange);
    }

}
