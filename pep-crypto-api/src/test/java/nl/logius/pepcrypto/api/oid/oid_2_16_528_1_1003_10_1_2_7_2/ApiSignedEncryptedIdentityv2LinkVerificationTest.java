package nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_7_2;

import nl.logius.pepcrypto.api.ApiAsn1Mapper;
import nl.logius.pepcrypto.api.ApiSignatureVerifier;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.signedencryptedidentityv2.Asn1SignedEncryptedIdentityv2Envelope;
import nl.logius.pepcrypto.lib.crypto.PepEcSchnorrVerificationKey;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import static nl.logius.pepcrypto.lib.TestResources.resourceToByteArray;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class ApiSignedEncryptedIdentityv2LinkVerificationTest {

    @InjectMocks
    private ApiSignedEncryptedIdentityv2LinkVerification service;

    @Mock
    private ApiAsn1Mapper<? extends Asn1Envelope> seiv2Mapper;

    @Mock
    private ApiAsn1Mapper<? extends Asn1Envelope> sepv2Mapper;

    @Mock
    private ApiSignatureVerifier<PepEcSchnorrVerificationKey> signatureVerifier;

    @Test
    public void test() {
        service.postConstruct();

        var exchange = mock(ApiSignedEncryptedLinkVerificationExchange.class);
        var bytes = resourceToByteArray("/v2/happy_flow/tc1_ei_multi_ee_20.asn1");
        var mappedInput = Asn1SignedEncryptedIdentityv2Envelope.fromByteArray(bytes);

        when(exchange.getMappedInput()).thenReturn(mappedInput);

        service.processExchange(exchange);
    }

}
