package nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_8_2;

import nl.logius.pepcrypto.api.ApiAsn1Mapper;
import nl.logius.pepcrypto.api.ApiDecryption;
import nl.logius.pepcrypto.api.ApiSignatureVerifier;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedPseudonymMigrationExchange;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import static org.mockito.Mockito.mock;

/**
 * ApiSignedEncryptedPseudonymv2DecryptionMigrationTest.
 */
@RunWith(StrictStubs.class)
public class ApiSignedEncryptedPseudonymv2DecryptionMigrationTest {

    @InjectMocks
    private ApiSignedEncryptedPseudonymv2DecryptionMigration service;

    @Mock
    private ApiAsn1Mapper asn1Mapper;

    @Mock
    private ApiSignatureVerifier signatureVerifier;

    @Mock
    private ApiDecryption migratable;

    @Test
    public void processExchange() {
        var exchange = mock(ApiSignedEncryptedPseudonymMigrationExchange.class);

        service.processExchange(exchange);
    }

}
