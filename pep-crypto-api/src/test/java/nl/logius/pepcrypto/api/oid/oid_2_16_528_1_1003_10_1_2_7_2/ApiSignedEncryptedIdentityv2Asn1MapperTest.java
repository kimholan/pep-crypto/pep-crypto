package nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_7_2;

import nl.logius.pepcrypto.api.ApiExchange;
import nl.logius.pepcrypto.lib.asn1.signedencryptedidentityv2.Asn1SignedEncryptedIdentityv2Envelope;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import static nl.logius.pepcrypto.lib.TestResources.resourceToByteArray;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class ApiSignedEncryptedIdentityv2Asn1MapperTest {

    @InjectMocks
    private ApiSignedEncryptedIdentityv2Asn1Mapper mapper;

    @Test
    public void map() {
        var bytes = resourceToByteArray("/v2/happy_flow/ei02.asn1");
        var mappable = mock(ApiExchange.class);

        when(mappable.getRawInput()).thenReturn(bytes);

        mapper.processRawInput(mappable);

        verify(mappable).getRawInput();
        verify(mappable).setMappedInput(any(Asn1SignedEncryptedIdentityv2Envelope.class));
    }

}
