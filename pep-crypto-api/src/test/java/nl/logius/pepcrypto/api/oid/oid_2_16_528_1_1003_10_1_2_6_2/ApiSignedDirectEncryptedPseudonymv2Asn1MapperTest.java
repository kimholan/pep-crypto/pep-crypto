package nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_6_2;

import nl.logius.pepcrypto.api.ApiExchange;
import nl.logius.pepcrypto.lib.asn1.signeddirectencryptedpseudonymv2.Asn1SignedDirectEncryptedPseudonymv2Envelope;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import static nl.logius.pepcrypto.lib.TestResources.resourceToByteArray;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class ApiSignedDirectEncryptedPseudonymv2Asn1MapperTest {

    @InjectMocks
    private ApiSignedDirectEncryptedPseudonymv2Asn1Mapper mapper;

    @Test
    public void map() {
        var bytes = resourceToByteArray("/v2/happy_flow/dep12.asn1");
        var mappable = mock(ApiExchange.class);

        when(mappable.getRawInput()).thenReturn(bytes);

        mapper.processRawInput(mappable);

        verify(mappable).getRawInput();
        verify(mappable).setMappedInput(any(Asn1SignedDirectEncryptedPseudonymv2Envelope.class));
    }

}
