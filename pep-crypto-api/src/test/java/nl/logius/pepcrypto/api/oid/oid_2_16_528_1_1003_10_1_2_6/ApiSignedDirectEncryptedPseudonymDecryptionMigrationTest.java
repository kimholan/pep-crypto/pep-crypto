package nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_6;

import nl.logius.pepcrypto.api.ApiAsn1Mapper;
import nl.logius.pepcrypto.api.ApiDecryption;
import nl.logius.pepcrypto.api.ApiSignatureVerifier;
import nl.logius.pepcrypto.api.encrypted.ApiSignedDirectEncryptedPseudonymMigrationExchange;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import static org.mockito.Mockito.mock;

/**
 * ApiSignedDirectEncryptedPseudonymDecryptionMigrationTest.
 */
@RunWith(StrictStubs.class)
public class ApiSignedDirectEncryptedPseudonymDecryptionMigrationTest {

    @InjectMocks
    private ApiSignedDirectEncryptedPseudonymDecryptionMigration service;

    @Mock
    private ApiAsn1Mapper asn1Mapper;

    @Mock
    private ApiSignatureVerifier signatureVerifier;

    @Mock
    private ApiDecryption migratable;

    @Test
    public void processExchange() {
        var exchange = mock(ApiSignedDirectEncryptedPseudonymMigrationExchange.class);

        service.processExchange(exchange);
    }

}
