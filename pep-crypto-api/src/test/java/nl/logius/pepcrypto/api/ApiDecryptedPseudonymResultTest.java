package nl.logius.pepcrypto.api;

import groovy.json.JsonOutput;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;
import nl.logius.pepcrypto.lib.crypto.PepCrypto;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.TEN;
import static java.math.BigInteger.TWO;
import static java.math.BigInteger.ZERO;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.skyscreamer.jsonassert.JSONCompareMode.LENIENT;

public class ApiDecryptedPseudonymResultTest {

    public static final String EXPECTED = "{\"notationIdentifier\":\"ID_BSNK_DECRYPTED_PSEUDONYM\",\"type\":0,\"rawInput\":null,\"schemeKeySetVersion\":2,\"diversifier\":null,\"pseudonymValue\":\"pseudonymValue\",\"recipientKeySetVersion\":10,\"schemeVersion\":1,\"recipient\":\"recipient\"}";

    @Test
    public void asAsn1DecryptedPseudonym() throws JSONException {
        var mock = mock(ApiDecryptedPseudonymResult.class);
        when(mock.asAsn1DecryptedPseudonym()).thenCallRealMethod();

        when(mock.getDecryptedPseudonymResultPseudonymValue()).thenReturn("pseudonymValue");

        var recipientKeyId = mock(PepRecipientKeyId.class);
        when(recipientKeyId.getSchemeVersion()).thenReturn(ONE);
        when(recipientKeyId.getSchemeKeySetVersion()).thenReturn(TWO);
        when(recipientKeyId.getRecipient()).thenReturn("recipient");
        when(recipientKeyId.getRecipientKeySetVersion()).thenReturn(TEN);
        when(mock.getDecryptedPseudonymResultPepRecipientKeyId()).thenReturn(recipientKeyId);

        var asn1Pseudonym = mock(Asn1Pseudonym.class);
        when(mock.getDecryptedPseudonymResultAsn1Pseudonym()).thenReturn(asn1Pseudonym);
        when(asn1Pseudonym.getType()).thenReturn(ZERO);

        var actual = JsonOutput.toJson(mock.asAsn1DecryptedPseudonym());
        JSONAssert.assertEquals(EXPECTED, actual, LENIENT);

        verify(mock).asAsn1DecryptedPseudonym();
        verify(mock).getDecryptedPseudonymResultPseudonymValue();
        verify(mock).getDecryptedPseudonymResultPepRecipientKeyId();
        verify(mock).getDecryptedPseudonymResultAsn1Pseudonym();

        verifyNoMoreInteractions(mock);
    }

    @Test
    public void getDecryptedPseudonymResultPseudonymValue() {
        var mock = mock(ApiDecryptedPseudonymResult.class);

        when(mock.getDecryptedPseudonymResultPseudonymValue()).thenCallRealMethod();
        when(mock.getDecryptedPseudonymResultEcPoint()).thenReturn(PepCrypto.getBasePoint());

        var actual = mock.getDecryptedPseudonymResultPseudonymValue();
        assertEquals("BEO9fpr7U9i4Uom8xI7lv+byATfRCgh+tueHHioQpZnHEK+NDTniBhEU/dBVRewcyKtAkyR/dydeB0P/7RFxguqpx3h3qqxqx9NSRdFpLo7h", actual);

        verify(mock).getDecryptedPseudonymResultPseudonymValue();
        verify(mock).getDecryptedPseudonymResultEcPoint();

        verifyNoMoreInteractions(mock);
    }

}
