package nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_5;

import nl.logius.pepcrypto.api.ApiAsn1Mapper;
import nl.logius.pepcrypto.api.ApiDecryption;
import nl.logius.pepcrypto.api.ApiDirectEncryptedPseudonymMigratable;
import nl.logius.pepcrypto.api.ApiSignatureVerifier;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import static org.mockito.Mockito.mock;

/**
 * ApiDirectDirectEncryptedPseudonymDecryptionMigrationTest.
 */
@RunWith(StrictStubs.class)
public class ApiDirectEncryptedPseudonymDecryptionMigrationTest {

    @InjectMocks
    private ApiDirectEncryptedPseudonymDecryptionMigration service;

    @Mock
    private ApiAsn1Mapper asn1Mapper;

    @Mock
    private ApiSignatureVerifier signatureVerifier;

    @Mock
    private ApiDecryption migratable;

    @Test
    public void processExchange() {
        var exchange = mock(ApiDirectEncryptedPseudonymMigratable.class);

        service.processDecryption(exchange);
    }

}
