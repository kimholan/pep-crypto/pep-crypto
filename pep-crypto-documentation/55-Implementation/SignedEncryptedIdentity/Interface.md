**How-To: OpenApi UI**

- Load and run the docker image
  - See the “How-To: Running a docker image from scratch on Windows” for instructions 
- Go to the following URL:
    http://localhost:8080/pep-crypto/api/v1/openapi-ui/index.html
- Click on ‘POST’  
  - Example request will be visible for the decryption of an EncryptedIdentity. 
- Click on the button ‘Try it out’. 
  - A bar with the text ‘Execute’ will appear below the example request.
- Click on ‘Execute’ and a response message is returned 
  - When the decryption is successful a HTTP status code 200 is 
    returned with an decryption result containing the ‘identifier’ (BSN).
