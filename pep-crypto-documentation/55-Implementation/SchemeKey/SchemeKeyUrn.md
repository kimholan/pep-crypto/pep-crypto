= Schemekey format

Schemekeys are passed in as key-value pairs: the key is the key metadata , the value is the key itself.

== key-metadata
same format as published by BSNk metadata (urn:nl-gdi-eid:1.0:pp-key:<Environment>:<SchemeKeySetVersion>:<KeyName>:<KeyVersion> )


== examples
- urn:nl-gdi-eid:1.0:pp-key:prod:1:IP_P:1 
- urn:nl-gdi-eid:1.0:pp-key:prod:10:PP_P:1 
- urn:nl-gdi-eid:1.0:pp-key:prod:1:U:1

== key-data
base64 (stored in BSI TR 03111 and ANSI X9.62 (2005)), binary data starts with '0x04' (uncompressed X and Y),total length 81bytes,
 
example:
- BKno8rHLf2wa/tmitaPPLi/2kdR3Tb7cKqduHirz037egIMG4WNA8aov/gYvJ1D84LgR2buZj06ZjxAUkxueMlc2UkM2wF7GIwkf48m8ST56

Reference
=========

See BPTSbeheer-190419-1417-58.pdf, section 'Metadata, Entity Descriptor per system, Key Descriptor', page 174-175