Caveats
=======

- BouncyCastle allows obtaining the X- and Y-coordinates from ECPoint-instances on non-normalized ECPoints
  without warning using getXCoord-methods or getYCoord-methods. The alternative getters for obtaining the 
  coordinates using ECPoint.getAffineXCoord()  and ECPoint.getAffineYCoord() will check and fail if 
  the ECPoint is not normalized.  
- Creating ECPoint-instances by specifying the curve and coordinates could lead to coordinates 
  not located on the BrainpoolP320r1-curve. The decryption component does not create ECPoint-instances
  that way and and as such this is a non-issue unless one chooses to do so for whatever reason.   
