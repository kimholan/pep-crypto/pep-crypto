Projectstructure (Maven)
========================

The top-level POM aggregates all maven modules for build purposes.

The core application depends on CDI, but care has been taken
to separate the pulled in dependencies by splitting the
project into multiple modules:
- to make it easier to replace certain specification related parts such 
  as the server implementation, which is currently Thorntail-based.
- to reuse code without the requirement to implement a high-level
  specification such as OpenAPI.
- better visibility of the dependency structure

| Module                   | Description                                       | 
|--------------------------|---------------------------------------------------|
| pep-crypto-pom           | Project configruation and dependency management   |
| pep-crypto-documentation | Documentation, reference data, specifications     |
| pep-crypto-lib           | Non-CDI library code                              |
| pep-crypto-api           | CDI-based decryption component                    |
| pep-crypto-microprofile  | Server application (Microprofile)                 |
| pep-crypto-thorntail     | Server application (Thorntail packaging)          |
| pep-crypto-test          | Testsuite                                         |

```mermaid

graph BT
  subgraph pep-crypto-pom
    p-c-pom-api
    p-c-pom-lib
    p-c-pom-microprofile
    p-c-pom-test
    p-c-pom-thorntail
    p-c-pom
  end
  subgraph pep-crypto-documentation
    p-c-documentation-->|inherits|p-c-pom
    p-c-documentation
  end
  subgraph pep-crypto-lib
    p-c-lib-->|inherits|p-c-pom-lib
    p-c-lib
  end
  subgraph pep-crypto-api
    p-c-api-->|inherits|p-c-pom-api
    p-c-api
  end
  subgraph pep-crypto-microprofile
    p-c-microprofile-->|inherits|p-c-pom-microprofile
    p-c-microprofile
  end
  subgraph pep-crypto-thorntail
    p-c-thorntail-->|inherits|p-c-pom-thorntail
    p-c-thorntail
  end
  subgraph pep-crypto-test
    p-c-test-->|inherits|p-c-pom-test
    p-c-test
  end
  subgraph pep-crypto
    p-c-->|aggregates|p-c-pom    
    p-c-->|aggregates|p-c-documentation    
    p-c-->|aggregates|p-c-lib
    p-c-->|aggregates|p-c-api
    p-c-->|aggregates|p-c-microprofile
    p-c-->|aggregates|p-c-thorntail
    p-c-->|aggregates|p-c-test
  end
```
