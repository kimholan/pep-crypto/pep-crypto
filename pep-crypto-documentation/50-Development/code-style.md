Code style
==========

- Non-goals
  - 'Purism' concerning nullability, solipsism, functional programming, 
     and other idioms, design patterns
  - Avoiding dependencies on well-known libraries at all costs
    and reimplementing common utlity methods 
  - 'Performance' without a specified reference setting
  - Code duplication at all costs
- Goals
  - Ease of reuse in a practical setting 
    - Classes in the pep-crypto-api module (and the modules it
      depends on) are considered reusable, thus they 
      are prefixed with the module name
      - This is ugly, but saves a lot of headaches due to name clashes
      - pep-crypto-microprofile is an example on how the API
        is applied, which is my most classes do not have any
        prefix: if the api classes weren't prefixed it would've
        been the other way around
  - Supporting unspecified non-functional requirements
    - Ease of integrating the pep-crypto-api 
  - Avoid forcing adopting code to depend on concrete types
    by providing interfaces
  - No assumptions on how the adopting code handles key material
  - ASN.1-schema bindings based on the schema
    - Although the current code generation is not perfect it forces
      the consider the correctness of the schema when dealing
      with ASN.1
- Code style/inspection profile 
  - Slightly modification of the default profile in IntelliJ
  - Settings export is provided in [idea-settings.zip](idea-settings.zip).


Code documentation style
========================

- Non-goals
  - Duplicating specifications and algorithm descriptions described
    in papers or otherwise availabe 
