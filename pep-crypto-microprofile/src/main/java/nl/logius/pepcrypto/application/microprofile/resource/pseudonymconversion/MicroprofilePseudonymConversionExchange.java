package nl.logius.pepcrypto.application.microprofile.resource.pseudonymconversion;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymRequest;
import nl.logius.pepcrypto.api.decrypted.ApiClosingKeyConversionExchange;
import nl.logius.pepcrypto.application.microprofile.keystore.MicroprofileServiceProviderKeyStoreExchange;
import nl.logius.pepcrypto.application.microprofile.resource.AbstractMicroprofileRequestExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1RecipientKeyId;
import nl.logius.pepcrypto.lib.asn1.decryptedpseudonym.Asn1DecryptedPseudonym;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.key.PepDpClosingKeyConversionClosingPrivateKey;
import org.bouncycastle.math.ec.ECPoint;

import static nl.logius.pepcrypto.lib.crypto.key.PepDpClosingKeyConversionClosingPrivateKey.dpClosingConversionClosingPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpClosingPrivateKey.epClosingPrivateKey;

class MicroprofilePseudonymConversionExchange
        extends AbstractMicroprofileRequestExchange<OASPseudonymRequest, Asn1DecryptedPseudonym>
        implements MicroprofileServiceProviderKeyStoreExchange,
                   ApiClosingKeyConversionExchange {

    MicroprofilePseudonymConversionExchange(OASPseudonymRequest request) {
        super(request, request::getPseudonym);
    }

    @Override
    public ECPoint getEcPoint() {
        return getMappedInput().asn1PseudonymValueAsEcPoint();
    }

    @Override
    public ECPoint getDecryptedPseudonymResultEcPoint() {
        return getConvertedEcPoint();
    }

    @Override
    public PepDpClosingKeyConversionClosingPrivateKey getSelectedConversionKey() {
        var epClosingSource = epClosingPrivateKey(getSourceClosingKey().getPrivateKey());
        var epClosingTarget = epClosingPrivateKey(getTargetClosingKey().getPrivateKey());
        return dpClosingConversionClosingPrivateKey(epClosingSource, epClosingTarget);
    }

    @Override
    public Asn1Pseudonym getDecryptedPseudonymResultAsn1Pseudonym() {
        return getMappedInput();
    }

    @Override
    public PepRecipientKeyId getDecryptedPseudonymResultPepRecipientKeyId() {
        return getTargetClosingKeyAsRecipientKeyId();
    }

    PepRecipientKeyId getTargetClosingKeyAsRecipientKeyId() {
        var inputRecipientKeyId = new Asn1RecipientKeyId(getMappedInput());
        var request = getRequest();
        var targetRecipientKeySetVersion = request.getTargetClosingKey();
        var targetSchemeKeySetVersion = request.getTargetClosingKeySchemeKeySetVersion();
        return complementRecipientKeyId(inputRecipientKeyId, targetRecipientKeySetVersion, targetSchemeKeySetVersion);
    }

}
