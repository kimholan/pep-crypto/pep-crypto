package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import nl.logius.pepcrypto.api.ApiAsn1SequenceDecodable;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationTargetExchange;
import nl.logius.pepcrypto.api.oid.ApiOID;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import org.apache.commons.lang3.ArrayUtils;

class MicroprofileVerifyLinkTargetExchange
        implements ApiSignedEncryptedLinkVerificationTargetExchange {

    private final ApiAsn1SequenceDecodable decodable;

    private boolean linked;

    private Asn1Envelope mappedInput;

    private byte[] encryptedHash;

    MicroprofileVerifyLinkTargetExchange(ApiAsn1SequenceDecodable decodable) {
        this.decodable = decodable;
    }

    @Override
    public boolean isLinked() {
        return linked;
    }

    @Override
    public void setLinked(boolean linked) {
        this.linked = linked;
    }

    @Override
    public byte[] getRawInput() {
        return decodable.getRawInput();
    }

    @Override
    public Asn1Envelope getMappedInput() {
        return mappedInput;
    }

    @Override
    public void setMappedInput(Asn1Envelope mappedInput) {
        this.mappedInput = mappedInput;
    }

    @Override
    public byte[] getEncryptedHash() {
        return ArrayUtils.clone(encryptedHash);
    }

    @Override
    public void setEncryptedHash(byte[] encryptedHash) {
        this.encryptedHash = ArrayUtils.clone(encryptedHash);
    }

    @Override
    public ApiOID getApiOIDType() {
        return decodable.getApiOIDType();
    }

}
