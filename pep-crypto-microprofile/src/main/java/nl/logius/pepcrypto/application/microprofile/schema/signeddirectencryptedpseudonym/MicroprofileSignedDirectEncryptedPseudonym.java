package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonym;
import nl.logius.pepcrypto.application.microprofile.schema.signaturevalue.MicroprofileSignatureValue;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectPseudonymEnvelope;

/**
 * Maps data to output model.
 */
public enum MicroprofileSignedDirectEncryptedPseudonym {
    ;

    public static OASSignedDirectEncryptedPseudonym newInstance(Asn1DirectPseudonymEnvelope source) {
        var target = new OASSignedDirectEncryptedPseudonym();

        target.notationIdentifier(source.asn1Oid())
              .signedDEP(MicroprofileSignedDEP.newInstance(source))
              .signatureValue(MicroprofileSignatureValue.newInstance(source));

        return target;
    }

}
