package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkResponse;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationService;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_7_2;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_8_2;

/**
 * Exposes the link verification.
 */
@ApplicationScoped
@Path("/verify-link")
public class MicroprofileVerifyLinkApi {

    @Inject
    @Any
    private Instance<ApiSignedEncryptedLinkVerificationService> serviceInstance;

    @Inject
    private MicroprofileVerifyLinkResponseMapper responseMapper;

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public @Valid OASVerifyLinkResponse processRequest(
            @MicroprofileRequiredOid({
                    OID_2_16_528_1_1003_10_1_2_7_2,
                    OID_2_16_528_1_1003_10_1_2_8_2,
            })
            @Valid OASVerifyLinkRequest request) {
        var linkSourceOidType = request.getAsn1SequenceDecodableOid();
        var service = serviceInstance.select(linkSourceOidType).get();
        var exchange = new MicroprofileVerifyLinkExchange(request);

        service.processExchange(exchange);

        return responseMapper.mapToResponse(exchange);
    }

}
