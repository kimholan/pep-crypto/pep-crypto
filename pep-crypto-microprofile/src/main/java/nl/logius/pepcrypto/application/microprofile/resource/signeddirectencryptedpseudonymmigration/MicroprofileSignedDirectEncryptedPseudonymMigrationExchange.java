package nl.logius.pepcrypto.application.microprofile.resource.signeddirectencryptedpseudonymmigration;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonymMigrationRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedDirectEncryptedPseudonymMigrationExchange;
import nl.logius.pepcrypto.application.microprofile.resource.AbstractMicroprofileSignedRequestExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectEncrypted;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectPseudonymEnvelope;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;
import nl.logius.pepcrypto.lib.crypto.PepEcSchnorrVerificationKey;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.key.PepDepMigrationClosingPrivateKey;
import nl.logius.pepcrypto.lib.crypto.key.PepEpDecryptionPrivateKey;
import nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrn;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.util.Optional;

import static nl.logius.pepcrypto.lib.crypto.key.PepDepMigrationClosingPrivateKey.depMigrationClosingPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepDrkiPrivateKey.drkiPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpClosingPrivateKey.epClosingPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpDecryptionPrivateKey.epDecryptionPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationPrivateKey.epMigrationPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationSourcePrivateKey.epMigrationSourcePrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationTargetPrivateKey.epMigrationTargetPrivateKey;
import static nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns.SCHEME_KEY_U;

/**
 * Carries the processing state of a message exchange.
 */
class MicroprofileSignedDirectEncryptedPseudonymMigrationExchange
        extends AbstractMicroprofileSignedRequestExchange<OASSignedDirectEncryptedPseudonymMigrationRequest, Asn1DirectPseudonymEnvelope>
        implements PepEcSchnorrVerificationKey,
                   Asn1DirectEncrypted,
                   ApiSignedDirectEncryptedPseudonymMigrationExchange {

    MicroprofileSignedDirectEncryptedPseudonymMigrationExchange(OASSignedDirectEncryptedPseudonymMigrationRequest request) {
        super(request, request::getSignedDirectEncryptedPseudonym);
    }

    @Override
    public PepDepMigrationClosingPrivateKey getClosingPepPrivateKey() {
        var epClosing = epClosingPrivateKey(getSelectedClosingKey().getPrivateKey());
        var epMigrationSource = epMigrationSourcePrivateKey(getMigrationSourceKey().getPrivateKey());
        var epMigrationTarget = epMigrationTargetPrivateKey(getMigrationTargetKey().getPrivateKey());
        var drki = drkiPrivateKey(getSelectedDirectReceiveKey().getPrivateKey());
        var epMigrationClosing = epMigrationPrivateKey(epClosing, epMigrationSource, epMigrationTarget);
        return depMigrationClosingPrivateKey(drki, epMigrationClosing);
    }

    @Override
    public PepEcSchnorrVerificationKey getVerificationKeys() {
        return this;
    }

    @Override
    public BigInteger asn1SigningKeyVersion() {
        return getMappedInput().asn1SigningKeyVersion();
    }

    @Override
    public String asn1AuthorizedParty() {
        return getMappedInput().asn1AuthorizedParty();
    }

    @Override
    public boolean isAuthorizedParty() {
        return Optional.of(getRequest())
                       .map(OASSignedDirectEncryptedPseudonymMigrationRequest::getAuthorizedParty)
                       .filter(StringUtils::isNotBlank)
                       .filter(it -> asn1AuthorizedParty().equals(it))
                       .isPresent();
    }

    @Override
    public PepEpDecryptionPrivateKey getDecryptionPepPrivateKey() {
        return epDecryptionPrivateKey(getSelectedDecryptionKey().getPrivateKey());
    }

    @Override
    public Asn1Pseudonym getDecryptedPseudonymResultAsn1Pseudonym() {
        return getMappedInput().asn1Pseudonym();
    }

    @Override
    public PepRecipientKeyId getDecryptedPseudonymResultPepRecipientKeyId() {
        var recipient = getMigrationTargetKey().getRecipient();
        var recipientKeySetVersion = getMigrationTargetKey().getRecipientKeySetVersion();
        var recipientKeyId = getMappedInput().asn1RecipientKeyId();

        return recipientKeyId.recipient(recipient)
                             .recipientKeySetVersion(recipientKeySetVersion);
    }

    @Override
    public ECPoint getDecryptedPseudonymResultEcPoint() {
        return getDecryptedEcPoint();
    }

    public boolean isMatchingSchemeKeyUrn(String urn) {
        var schemeKeySetVersion = getSelectedSchemeKeySetVersion();
        var signingKeyVersion = asn1SigningKeyVersion();

        return Optional.ofNullable(urn)
                       .map(SCHEME_KEY_U::asPepSchemeKeyUrn)
                       .filter(PepSchemeKeyUrn::matches)
                       .filter(it -> it.isMatchingSchemeKeySetVersionString(schemeKeySetVersion))
                       .filter(it -> it.isMatchingSchemeKeyVersionString(signingKeyVersion))
                       .isPresent();
    }

    String getRequestMigrationId() {
        return getRequest().getMigrationID();
    }

    boolean isMigrationTargetSelectionInvalid() {
        return getRequestTargetMigrant() == null ^ getRequestTargetMigrantKeySetVersion() == null;
    }

    String getRequestTargetMigrant() {
        return getRequest().getTargetMigrant();
    }

    BigInteger getRequestTargetMigrantKeySetVersion() {
        return Optional.of(getRequest())
                       .map(OASSignedDirectEncryptedPseudonymMigrationRequest::getTargetMigrantKeySetVersion)
                       .map(BigInteger::new)
                       .orElse(null);
    }

}
