package nl.logius.pepcrypto.application.microprofile.schema.pseudonymmigrationexport;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationExportRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASPseudonymMigrationExportRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASPseudonymMigrationExportRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASPseudonymMigrationExportRequest target) {
        return target::getPseudonym;
    }

}
