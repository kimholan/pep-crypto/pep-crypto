package nl.logius.pepcrypto.application.microprofile.resource;

enum ExchangeProperties {
    // Basic message
    REQUEST,
    RAW_INPUT_SUPPLIER,
    PARSED_SERVICE_PROVIDER_KEYS,
    MAPPED_INPUT,
    //  MicroprofilePseudonymConversionExchange
    CONVERTED_EC_POINT,
    SOURCE_CLOSING_KEY,
    TARGET_CLOSING_KEY,
    // MicroprofilePseudonymMigrationImportExchange
    MIGRATION_TARGET_KEY,
    // MicroprofilePseudonymMigrationExportExchange
    MIGRATION_SOURCE_KEY,
    // MicroprofileDecryptionKeystoreExchange
    DECRYPTED_EC_POINT,
    PARSED_SCHEME_KEYS,
    SELECTED_SCHEME_KEY,
    SELECTED_DECRYPTION_KEY,
    SELECTED_CLOSING_KEY,
    SELECTED_DIRECT_RECEIVE_KEY

}
