package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedIdentityRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASSignedDirectEncryptedIdentityRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASSignedDirectEncryptedIdentityRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASSignedDirectEncryptedIdentityRequest target) {
        return target::getSignedDirectEncryptedIdentity;
    }

}
