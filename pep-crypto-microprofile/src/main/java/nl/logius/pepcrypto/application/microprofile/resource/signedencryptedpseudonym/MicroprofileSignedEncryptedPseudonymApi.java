package nl.logius.pepcrypto.application.microprofile.resource.signedencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonymRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedPseudonymService;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_4;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_8;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_8_2;

/**
 * Exposes the decryption of SignedEncryptedPseudonym as a JAX-RS resource.
 */
@ApplicationScoped
@Path("/signed-encrypted-pseudonym")
public class MicroprofileSignedEncryptedPseudonymApi {

    @Inject
    @Any
    private Instance<ApiSignedEncryptedPseudonymService> prototypeServiceInstance;

    @Inject
    private MicroprofileSignedEncryptedPseudonymResponseMapper responseMapper;

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public @Valid OASDecryptedPseudonymResponse processRequest(
            @MicroprofileRequiredOid({
                    OID_2_16_528_1_1003_10_1_2_4,
                    OID_2_16_528_1_1003_10_1_2_8,
                    OID_2_16_528_1_1003_10_1_2_8_2
            })
            @Valid OASSignedEncryptedPseudonymRequest request) {
        var oidAnnotationLiteral = request.getAsn1SequenceDecodableOid();
        var exchange = new MicroprofileSignedEncryptedPseudonymExchange(request);
        var service = prototypeServiceInstance.select(oidAnnotationLiteral).get();
        service.processExchange(exchange);

        // Map the processed values to the response
        return responseMapper.mapToResponse(exchange);
    }

}



