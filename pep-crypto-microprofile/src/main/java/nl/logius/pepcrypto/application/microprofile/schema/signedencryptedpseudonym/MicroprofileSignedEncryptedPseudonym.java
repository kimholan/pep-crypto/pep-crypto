package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonym;
import nl.logius.pepcrypto.application.microprofile.schema.signaturevalue.MicroprofileSignatureValue;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1PseudonymEnvelope;

/**
 * Maps data to output model.
 */
public enum MicroprofileSignedEncryptedPseudonym {
    ;

    public static OASSignedEncryptedPseudonym newInstance(Asn1Envelope source) {
        return newInstance((Asn1PseudonymEnvelope) source);
    }

    public static OASSignedEncryptedPseudonym newInstance(Asn1PseudonymEnvelope source) {
        var target = new OASSignedEncryptedPseudonym();

        target.notationIdentifier(source.asn1Oid())
              .signedEP(MicroprofileSignedEP.newInstance(source))
              .signatureValue(MicroprofileSignatureValue.newInstance(source));

        return target;
    }

}
