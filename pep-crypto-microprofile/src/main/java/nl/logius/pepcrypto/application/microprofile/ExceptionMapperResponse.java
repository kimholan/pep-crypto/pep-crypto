package nl.logius.pepcrypto.application.microprofile;

import nl.logius.pepcrypto.lib.lang.PepRuntimeException;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.validation.ValidationException;
import javax.ws.rs.core.Response;

enum ExceptionMapperResponse {
    ;

    static Response fromRuntimeException(RuntimeException cause) {
        return fromThrowable(cause);
    }

    static Response fromValidationException(ValidationException cause) {
        var actualCause = ExceptionUtils.getThrowableList(cause).stream()
                                        .filter(PepRuntimeException.class::isInstance)
                                        .findFirst()
                                        .orElse(cause);
        return fromThrowable(actualCause);
    }

    static Response fromThrowable(Throwable cause) {
        return Response.serverError().entity(cause.getMessage()).build();
    }

}
