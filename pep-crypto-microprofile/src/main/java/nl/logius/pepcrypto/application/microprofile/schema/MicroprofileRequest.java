package nl.logius.pepcrypto.application.microprofile.schema;

import nl.logius.pepcrypto.api.ApiAsn1SequenceDecodable;
import nl.logius.pepcrypto.api.oid.ApiOID;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import static nl.logius.pepcrypto.application.microprofile.exception.MicroprofileExceptionDetail.MICROPROFILE_MODULE;

public class MicroprofileRequest {

    private ApiAsn1SequenceDecodable asn1SequenceDecodable;

    private List<ApiAsn1SequenceDecodable> linkTargetAsn1SequenceDecodables;

    public Supplier<List<String>> rawServiceProviderKeysSupplier() {
        return this::getServiceProviderKeys;
    }

    public Supplier<Map<String, byte[]>> rawSchemeKeysSupplier() {
        return this::getSchemeKeys;
    }

    protected List<String> getServiceProviderKeys() {
        throw MICROPROFILE_MODULE.get();
    }

    protected Map<String, byte[]> getSchemeKeys() {
        throw MICROPROFILE_MODULE.get();
    }

    public ApiAsn1SequenceDecodable getAsn1SequenceDecodable() {
        return asn1SequenceDecodable;
    }

    public void setAsn1SequenceDecodable(ApiAsn1SequenceDecodable decodable) {
        asn1SequenceDecodable = decodable;
    }

    public ApiOID getAsn1SequenceDecodableOid() {
        return Optional.ofNullable(getAsn1SequenceDecodable())
                       .map(ApiAsn1SequenceDecodable::getApiOIDType)
                       .orElse(null);
    }

    public List<ApiAsn1SequenceDecodable> getLinkTargetAsn1SequenceDecodables() {
        return linkTargetAsn1SequenceDecodables;
    }

    public void setLinkTargetAsn1SequenceDecodables(List<ApiAsn1SequenceDecodable> linkTargetAsn1SequenceDecodables) {
        this.linkTargetAsn1SequenceDecodables = linkTargetAsn1SequenceDecodables;
    }

}
