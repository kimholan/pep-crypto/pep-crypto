package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedpseudonymmigration;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonymMigrationRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASSignedEncryptedPseudonymMigrationRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASSignedEncryptedPseudonymMigrationRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASSignedEncryptedPseudonymMigrationRequest target) {
        return target::getSignedEncryptedPseudonym;
    }

}
