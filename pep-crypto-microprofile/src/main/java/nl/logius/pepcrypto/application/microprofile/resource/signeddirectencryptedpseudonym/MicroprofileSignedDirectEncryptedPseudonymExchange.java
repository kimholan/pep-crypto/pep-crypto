package nl.logius.pepcrypto.application.microprofile.resource.signeddirectencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonymRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedDirectEncryptedPseudonymExchange;
import nl.logius.pepcrypto.application.microprofile.resource.AbstractMicroprofileSignedRequestExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectEncrypted;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectPseudonymEnvelope;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;
import nl.logius.pepcrypto.lib.crypto.PepPublicVerificationKey;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.key.PepDrkiPrivateKey;
import nl.logius.pepcrypto.lib.crypto.key.PepEpClosingPrivateKey;
import nl.logius.pepcrypto.lib.crypto.key.PepEpDecryptionPrivateKey;
import nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrn;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.util.Optional;

import static nl.logius.pepcrypto.lib.crypto.key.PepDrkiPrivateKey.drkiPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpClosingPrivateKey.epClosingPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpDecryptionPrivateKey.epDecryptionPrivateKey;
import static nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns.SCHEME_KEY_U;

/**
 * Carries the processing state of a message exchange.
 */
class MicroprofileSignedDirectEncryptedPseudonymExchange
        extends AbstractMicroprofileSignedRequestExchange<OASSignedDirectEncryptedPseudonymRequest, Asn1DirectPseudonymEnvelope>
        implements PepPublicVerificationKey,
                   Asn1DirectEncrypted,
                   ApiSignedDirectEncryptedPseudonymExchange {

    MicroprofileSignedDirectEncryptedPseudonymExchange(OASSignedDirectEncryptedPseudonymRequest request) {
        super(request, request::getSignedDirectEncryptedPseudonym);
    }

    @Override
    public BigInteger asn1SigningKeyVersion() {
        return getMappedInput().asn1SigningKeyVersion();
    }

    @Override
    public String asn1AuthorizedParty() {
        return getMappedInput().asn1AuthorizedParty();
    }

    @Override
    public boolean isAuthorizedParty() {
        return Optional.of(getRequest())
                       .map(OASSignedDirectEncryptedPseudonymRequest::getAuthorizedParty)
                       .filter(StringUtils::isNotBlank)
                       .filter(it -> asn1AuthorizedParty().equals(it))
                       .isPresent();
    }

    @Override
    public PepEpDecryptionPrivateKey getDecryptionPepPrivateKey() {
        return epDecryptionPrivateKey(getSelectedDecryptionKey().getPrivateKey());
    }

    @Override
    public PepPublicVerificationKey getVerificationKeys() {
        return this;
    }

    @Override
    public ECPoint getDecryptedPseudonymResultEcPoint() {
        return getDecryptedEcPoint();
    }

    @Override
    public Asn1Pseudonym getDecryptedPseudonymResultAsn1Pseudonym() {
        return getMappedInput().asn1Pseudonym();
    }

    @Override
    public PepRecipientKeyId getDecryptedPseudonymResultPepRecipientKeyId() {
        var selectedClosingKey = getSelectedClosingKey();
        var targetClosingKeyRecipientKeySetVersion = selectedClosingKey.getRecipientKeySetVersion();
        var targetClosingKeySchemeKeySetVersion = selectedClosingKey.getSchemeKeySetVersion();
        var recipientKeyId = getMappedInput().asn1RecipientKeyId();

        return recipientKeyId.recipientKeySetVersion(targetClosingKeyRecipientKeySetVersion)
                             .schemeKeySetVersion(targetClosingKeySchemeKeySetVersion);
    }

    @Override
    public PepDrkiPrivateKey getSelectedDirectReceivePrivateKey() {
        return drkiPrivateKey(getSelectedDirectReceiveKey().getPrivateKey());
    }

    @Override
    public PepEpClosingPrivateKey getClosingPepPrivateKey() {
        return epClosingPrivateKey(getSelectedClosingKey().getPrivateKey());
    }

    public boolean isMatchingSchemeKeyUrn(String urn) {
        var schemeKeySetVersion = getSelectedSchemeKeySetVersion();
        var signingKeyVersion = asn1SigningKeyVersion();

        return Optional.ofNullable(urn)
                       .map(SCHEME_KEY_U::asPepSchemeKeyUrn)
                       .filter(PepSchemeKeyUrn::matches)
                       .filter(it -> it.isMatchingSchemeKeySetVersionString(schemeKeySetVersion))
                       .filter(it -> it.isMatchingSchemeKeyVersionString(signingKeyVersion))
                       .isPresent();
    }

    PepRecipientKeyId getTargetClosingKeyAsRecipientKeyId() {
        var recipientKeyId = getSelectedDecryptionKeyRecipientKeyId();
        var request = getRequest();
        var targetRecipientKeySetVersion = request.getTargetClosingKey();
        var targetSchemeKeySetVersion = request.getTargetClosingKeySchemeKeySetVersion();
        return complementRecipientKeyId(recipientKeyId, targetRecipientKeySetVersion, targetSchemeKeySetVersion);
    }

}
