package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedIdentityRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASSignedEncryptedIdentityRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASSignedEncryptedIdentityRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASSignedEncryptedIdentityRequest target) {
        return target::getSignedEncryptedIdentity;
    }

}
