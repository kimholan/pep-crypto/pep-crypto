package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASDirectEncryptedIdentity;
import nl.logius.pepcrypto.lib.asn1.signeddirectencryptedidentityv2.Asn1SignedDirectEncryptedIdentityv2Envelope;

/**
 * Maps data to output model.
 */
enum MicroprofileDirectEncryptedIdentity {
    ;

    static OASDirectEncryptedIdentity newInstance(Asn1SignedDirectEncryptedIdentityv2Envelope source) {
        var target = new OASDirectEncryptedIdentity();
        var points = source.asn1EcPointValues();
        var pepRecipientKeyId = source.asn1RecipientKeyId();

        target.creator(source.asn1Body().getCreator())
              .points(points)
              .authorizedParty(source.asn1AuthorizedParty())
              .notationIdentifier(source.asn1BodyOid())
              .recipient(pepRecipientKeyId.getRecipient())
              .recipientKeySetVersion(pepRecipientKeyId.getRecipientKeySetVersion().toString())
              .schemeVersion(pepRecipientKeyId.getSchemeVersion().toString())
              .schemeKeySetVersion(pepRecipientKeyId.getSchemeKeySetVersion().toString())
        ;

        return target;
    }

}
