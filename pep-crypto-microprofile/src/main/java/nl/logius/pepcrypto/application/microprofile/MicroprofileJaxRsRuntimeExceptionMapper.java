package nl.logius.pepcrypto.application.microprofile;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static nl.logius.pepcrypto.application.microprofile.ExceptionMapperResponse.fromRuntimeException;

@Provider
public class MicroprofileJaxRsRuntimeExceptionMapper
        implements ExceptionMapper<RuntimeException> {

    @Override
    public Response toResponse(RuntimeException cause) {
        return fromRuntimeException(cause);
    }

}
