package nl.logius.pepcrypto.application.microprofile.schema.decryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonym;
import nl.logius.pepcrypto.application.microprofile.schema.diversifier.MicroprofileDiversifier;
import nl.logius.pepcrypto.lib.asn1.decryptedpseudonym.Asn1DecryptedPseudonym;

/**
 * Maps data to output model.
 */
public enum MicroprofileDecryptedPseudonym {
    ;

    public static OASDecryptedPseudonym newInstance(Asn1DecryptedPseudonym source) {
        var target = new OASDecryptedPseudonym();

        target.notationIdentifier(source.asn1Oid())
              .type(Character.toString((char) source.getType().intValue()))
              .pseudonymValue(source.getPseudonymValue())
              .diversifier(MicroprofileDiversifier.newInstance(source.getDiversifier()))
              .recipient(source.getRecipient())
              .recipientKeySetVersion(source.getRecipientKeySetVersion().toString())
              .schemeVersion(source.getSchemeVersion().toString())
              .schemeKeySetVersion(source.getSchemeKeySetVersion().toString());

        return target;
    }

}
