package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationExchange;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationTargetExchange;
import nl.logius.pepcrypto.application.microprofile.resource.AbstractMicroprofileSignedRequestExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1IdentityEnvelope;
import nl.logius.pepcrypto.lib.asn1.Asn1PseudonymEnvelope;
import nl.logius.pepcrypto.lib.crypto.PepEcSchnorrVerificationKey;
import nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrn;
import nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

class MicroprofileVerifyLinkExchange
        extends AbstractMicroprofileSignedRequestExchange<OASVerifyLinkRequest, Asn1Envelope>
        implements ApiSignedEncryptedLinkVerificationExchange {

    private final List<ApiSignedEncryptedLinkVerificationTargetExchange> linkVerificationTargets;

    private Set<String> sameIds;

    private PepSchemeKeyUrns pepSchemeKeyUrn;

    MicroprofileVerifyLinkExchange(OASVerifyLinkRequest request) {
        super(request, request::getLinkSource);
        linkVerificationTargets = request.getLinkTargetAsn1SequenceDecodables().stream()
                                         .map(MicroprofileVerifyLinkTargetExchange::new)
                                         .collect(Collectors.toList());
    }

    @Override
    public PepEcSchnorrVerificationKey getVerificationKeys() {
        return this;
    }

    @Override
    public List<ApiSignedEncryptedLinkVerificationTargetExchange> getLinkVerificationTargets() {
        return linkVerificationTargets;
    }

    @Override
    public void setLinkSourceSameIds(List<String> sameIds) {
        this.sameIds = new HashSet<>(sameIds);
    }

    @Override
    public boolean containsSameId(String id) {
        return sameIds.contains(id);
    }

    boolean isMatchingSchemeKeyUrn(String urn) {
        var schemeKeySetVersion = getSelectedSchemeKeySetVersion();
        return Optional.ofNullable(urn)
                       .map(pepSchemeKeyUrn::asPepSchemeKeyUrn)
                       .filter(PepSchemeKeyUrn::matches)
                       .filter(it -> it.isMatchingSchemeKeySetVersionString(schemeKeySetVersion))
                       .isPresent();
    }

    void setPepSchemeKeyUrn(PepSchemeKeyUrns schemeKeyUrn) {
        pepSchemeKeyUrn = schemeKeyUrn;
    }

    boolean isIdentityEnvelope() {
        return getMappedInput() instanceof Asn1IdentityEnvelope;
    }

    boolean isPseudonymEnvelope() {
        return getMappedInput() instanceof Asn1PseudonymEnvelope;
    }

}
