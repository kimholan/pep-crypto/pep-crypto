package nl.logius.pepcrypto.application.microprofile.resource.pseudonymconversion;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymRequest;
import nl.logius.pepcrypto.api.decrypted.ApiClosingKeyConversionService;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_3_2;

@ApplicationScoped
@Path("/pseudonym-conversion")
public class MicroprofilePseudonymConversionApi {

    @Inject
    @Any
    private Instance<ApiClosingKeyConversionService> serviceInstance;

    @Inject
    private MicroprofilePseudonymConversionResponseMapper responseMapper;

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public @Valid OASDecryptedPseudonymResponse processRequest(
            @MicroprofileRequiredOid({OID_2_16_528_1_1003_10_1_3_2})
            @Valid OASPseudonymRequest request) {
        var oidAnnotationLiteral = request.getAsn1SequenceDecodableOid();
        var exchange = new MicroprofilePseudonymConversionExchange(request);
        var service = serviceInstance.select(oidAnnotationLiteral).get();
        service.processExchange(exchange);

        return responseMapper.mapToResponse(exchange);
    }

}
