package nl.logius.pepcrypto.application.microprofile.resource.signeddirectencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonymRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedDirectEncryptedPseudonymService;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_6;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_6_2;

/**
 * Exposes the decryption of SignedDirectEncryptedPseudonym as a JAX-RS resource.
 */
@ApplicationScoped
@Path("/signed-direct-encrypted-pseudonym")
public class MicroprofileSignedDirectEncryptedPseudonymApi {

    @Inject
    @Any
    private Instance<ApiSignedDirectEncryptedPseudonymService> serviceInstance;

    @Inject
    private MicroprofileSignedDirectEncryptedPseudonymResponseMapper responseMapper;

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public @Valid OASDecryptedPseudonymResponse processRequest(
            @MicroprofileRequiredOid({
                    OID_2_16_528_1_1003_10_1_2_6,
                    OID_2_16_528_1_1003_10_1_2_6_2,
            })
            @Valid OASSignedDirectEncryptedPseudonymRequest request) {
        var exchange = new MicroprofileSignedDirectEncryptedPseudonymExchange(request);
        var oidAnnotationLiteral = request.getAsn1SequenceDecodableOid();
        var service = serviceInstance.select(oidAnnotationLiteral).get();
        service.processExchange(exchange);

        // Map the processed values to the response
        return responseMapper.mapToResponse(exchange);
    }

}



