package nl.logius.pepcrypto.application.microprofile.schema.verifylink;

import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Dependent
class OASVerifyLinkRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASVerifyLinkRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASVerifyLinkRequest target) {
        return target::getLinkSource;
    }

    @Override
    public boolean isValid(OASVerifyLinkRequest target, ConstraintValidatorContext context) {
        var linkSourceValid = super.isValid(target, context);
        var linkTargetValid = isLinkTargetsValid(target);

        return linkSourceValid && linkTargetValid;
    }

    private boolean isLinkTargetsValid(OASVerifyLinkRequest target) {
        var linkTargets = Optional.of(target)
                                  .map(OASVerifyLinkRequest::getLinkTargets)
                                  .stream().flatMap(Collection::stream)
                                  .map(this::decodeRawInput)
                                  .collect(Collectors.toList());
        target.setLinkTargetAsn1SequenceDecodables(linkTargets);

        return linkTargets.stream().allMatch(this::isMatchingOidType);
    }

}
