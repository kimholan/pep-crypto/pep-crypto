package nl.logius.pepcrypto.application.microprofile.resource.pseudonymmigrationimport;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationImportRequest;
import nl.logius.pepcrypto.api.decrypted.ApiMigrationImportExchange;
import nl.logius.pepcrypto.application.microprofile.resource.AbstractMicroprofileRequestExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;
import nl.logius.pepcrypto.lib.asn1.migrationintermediarypseudonym.Asn1MigrationIntermediaryPseudonym;
import nl.logius.pepcrypto.lib.crypto.PepCrypto;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationTargetPrivateKey;
import org.bouncycastle.math.ec.ECPoint;

import static nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationTargetPrivateKey.epMigrationTargetPrivateKey;
import static nl.logius.pepcrypto.lib.pem.PemEcPrivateKeyHeader.TARGET_MIGRANT;
import static nl.logius.pepcrypto.lib.pem.PemEcPrivateKeyHeader.TARGET_MIGRANT_KEY_SET_VERSION;

class MicroprofilePseudonymMigrationImportExchange
        extends AbstractMicroprofileRequestExchange<OASPseudonymMigrationImportRequest, Asn1MigrationIntermediaryPseudonym>
        implements ApiMigrationImportExchange {

    MicroprofilePseudonymMigrationImportExchange(OASPseudonymMigrationImportRequest request) {
        super(request, request::getMigrationIntermediaryPseudonym);
    }

    @Override
    public ECPoint getEcPoint() {
        return PepCrypto.decodeEcPoint(getMappedInput().getPseudonymValue());
    }

    @Override
    public ECPoint getDecryptedPseudonymResultEcPoint() {
        return getConvertedEcPoint();
    }

    @Override
    public PepEpMigrationTargetPrivateKey getSelectedConversionKey() {
        return epMigrationTargetPrivateKey(getMigrationTargetKey().getPrivateKey());
    }

    @Override
    public Asn1Pseudonym getDecryptedPseudonymResultAsn1Pseudonym() {
        return getMappedInput();
    }

    @Override
    public PepRecipientKeyId getDecryptedPseudonymResultPepRecipientKeyId() {
        var targetRecipient = getMigrationTargetKey().getSpecifiedHeader(TARGET_MIGRANT);
        var targetRecipientKeySetVersion = getMigrationTargetKey().getSpecifiedHeader(TARGET_MIGRANT_KEY_SET_VERSION);

        return getMappedInput().asTargetAsn1RecipientKeyId()
                               .recipient(targetRecipient)
                               .recipientKeySetVersion(targetRecipientKeySetVersion);
    }

}
