package nl.logius.pepcrypto.application.microprofile.resource.pseudonymmigrationexport;

import generated.nl.logius.pepcrypto.openapi.model.OASMigrationIntermediaryPseudonymResponse;
import nl.logius.pepcrypto.application.microprofile.schema.decryptedpseudonym.MicroprofileDecryptedPseudonym;
import nl.logius.pepcrypto.application.microprofile.schema.migrationintermediarypseudonym.MicroprofileMigrationIntermediaryPseudonym;
import nl.logius.pepcrypto.lib.asn1.migrationintermediarypseudonym.Asn1MigrationIntermediaryPseudonym;

import javax.enterprise.context.ApplicationScoped;

import static nl.logius.pepcrypto.application.microprofile.exception.MicroprofileExceptionDetail.ENCODE_MIGRATION_INTERMEDIARY_PSEUDONYM_AS_DER;

@ApplicationScoped
class MicroprofilePseudonymMigrationExportResponseMapper {

    OASMigrationIntermediaryPseudonymResponse mapToResponse(MicroprofilePseudonymMigrationExportExchange exchange) {
        var sourcePseudonym = exchange.getMappedInput();
        var decodedInput = MicroprofileDecryptedPseudonym.newInstance(sourcePseudonym);

        // Export conversion result
        var convertedEcPoint = exchange.getConvertedEcPoint();
        var migrationId = exchange.getMigrationSourceMigrationId();
        var targetRecipientKeyId = exchange.getMigrationSourceTargetRecipientKeyId();
        var intermediary = Asn1MigrationIntermediaryPseudonym.forMigration(sourcePseudonym, convertedEcPoint, targetRecipientKeyId, migrationId);
        var decodedMigrationIntermediaryPseudonym = MicroprofileMigrationIntermediaryPseudonym.newInstance(intermediary);
        var migrationIntermediaryPseudonym = ENCODE_MIGRATION_INTERMEDIARY_PSEUDONYM_AS_DER.call(intermediary::encodeByteArray);

        // Response
        var response = new OASMigrationIntermediaryPseudonymResponse();
        response.migrationIntermediaryPseudonym(migrationIntermediaryPseudonym)
                .decodedMigrationIntermediaryPseudonym(decodedMigrationIntermediaryPseudonym)
                .decodedInput(decodedInput);

        return response;
    }

}
