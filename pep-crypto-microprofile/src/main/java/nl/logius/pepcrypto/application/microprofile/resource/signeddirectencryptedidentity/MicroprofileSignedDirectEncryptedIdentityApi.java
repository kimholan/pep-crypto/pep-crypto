package nl.logius.pepcrypto.application.microprofile.resource.signeddirectencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedIdentityResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedIdentityRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedDirectEncryptedIdentityService;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_10_2;

/**
 * Exposes the decryption of SignedDirectEncryptedIdentity as a JAX-RS resource.
 */
@ApplicationScoped
@Path("/signed-direct-encrypted-identity")
public class MicroprofileSignedDirectEncryptedIdentityApi {

    @Inject
    @Any
    private Instance<ApiSignedDirectEncryptedIdentityService> serviceInstance;

    @Inject
    private MicroprofileSignedDirectEncryptedIdentityResponseMapper responseMapper;

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public @Valid OASDecryptedIdentityResponse processRequest(
            @MicroprofileRequiredOid(OID_2_16_528_1_1003_10_1_2_10_2)
            @Valid OASSignedDirectEncryptedIdentityRequest request) {
        var oidAnnotationLiteral = request.getAsn1SequenceDecodableOid();
        var exchange = new MicroprofileSignedDirectEncryptedIdentityExchange(request);
        var service = serviceInstance.select(oidAnnotationLiteral).get();
        service.processExchange(exchange);

        // Map the processed values to the response
        return responseMapper.mapToResponse(exchange);
    }

}



