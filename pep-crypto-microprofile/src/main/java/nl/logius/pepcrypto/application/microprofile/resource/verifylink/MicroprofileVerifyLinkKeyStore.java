package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import nl.logius.pepcrypto.api.event.ApiEvent;
import nl.logius.pepcrypto.application.microprofile.keystore.MicroprofileAbstractKeyStore;
import nl.logius.pepcrypto.application.microprofile.keystore.MicroprofileIdentityDecryptionKeySelector;
import nl.logius.pepcrypto.application.microprofile.keystore.MicroprofilePseudonymDecryptionKeySelector;
import nl.logius.pepcrypto.application.microprofile.keystore.MicroprofileSchemeKeySelector;
import nl.logius.pepcrypto.lib.lang.PepExceptionCollector;
import nl.logius.pepcrypto.lib.pem.PemEcPrivateKey;
import nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import static nl.logius.pepcrypto.api.event.ApiEvent.ApiEventPhaseType.AFTER;
import static nl.logius.pepcrypto.api.event.ApiEvent.ApiEventType.ASN1_MAPPING;
import static nl.logius.pepcrypto.application.microprofile.exception.MicroprofileExceptionDetail.MICROPROFILE_MODULE;
import static nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns.SCHEME_KEY_IPP;
import static nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns.SCHEME_KEY_PPP;

@ApplicationScoped
class MicroprofileVerifyLinkKeyStore
        extends MicroprofileAbstractKeyStore {

    @Inject
    private MicroprofilePseudonymDecryptionKeySelector pseudonymDecryptionKeySelector;

    @Inject
    private MicroprofileIdentityDecryptionKeySelector identityDecryptionKeySelector;

    @Inject
    private MicroprofileSchemeKeySelector schemeKeySelector;

    void processKeySelection(@Observes @ApiEvent(value = ASN1_MAPPING, phase = AFTER) MicroprofileVerifyLinkExchange exchange) {
        var collector = new PepExceptionCollector();

        collector.ignore(() -> requireValidSchemeKeyNames(exchange));
        collector.ignore(() -> requireValidSchemeKeyStructure(exchange));
        collector.ignore(() -> processEnvelopeType(exchange));
        collector.ignore(() -> parseSchemeKeys(exchange));
        collector.ignore(() -> parseServiceProviderKeys(exchange));
        collector.ignore(() -> requireUniqueRecipient(exchange));
        collector.ignore(() -> processSchemeKeySelection(exchange));
        collector.ignore(() -> processServiceProviderKeySelection(exchange));

        collector.requireNoExceptions(MICROPROFILE_MODULE);
    }

    private void processSchemeKeySelection(MicroprofileVerifyLinkExchange exchange) {
        var parsedSchemeKeys = exchange.getParsedSchemeKeys();
        var selectedSchemeKey = schemeKeySelector.selectSchemeKey(parsedSchemeKeys, exchange::isMatchingSchemeKeyUrn);

        exchange.setSelectedSchemeKey(selectedSchemeKey);
    }

    private void processEnvelopeType(MicroprofileVerifyLinkExchange exchange) {
        var identityEnvelope = exchange.isIdentityEnvelope();
        var pseudonymEnvelope = exchange.isPseudonymEnvelope();

        MICROPROFILE_MODULE.requireTrue(identityEnvelope ^ pseudonymEnvelope);

        var pepSchemeKeyUrns = getPepSchemeKeyUrns(exchange);
        exchange.setPepSchemeKeyUrn(pepSchemeKeyUrns);
    }

    private PepSchemeKeyUrns getPepSchemeKeyUrns(MicroprofileVerifyLinkExchange exchange) {
        PepSchemeKeyUrns pepSchemeKeyUrns;

        if (exchange.isIdentityEnvelope()) {
            pepSchemeKeyUrns = SCHEME_KEY_IPP;
        } else if (exchange.isPseudonymEnvelope()) {
            pepSchemeKeyUrns = SCHEME_KEY_PPP;
        } else {
            pepSchemeKeyUrns = null;
        }

        return pepSchemeKeyUrns;
    }

    private void processServiceProviderKeySelection(MicroprofileVerifyLinkExchange exchange) {
        var decryptionKey = getDecryptionKey(exchange);

        exchange.setSelectedDecryptionKey(decryptionKey);
    }

    private PemEcPrivateKey getDecryptionKey(MicroprofileVerifyLinkExchange exchange) {
        PemEcPrivateKey decryptionKey;

        if (exchange.isIdentityEnvelope()) {
            decryptionKey = getIdentityDecryptionKeys(exchange);
        } else if (exchange.isPseudonymEnvelope()) {
            decryptionKey = getPseudonymDecryptionKeys(exchange);
        } else {
            decryptionKey = null;
        }
        return decryptionKey;
    }

    private PemEcPrivateKey getIdentityDecryptionKeys(MicroprofileVerifyLinkExchange exchange) {
        var parsedServiceProviderKeys = exchange.getParsedServiceProviderKeys();
        var selectedDecryptionKeyRecipientKeyId = exchange.getSelectedDecryptionKeyRecipientKeyId();
        var decryptionKeys = identityDecryptionKeySelector.filterByRecipientKeyId(parsedServiceProviderKeys, selectedDecryptionKeyRecipientKeyId);
        return identityDecryptionKeySelector.requireUniqueMatch(decryptionKeys);
    }

    private PemEcPrivateKey getPseudonymDecryptionKeys(MicroprofileVerifyLinkExchange exchange) {
        var parsedServiceProviderKeys = exchange.getParsedServiceProviderKeys();
        var selectedDecryptionKeyRecipientKeyId = exchange.getSelectedDecryptionKeyRecipientKeyId();
        var decryptionKeys = pseudonymDecryptionKeySelector.filterByRecipientKeyId(parsedServiceProviderKeys, selectedDecryptionKeyRecipientKeyId);
        return pseudonymDecryptionKeySelector.requireUniqueMatch(decryptionKeys);
    }

}

