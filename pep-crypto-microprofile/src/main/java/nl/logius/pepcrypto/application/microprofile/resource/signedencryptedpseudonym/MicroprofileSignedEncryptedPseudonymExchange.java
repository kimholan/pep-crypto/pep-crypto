package nl.logius.pepcrypto.application.microprofile.resource.signedencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonymRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedPseudonymExchange;
import nl.logius.pepcrypto.application.microprofile.resource.AbstractMicroprofileSignedRequestExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1PseudonymEnvelope;
import nl.logius.pepcrypto.lib.crypto.PepEcSchnorrVerificationKey;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.key.PepEpClosingPrivateKey;
import nl.logius.pepcrypto.lib.crypto.key.PepEpDecryptionPrivateKey;
import nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrn;
import org.bouncycastle.math.ec.ECPoint;

import java.util.Optional;

import static nl.logius.pepcrypto.lib.crypto.key.PepEpClosingPrivateKey.epClosingPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpDecryptionPrivateKey.epDecryptionPrivateKey;
import static nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns.SCHEME_KEY_PPP;

/**
 * Carries the processing state of a message exchange.
 */
class MicroprofileSignedEncryptedPseudonymExchange
        extends AbstractMicroprofileSignedRequestExchange<OASSignedEncryptedPseudonymRequest, Asn1PseudonymEnvelope>
        implements PepEcSchnorrVerificationKey,
                   ApiSignedEncryptedPseudonymExchange {

    MicroprofileSignedEncryptedPseudonymExchange(OASSignedEncryptedPseudonymRequest request) {
        super(request, request::getSignedEncryptedPseudonym);
    }

    @Override
    public PepEcSchnorrVerificationKey getVerificationKeys() {
        return this;
    }

    @Override
    public PepEpClosingPrivateKey getClosingPepPrivateKey() {
        return epClosingPrivateKey(getSelectedClosingKey().getPrivateKey());
    }

    @Override
    public PepEpDecryptionPrivateKey getDecryptionPepPrivateKey() {
        return epDecryptionPrivateKey(getSelectedDecryptionKey().getPrivateKey());
    }

    @Override
    public Asn1Pseudonym getDecryptedPseudonymResultAsn1Pseudonym() {
        return getMappedInput().asn1Pseudonym();
    }

    @Override
    public PepRecipientKeyId getDecryptedPseudonymResultPepRecipientKeyId() {
        var selectedClosingKey = getSelectedClosingKey();
        var targetClosingKeyRecipientKeySetVersion = selectedClosingKey.getRecipientKeySetVersion();
        var targetClosingKeySchemeKeySetVersion = selectedClosingKey.getSchemeKeySetVersion();
        var recipientKeyId = getMappedInput().asn1RecipientKeyId();

        return recipientKeyId.recipientKeySetVersion(targetClosingKeyRecipientKeySetVersion)
                             .schemeKeySetVersion(targetClosingKeySchemeKeySetVersion);
    }

    @Override
    public ECPoint getDecryptedPseudonymResultEcPoint() {
        return getDecryptedEcPoint();
    }

    public boolean isMatchingSchemeKeyUrn(String urn) {
        var schemeKeySetVersion = getSelectedSchemeKeySetVersion();
        return Optional.ofNullable(urn)
                       .map(SCHEME_KEY_PPP::asPepSchemeKeyUrn)
                       .filter(PepSchemeKeyUrn::matches)
                       .filter(it -> it.isMatchingSchemeKeySetVersionString(schemeKeySetVersion))
                       .isPresent();
    }

    PepRecipientKeyId getTargetClosingKeyAsRecipientKeyId() {
        var recipientKeyId = getSelectedDecryptionKeyRecipientKeyId();
        var request = getRequest();
        var targetRecipientKeySetVersion = request.getTargetClosingKey();
        var targetSchemeKeySetVersion = request.getTargetClosingKeySchemeKeySetVersion();
        return complementRecipientKeyId(recipientKeyId, targetRecipientKeySetVersion, targetSchemeKeySetVersion);
    }

}
