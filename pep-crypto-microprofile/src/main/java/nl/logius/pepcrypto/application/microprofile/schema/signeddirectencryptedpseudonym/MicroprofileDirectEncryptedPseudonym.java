package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASDirectEncryptedPseudonym;
import nl.logius.pepcrypto.application.microprofile.schema.diversifier.MicroprofileDiversifier;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectPseudonymEnvelope;

/**
 * Maps data to output model.
 */
enum MicroprofileDirectEncryptedPseudonym {
    ;

    static OASDirectEncryptedPseudonym newInstance(Asn1DirectPseudonymEnvelope source) {
        var target = new OASDirectEncryptedPseudonym();
        var points = source.asn1EcPointValues();
        var pepRecipientKeyId = source.asn1RecipientKeyId();

        target.creator(source.asn1Body().getCreator())
              .diversifier(MicroprofileDiversifier.newInstance(source.asn1PseudonymDiversifier()))
              .points(points)
              .type(Character.toString((char) source.asn1PseudonymType().intValue()))
              .authorizedParty(source.asn1AuthorizedParty())
              .notationIdentifier(source.asn1BodyOid())
              .recipient(pepRecipientKeyId.getRecipient())
              .recipientKeySetVersion(pepRecipientKeyId.getRecipientKeySetVersion().toString())
              .schemeVersion(pepRecipientKeyId.getSchemeVersion().toString())
              .schemeKeySetVersion(pepRecipientKeyId.getSchemeKeySetVersion().toString())

        ;

        return target;
    }

}

