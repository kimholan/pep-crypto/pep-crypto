package nl.logius.pepcrypto.application.microprofile.resource;

import nl.logius.pepcrypto.application.microprofile.keystore.MicroprofileDecryptionKeystoreExchange;
import nl.logius.pepcrypto.application.microprofile.schema.MicroprofileRequest;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1RecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.PepEcPointTriplet;
import nl.logius.pepcrypto.lib.crypto.PepEcSignature;

import java.math.BigInteger;
import java.util.function.Supplier;

public class AbstractMicroprofileSignedRequestExchange<R extends MicroprofileRequest, S extends Asn1Envelope>
        extends AbstractMicroprofileRequestExchange<R, S>
        implements MicroprofileDecryptionKeystoreExchange {

    protected AbstractMicroprofileSignedRequestExchange(R request, Supplier<byte[]> rawInputSupplier) {
        super(request, rawInputSupplier);
    }

    public PepEcPointTriplet getEncryptedEcPointTriplet() {
        return getMappedInput().asn1EcPointTriplet();
    }

    @Override
    public BigInteger getSelectedSchemeKeySetVersion() {
        return getMappedInput().asn1RecipientKeyId().getSchemeKeySetVersion();
    }

    @Override
    public Asn1RecipientKeyId getSelectedDecryptionKeyRecipientKeyId() {
        return new Asn1RecipientKeyId(getMappedInput().asn1RecipientKeyId());
    }

    public PepEcSignature getSignature() {
        return getMappedInput().asn1Signature();
    }

    public byte[] getSignedData() {
        return getMappedInput().asn1SignedData();
    }

}
