package nl.logius.pepcrypto.application.microprofile.resource.pseudonymmigrationexport;

import generated.nl.logius.pepcrypto.openapi.model.OASMigrationIntermediaryPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationExportRequest;
import nl.logius.pepcrypto.api.decrypted.ApiMigrationExportService;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_3_2;

@ApplicationScoped
@Path("/pseudonym-migration-export")
public class MicroprofilePseudonymMigrationExportApi {

    @Inject
    @Any
    private Instance<ApiMigrationExportService> serviceInstance;

    @Inject
    private MicroprofilePseudonymMigrationExportResponseMapper responseMapper;

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public @Valid OASMigrationIntermediaryPseudonymResponse processRequest(
            @MicroprofileRequiredOid(OID_2_16_528_1_1003_10_1_3_2)
            @Valid OASPseudonymMigrationExportRequest request) {
        var oidAnnotationLiteral = request.getAsn1SequenceDecodableOid();
        var exchange = new MicroprofilePseudonymMigrationExportExchange(request);
        var service = serviceInstance.select(oidAnnotationLiteral).get();
        service.processExchange(exchange);

        return responseMapper.mapToResponse(exchange);
    }

}
