package nl.logius.pepcrypto.application.microprofile.resource.pseudonymmigrationimport;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationImportRequest;
import nl.logius.pepcrypto.api.decrypted.ApiMigrationImportService;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_3_3;

@ApplicationScoped
@Path("/pseudonym-migration-import")
public class MicroprofilePseudonymMigrationImportApi {

    @Inject
    @Any
    private Instance<ApiMigrationImportService> serviceInstance;

    @Inject
    private MicroprofilePseudonymMigrationImportResponseMapper responseMapper;

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public @Valid OASDecryptedPseudonymResponse processRequest(
            @MicroprofileRequiredOid({OID_2_16_528_1_1003_10_1_3_3})
            @Valid OASPseudonymMigrationImportRequest request) {
        var oidAnnotationLiteral = request.getAsn1SequenceDecodableOid();
        var exchange = new MicroprofilePseudonymMigrationImportExchange(request);
        var service = serviceInstance.select(oidAnnotationLiteral).get();
        service.processExchange(exchange);

        return responseMapper.mapToResponse(exchange);
    }

}
