package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedIdentity;
import nl.logius.pepcrypto.application.microprofile.schema.signaturevalue.MicroprofileSignatureValue;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;
import nl.logius.pepcrypto.lib.asn1.Asn1IdentityEnvelope;

/**
 * Maps data to output model.
 */
public enum MicroprofileSignedEncryptedIdentity {
    ;

    public static OASSignedEncryptedIdentity newInstance(Asn1Envelope source) {
        return newInstance((Asn1IdentityEnvelope) source);
    }

    public static OASSignedEncryptedIdentity newInstance(Asn1IdentityEnvelope source) {
        var target = new OASSignedEncryptedIdentity();

        target.notationIdentifier(source.asn1Oid())
              .signedEI(MicroprofileSignedEI.newInstance(source))
              .signatureValue(MicroprofileSignatureValue.newInstance(source));

        return target;
    }

}
