package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDEP;
import nl.logius.pepcrypto.application.microprofile.schema.extraelement.MicroprofileExtraElement;
import nl.logius.pepcrypto.lib.asn1.Asn1DirectPseudonymEnvelope;
import nl.logius.pepcrypto.lib.asn1.signeddirectencryptedpseudonymv2.Asn1SignedDirectEncryptedPseudonymv2Envelope;

/**
 * Maps data to output model.
 */
enum MicroprofileSignedDEP {
    ;

    static OASSignedDEP newInstance(Asn1DirectPseudonymEnvelope source) {
        var target = new OASSignedDEP();
        target.setExtraElements(null);

        var auditElement = source.asn1AuditElement();
        var signingKeyVersion = source.asn1SigningKeyVersion();

        target.auditElement(auditElement)
              .directEncryptedPseudonym(MicroprofileDirectEncryptedPseudonym.newInstance(source))
              .signingKeyVersion(signingKeyVersion.toString());

        if (source instanceof Asn1SignedDirectEncryptedPseudonymv2Envelope) {
            var v2Source = (Asn1SignedDirectEncryptedPseudonymv2Envelope) source;
            var signedBody = v2Source.asn1SignedBody();

            target.issuanceDate(signedBody.getIssuanceDate())
                  .extraElements(MicroprofileExtraElement.newInstance(signedBody));
        }

        return target;
    }

}
