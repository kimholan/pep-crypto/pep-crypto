package nl.logius.pepcrypto.application.microprofile.schema.identitydecryptionresult;

import generated.nl.logius.pepcrypto.openapi.model.OASIdentityDecryptionResult;
import nl.logius.pepcrypto.lib.crypto.PepIdentityOaepDecoded;

/**
 * Maps data to output model.
 */
public enum MicroprofileIdentityDecryptionResult {
    ;

    public static OASIdentityDecryptionResult newInstance(PepIdentityOaepDecoded source) {
        var target = new OASIdentityDecryptionResult();

        target.version(source.getVersion())
              .identifier(source.getIdentifier())
              .type(source.getType())
              .length(source.getLength())
              .bytes(source.getBytes());

        return target;
    }

}
