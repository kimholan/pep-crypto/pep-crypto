package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedpseudonymmigration;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonymMigrationRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASSignedDirectEncryptedPseudonymMigrationRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASSignedDirectEncryptedPseudonymMigrationRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASSignedDirectEncryptedPseudonymMigrationRequest target) {
        return target::getSignedDirectEncryptedPseudonym;
    }

}
