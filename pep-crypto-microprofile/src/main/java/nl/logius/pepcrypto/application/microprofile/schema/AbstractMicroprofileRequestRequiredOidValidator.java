package nl.logius.pepcrypto.application.microprofile.schema;

import nl.logius.pepcrypto.api.ApiAsn1SequenceDecodable;
import nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType;
import nl.logius.pepcrypto.api.oid.Asn1SequenceDecodable;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileAsn1SequenceDecoder;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;

import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;
import java.util.function.Supplier;

public abstract class AbstractMicroprofileRequestRequiredOidValidator<T extends MicroprofileRequest>
        implements ConstraintValidator<MicroprofileRequiredOid, T> {

    @Inject
    private MicroprofileAsn1SequenceDecoder asn1SequenceDecoder;

    private ApiOIDType[] supportedOids;

    @Override
    public void initialize(MicroprofileRequiredOid constraintAnnotation) {
        supportedOids = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(T target, ConstraintValidatorContext context) {
        var rawInputSupplier = getRawInputSupplier(target);
        var decodable = decodeRawInput(rawInputSupplier);
        target.setAsn1SequenceDecodable(decodable);
        return isMatchingOidType(decodable);
    }

    protected boolean isMatchingOidType(ApiAsn1SequenceDecodable decodable) {
        var matchingOidType = decodable.getMatchingOidType(supportedOids);
        return Optional.ofNullable(matchingOidType).isPresent();
    }

    protected ApiAsn1SequenceDecodable decodeRawInput(byte[] rawInput) {
        var decodable = new Asn1SequenceDecodable(rawInput);
        asn1SequenceDecoder.decodeRawInput(decodable);
        return decodable;
    }

    protected ApiAsn1SequenceDecodable decodeRawInput(Supplier<byte[]> rawInputSupplier) {
        return decodeRawInput(rawInputSupplier.get());
    }

    protected abstract Supplier<byte[]> getRawInputSupplier(T target);

}


