package nl.logius.pepcrypto.application.microprofile.schema.pseudonymmigrationimport;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationImportRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASPseudonymMigrationImportRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASPseudonymMigrationImportRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASPseudonymMigrationImportRequest target) {
        return target::getMigrationIntermediaryPseudonym;
    }

}
