package nl.logius.pepcrypto.application.microprofile.resource;

import nl.logius.pepcrypto.application.microprofile.schema.MicroprofileRequest;
import nl.logius.pepcrypto.lib.asn1.Asn1PepType;
import nl.logius.pepcrypto.lib.asn1.Asn1RecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.pem.PemEcPrivateKey;
import org.bouncycastle.math.ec.ECPoint;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.CONVERTED_EC_POINT;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.DECRYPTED_EC_POINT;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.MAPPED_INPUT;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.MIGRATION_SOURCE_KEY;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.MIGRATION_TARGET_KEY;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.PARSED_SCHEME_KEYS;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.PARSED_SERVICE_PROVIDER_KEYS;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.RAW_INPUT_SUPPLIER;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.REQUEST;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.SELECTED_CLOSING_KEY;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.SELECTED_DECRYPTION_KEY;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.SELECTED_DIRECT_RECEIVE_KEY;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.SELECTED_SCHEME_KEY;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.SOURCE_CLOSING_KEY;
import static nl.logius.pepcrypto.application.microprofile.resource.ExchangeProperties.TARGET_CLOSING_KEY;

public abstract class AbstractMicroprofileRequestExchange<S extends MicroprofileRequest, T extends Asn1PepType>
        implements MicroprofileRequestExchange<S, T> {

    private final Map<ExchangeProperties, Object> properties;

    protected AbstractMicroprofileRequestExchange(S request, Supplier<byte[]> rawInputSupplier) {
        properties = new EnumMap<>(ExchangeProperties.class);
        putProperty(REQUEST, request);
        putProperty(RAW_INPUT_SUPPLIER, rawInputSupplier);
    }

    @Override
    public S getRequest() {
        return getProperty(REQUEST);
    }

    @Override
    public byte[] getRawInput() {
        return getRawInputSupplier().get();
    }

    @Override
    public List<String> getRawServiceProviderKeys() {
        return Optional.ofNullable(getRawServiceProviderKeysSupplier())
                       .map(Supplier::get)
                       .orElse(null);
    }

    @Override
    public T getMappedInput() {
        return getProperty(MAPPED_INPUT);
    }

    @Override
    public void setMappedInput(T mappedInput) {
        putProperty(MAPPED_INPUT, mappedInput);
    }

    @Override
    public List<PemEcPrivateKey> getParsedServiceProviderKeys() {
        return getProperty(PARSED_SERVICE_PROVIDER_KEYS);
    }

    @Override
    public void setParsedServiceProviderKeys(List<PemEcPrivateKey> parsedServiceProviderKeys) {
        putProperty(PARSED_SERVICE_PROVIDER_KEYS, parsedServiceProviderKeys);
    }

    public Map<String, byte[]> getRawSchemeKeys() {
        return Optional.ofNullable(getRawSchemeKeysSupplier())
                       .map(Supplier::get)
                       .orElse(null);
    }

    public ECPoint getConvertedEcPoint() {
        return getProperty(CONVERTED_EC_POINT);
    }

    public void setConvertedEcPoint(ECPoint convertedEcPoint) {
        putProperty(CONVERTED_EC_POINT, convertedEcPoint);
    }

    public PemEcPrivateKey getSourceClosingKey() {
        return getProperty(SOURCE_CLOSING_KEY);
    }

    public void setSourceClosingKey(PemEcPrivateKey sourceClosingKey) {
        putProperty(SOURCE_CLOSING_KEY, sourceClosingKey);
    }

    public PemEcPrivateKey getTargetClosingKey() {
        return getProperty(TARGET_CLOSING_KEY);
    }

    public void setTargetClosingKey(PemEcPrivateKey targetClosingKey) {
        putProperty(TARGET_CLOSING_KEY, targetClosingKey);
    }

    public PemEcPrivateKey getMigrationTargetKey() {
        return getProperty(MIGRATION_TARGET_KEY);
    }

    public void setMigrationTargetKey(PemEcPrivateKey migrationTargetKey) {
        putProperty(MIGRATION_TARGET_KEY, migrationTargetKey);
    }

    public PemEcPrivateKey getMigrationSourceKey() {
        return getProperty(MIGRATION_SOURCE_KEY);
    }

    public void setMigrationSourceKey(PemEcPrivateKey migrationSourceKey) {
        putProperty(MIGRATION_SOURCE_KEY, migrationSourceKey);
    }

    public ECPoint getDecryptedEcPoint() {
        return getProperty(DECRYPTED_EC_POINT);
    }

    public void setDecryptedEcPoint(ECPoint decryptedEcPoint) {
        putProperty(DECRYPTED_EC_POINT, decryptedEcPoint);
    }

    public ECPoint getSelectedSchemeKey() {
        return getProperty(SELECTED_SCHEME_KEY);
    }

    public void setSelectedSchemeKey(ECPoint selectedSchemeKey) {
        putProperty(SELECTED_SCHEME_KEY, selectedSchemeKey);
    }

    public PemEcPrivateKey getSelectedDecryptionKey() {
        return getProperty(SELECTED_DECRYPTION_KEY);
    }

    public void setSelectedDecryptionKey(PemEcPrivateKey selectedDecryptionKey) {
        putProperty(SELECTED_DECRYPTION_KEY, selectedDecryptionKey);
    }

    public Map<String, ECPoint> getParsedSchemeKeys() {
        return getProperty(PARSED_SCHEME_KEYS);
    }

    public void setParsedSchemeKeys(Map<String, ECPoint> parsedSchemeKeys) {
        putProperty(PARSED_SCHEME_KEYS, parsedSchemeKeys);
    }

    public PemEcPrivateKey getSelectedClosingKey() {
        return getProperty(SELECTED_CLOSING_KEY);
    }

    public void setSelectedClosingKey(PemEcPrivateKey selectedClosingKey) {
        putProperty(SELECTED_CLOSING_KEY, selectedClosingKey);
    }

    public PemEcPrivateKey getSelectedDirectReceiveKey() {
        return getProperty(SELECTED_DIRECT_RECEIVE_KEY);
    }

    public void setSelectedDirectReceiveKey(PemEcPrivateKey selectedDirectReceiveKey) {
        putProperty(SELECTED_DIRECT_RECEIVE_KEY, selectedDirectReceiveKey);
    }

    public ECPoint getSchemePublicKey() {
        return getSelectedSchemeKey();
    }

    public ECPoint getRecipientPublicKey() {
        return getSelectedDecryptionKey().getPublicKey();
    }

    protected final void putProperty(ExchangeProperties key, Object value) {
        properties.put(key, value);
    }

    protected final <V> V getProperty(ExchangeProperties key) {
        return (V) properties.get(key);
    }

    Supplier<byte[]> getRawInputSupplier() {
        return getProperty(RAW_INPUT_SUPPLIER);
    }

    Supplier<List<String>> getRawServiceProviderKeysSupplier() {
        return Optional.ofNullable(getRequest())
                       .map(MicroprofileRequest::rawServiceProviderKeysSupplier)
                       .orElse(null);
    }

    Supplier<Map<String, byte[]>> getRawSchemeKeysSupplier() {
        return Optional.ofNullable(getRequest())
                       .map(MicroprofileRequest::rawSchemeKeysSupplier)
                       .orElse(null);
    }

    protected PepRecipientKeyId complementRecipientKeyId(Asn1RecipientKeyId recipientKeyId, String recipientKeySetVersion, String schemeKeySetVersion) {
        var newRecipientKeyId = Optional.ofNullable(recipientKeySetVersion)
                                        .map(recipientKeyId::recipientKeySetVersion)
                                        .orElse(null);

        if (Objects.nonNull(schemeKeySetVersion)) {
            var applyToRecipientKeyId = Optional.ofNullable(newRecipientKeyId)
                                                .orElse(recipientKeyId);
            newRecipientKeyId = applyToRecipientKeyId.schemeKeySetVersion(schemeKeySetVersion);
        }

        return newRecipientKeyId;
    }

}
