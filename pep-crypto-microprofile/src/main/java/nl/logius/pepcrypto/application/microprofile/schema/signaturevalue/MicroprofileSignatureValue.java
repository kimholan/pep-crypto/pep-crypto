package nl.logius.pepcrypto.application.microprofile.schema.signaturevalue;

import generated.nl.logius.pepcrypto.openapi.model.OASSignatureValue;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;

/**
 * Maps data to output model.
 */
public enum MicroprofileSignatureValue {
    ;

    public static OASSignatureValue newInstance(Asn1Envelope source) {
        var target = new OASSignatureValue();

        var asn1Signature = source.asn1Signature();
        target.signatureType(source.asn1SignatureOid())
              .r(asn1Signature.asn1RByteArray())
              .s(asn1Signature.asn1SByteArray());

        return target;
    }

}
