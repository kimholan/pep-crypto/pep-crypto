package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASEncryptedIdentity;
import nl.logius.pepcrypto.lib.asn1.Asn1IdentityEnvelope;

/**
 * Maps data to output model.
 */
enum MicroprofileEncryptedIdentity {
    ;

    static OASEncryptedIdentity newInstance(Asn1IdentityEnvelope source) {
        var target = new OASEncryptedIdentity();
        var points = source.asn1EcPointValues();
        var pepRecipientKeyId = source.asn1RecipientKeyId();

        target.notationIdentifier(source.asn1BodyOid())
              .creator(source.asn1Body().getCreator())
              .points(points)
              .recipient(pepRecipientKeyId.getRecipient())
              .recipientKeySetVersion(pepRecipientKeyId.getRecipientKeySetVersion().toString())
              .schemeVersion(pepRecipientKeyId.getSchemeVersion().toString())
              .schemeKeySetVersion(pepRecipientKeyId.getSchemeKeySetVersion().toString());

        return target;
    }

}

