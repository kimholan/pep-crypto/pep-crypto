package nl.logius.pepcrypto.application.microprofile.resource.pseudonymmigrationexport;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationExportRequest;
import nl.logius.pepcrypto.api.decrypted.ApiMigrationExportExchange;
import nl.logius.pepcrypto.application.microprofile.keystore.MicroprofileServiceProviderKeyStoreExchange;
import nl.logius.pepcrypto.application.microprofile.resource.AbstractMicroprofileRequestExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1RecipientKeyId;
import nl.logius.pepcrypto.lib.asn1.decryptedpseudonym.Asn1DecryptedPseudonym;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationSourcePrivateKey;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.util.Optional;

import static nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationSourcePrivateKey.epMigrationSourcePrivateKey;
import static nl.logius.pepcrypto.lib.pem.PemEcPrivateKeyHeader.MIGRATION_ID;
import static nl.logius.pepcrypto.lib.pem.PemEcPrivateKeyHeader.TARGET_MIGRANT;
import static nl.logius.pepcrypto.lib.pem.PemEcPrivateKeyHeader.TARGET_MIGRANT_KEY_SET_VERSION;

class MicroprofilePseudonymMigrationExportExchange
        extends AbstractMicroprofileRequestExchange<OASPseudonymMigrationExportRequest, Asn1DecryptedPseudonym>
        implements ApiMigrationExportExchange, MicroprofileServiceProviderKeyStoreExchange {

    MicroprofilePseudonymMigrationExportExchange(OASPseudonymMigrationExportRequest request) {
        super(request, request::getPseudonym);
    }

    @Override
    public ECPoint getEcPoint() {
        return getMappedInput().asn1PseudonymValueAsEcPoint();
    }

    @Override
    public PepEpMigrationSourcePrivateKey getSelectedConversionKey() {
        return epMigrationSourcePrivateKey(getMigrationSourceKey().getPrivateKey());
    }

    String getMigrationSourceTargetMigrant() {
        return getMigrationSourceKey().getSpecifiedHeader(TARGET_MIGRANT);
    }

    BigInteger getMigrationSourceTargetKeySetVersion() {
        return new BigInteger(getMigrationSourceKey().getSpecifiedHeader(TARGET_MIGRANT_KEY_SET_VERSION));
    }

    String getMigrationSourceMigrationId() {
        return getMigrationSourceKey().getSpecifiedHeader(MIGRATION_ID);
    }

    String getRequestMigrationId() {
        return getRequest().getMigrationID();
    }

    boolean isMigrationTargetSelectionInvalid() {
        return getRequestTargetMigrant() == null ^ getRequestTargetMigrantKeySetVersion() == null;
    }

    String getRequestTargetMigrant() {
        return getRequest().getTargetMigrant();
    }

    BigInteger getRequestTargetMigrantKeySetVersion() {
        return Optional.of(getRequest())
                       .map(OASPseudonymMigrationExportRequest::getTargetMigrantKeySetVersion)
                       .map(BigInteger::new)
                       .orElse(null);
    }

    PepRecipientKeyId getMigrationSourceTargetRecipientKeyId() {
        var recipient = getMigrationSourceTargetMigrant();
        var recipientKeySetVersion = getMigrationSourceTargetKeySetVersion();
        return new Asn1RecipientKeyId(getMigrationSourceKey(), recipient, recipientKeySetVersion);
    }

}
