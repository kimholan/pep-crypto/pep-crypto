package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkDetail;
import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkResponse;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationTargetExchange;
import nl.logius.pepcrypto.api.oid.ApiOID;
import nl.logius.pepcrypto.application.microprofile.schema.signedencryptedidentity.MicroprofileSignedEncryptedIdentity;
import nl.logius.pepcrypto.application.microprofile.schema.signedencryptedpseudonym.MicroprofileSignedEncryptedPseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1Envelope;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_7_2;
import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_8_2;
import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.of;

/**
 * Maps the processing state to the outgoing message.
 */
@ApplicationScoped
class MicroprofileVerifyLinkResponseMapper {

    private static final Map<ApiOID, Function<Asn1Envelope, Object>> OID_MAPPERS = newOidDecodedInputMappers();

    private static Map<ApiOID, Function<Asn1Envelope, Object>> newOidDecodedInputMappers() {
        return Map.of(
                of(OID_2_16_528_1_1003_10_1_2_7_2), MicroprofileSignedEncryptedIdentity::newInstance,
                of(OID_2_16_528_1_1003_10_1_2_8_2), MicroprofileSignedEncryptedPseudonym::newInstance
        );
    }

    OASVerifyLinkResponse mapToResponse(MicroprofileVerifyLinkExchange exchange) {
        var allTargetsLinked = exchange.isLinkSourceLinkedToAllLinkTargets();
        var linkSourceDecodedInput = linkSourceDecodedInput(exchange);
        var linkDetails = linkDetails(exchange);

        return new OASVerifyLinkResponse()
                       .linked(allTargetsLinked)
                       .decodedInput(linkSourceDecodedInput)
                       .linkDetails(linkDetails);
    }

    private List<OASVerifyLinkDetail> linkDetails(MicroprofileVerifyLinkExchange exchange) {
        return exchange.getLinkVerificationTargets().stream()
                       .map(this::verifyLinkDetail)
                       .collect(Collectors.toList());

    }

    private Object linkSourceDecodedInput(MicroprofileVerifyLinkExchange exchange) {
        var linkSourceOid = exchange.getRequest().getAsn1SequenceDecodableOid();
        var linkSourceMapper = OID_MAPPERS.get(linkSourceOid);
        var linkSourceMappedInput = exchange.getMappedInput();
        return linkSourceMapper.apply(linkSourceMappedInput);
    }

    private OASVerifyLinkDetail verifyLinkDetail(ApiSignedEncryptedLinkVerificationTargetExchange target) {
        var targetLinked = target.isLinked();
        var recipient = linkTargetRecipientOin(target);
        var linkTargetDecodedInput = linkTargetDecodedInput(target);
        var encryptedHash = target.getEncryptedHash();

        return new OASVerifyLinkDetail()
                       .linked(targetLinked)
                       .oin(recipient)
                       .hash(encryptedHash)
                       .decodedInput(linkTargetDecodedInput);
    }

    private String linkTargetRecipientOin(ApiSignedEncryptedLinkVerificationTargetExchange target) {
        return target.getMappedInput()
                     .asn1RecipientKeyId()
                     .getRecipient();
    }

    private Object linkTargetDecodedInput(ApiSignedEncryptedLinkVerificationTargetExchange target) {
        var linkTargetOid = target.getApiOIDType();
        var linkTargetMapper = OID_MAPPERS.get(linkTargetOid);
        var targetMappedInput = target.getMappedInput();
        return linkTargetMapper.apply(targetMappedInput);
    }

}
