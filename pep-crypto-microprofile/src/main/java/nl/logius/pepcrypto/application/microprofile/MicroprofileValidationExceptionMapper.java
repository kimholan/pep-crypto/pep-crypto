package nl.logius.pepcrypto.application.microprofile;

import javax.validation.ValidationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static nl.logius.pepcrypto.application.microprofile.ExceptionMapperResponse.fromValidationException;

@Provider
public class MicroprofileValidationExceptionMapper
        implements ExceptionMapper<ValidationException> {

    @Override
    public Response toResponse(ValidationException cause) {
        return fromValidationException(cause);
    }

}
