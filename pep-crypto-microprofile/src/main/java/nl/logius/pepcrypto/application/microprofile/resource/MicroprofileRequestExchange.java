package nl.logius.pepcrypto.application.microprofile.resource;

import nl.logius.pepcrypto.api.ApiExchange;
import nl.logius.pepcrypto.application.microprofile.keystore.MicroprofileServiceProviderKeyStoreExchange;
import nl.logius.pepcrypto.application.microprofile.schema.MicroprofileRequest;
import nl.logius.pepcrypto.lib.asn1.Asn1PepType;

/**
 * Basic interface for OASRequest exchanges.
 *
 * @param <S> OAS request type.
 * @param <T> ASN.1-type.
 */
public interface MicroprofileRequestExchange<S extends MicroprofileRequest, T extends Asn1PepType>
        extends MicroprofileServiceProviderKeyStoreExchange, ApiExchange<T> {

    S getRequest();

}
