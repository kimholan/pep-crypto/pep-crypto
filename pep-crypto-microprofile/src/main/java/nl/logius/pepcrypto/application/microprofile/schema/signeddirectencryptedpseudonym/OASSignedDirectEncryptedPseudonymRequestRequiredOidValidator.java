package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonymRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASSignedDirectEncryptedPseudonymRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASSignedDirectEncryptedPseudonymRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASSignedDirectEncryptedPseudonymRequest target) {
        return target::getSignedDirectEncryptedPseudonym;
    }

}
