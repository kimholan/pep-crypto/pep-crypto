package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedIdentity;
import nl.logius.pepcrypto.application.microprofile.schema.signaturevalue.MicroprofileSignatureValue;
import nl.logius.pepcrypto.lib.asn1.signeddirectencryptedidentityv2.Asn1SignedDirectEncryptedIdentityv2Envelope;

/**
 * Maps data to output model.
 */
public enum MicroprofileSignedDirectEncryptedIdentity {
    ;

    public static OASSignedDirectEncryptedIdentity newInstance(Asn1SignedDirectEncryptedIdentityv2Envelope source) {
        var target = new OASSignedDirectEncryptedIdentity();

        target.notationIdentifier(source.asn1Oid())
              .signedDEI(MicroprofileSignedDEI.newInstance(source))
              .signatureValue(MicroprofileSignatureValue.newInstance(source));

        return target;
    }

}
