package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEI;
import nl.logius.pepcrypto.application.microprofile.schema.extraelement.MicroprofileExtraElement;
import nl.logius.pepcrypto.lib.asn1.Asn1IdentityEnvelope;
import nl.logius.pepcrypto.lib.asn1.signedencryptedidentityv2.Asn1SignedEncryptedIdentityv2Envelope;

/**
 * Maps data to output model.
 */
enum MicroprofileSignedEI {
    ;

    static OASSignedEI newInstance(Asn1IdentityEnvelope source) {
        var target = new OASSignedEI();
        target.setExtraElements(null);

        var auditElement = source.asn1AuditElement();

        target.encryptedIdentity(MicroprofileEncryptedIdentity.newInstance(source))
              .auditElement(auditElement);

        if (source instanceof Asn1SignedEncryptedIdentityv2Envelope) {
            var v2Source = (Asn1SignedEncryptedIdentityv2Envelope) source;
            var signedBody = v2Source.getSignedEI();

            target.issuanceDate(signedBody.getIssuanceDate())
                  .extraElements(MicroprofileExtraElement.newInstance(signedBody));
        }

        return target;
    }

}
