package nl.logius.pepcrypto.application.microprofile.resource.signedencryptedpseudonymmigration;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonymMigrationRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedPseudonymMigrationExchange;
import nl.logius.pepcrypto.application.microprofile.resource.AbstractMicroprofileSignedRequestExchange;
import nl.logius.pepcrypto.lib.asn1.Asn1Pseudonym;
import nl.logius.pepcrypto.lib.asn1.Asn1PseudonymEnvelope;
import nl.logius.pepcrypto.lib.crypto.PepEcSchnorrVerificationKey;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import nl.logius.pepcrypto.lib.crypto.key.PepEpDecryptionPrivateKey;
import nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationPrivateKey;
import nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrn;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.util.Optional;

import static nl.logius.pepcrypto.lib.crypto.key.PepEpClosingPrivateKey.epClosingPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpDecryptionPrivateKey.epDecryptionPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationPrivateKey.epMigrationPrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationSourcePrivateKey.epMigrationSourcePrivateKey;
import static nl.logius.pepcrypto.lib.crypto.key.PepEpMigrationTargetPrivateKey.epMigrationTargetPrivateKey;
import static nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns.SCHEME_KEY_PPP;

/**
 * Carries the processing state of a message exchange.
 */
class MicroprofileSignedEncryptedPseudonymMigrationExchange
        extends AbstractMicroprofileSignedRequestExchange<OASSignedEncryptedPseudonymMigrationRequest, Asn1PseudonymEnvelope>
        implements PepEcSchnorrVerificationKey,
                   ApiSignedEncryptedPseudonymMigrationExchange {

    MicroprofileSignedEncryptedPseudonymMigrationExchange(OASSignedEncryptedPseudonymMigrationRequest request) {
        super(request, request::getSignedEncryptedPseudonym);
    }

    @Override
    public PepEpMigrationPrivateKey getClosingPepPrivateKey() {
        var epClosing = epClosingPrivateKey(getSelectedClosingKey().getPrivateKey());
        var epMigrationSource = epMigrationSourcePrivateKey(getMigrationSourceKey().getPrivateKey());
        var epMigrationTarget = epMigrationTargetPrivateKey(getMigrationTargetKey().getPrivateKey());

        return epMigrationPrivateKey(epClosing, epMigrationSource, epMigrationTarget);
    }

    @Override
    public PepEcSchnorrVerificationKey getVerificationKeys() {
        return this;
    }

    @Override
    public Asn1Pseudonym getDecryptedPseudonymResultAsn1Pseudonym() {
        return getMappedInput().asn1Pseudonym();
    }

    @Override
    public PepRecipientKeyId getDecryptedPseudonymResultPepRecipientKeyId() {
        var recipient = getMigrationTargetKey().getRecipient();
        var recipientKeySetVersion = getMigrationTargetKey().getRecipientKeySetVersion();
        var recipientKeyId = getMappedInput().asn1RecipientKeyId();
        return recipientKeyId.recipient(recipient)
                             .recipientKeySetVersion(recipientKeySetVersion);
    }

    @Override
    public PepEpDecryptionPrivateKey getDecryptionPepPrivateKey() {
        return epDecryptionPrivateKey(getSelectedDecryptionKey().getPrivateKey());
    }

    @Override
    public ECPoint getDecryptedPseudonymResultEcPoint() {
        return getDecryptedEcPoint();
    }

    public boolean isMatchingSchemeKeyUrn(String urn) {
        var schemeKeySetVersion = getSelectedSchemeKeySetVersion();
        return Optional.ofNullable(urn)
                       .map(SCHEME_KEY_PPP::asPepSchemeKeyUrn)
                       .filter(PepSchemeKeyUrn::matches)
                       .filter(it -> it.isMatchingSchemeKeySetVersionString(schemeKeySetVersion))
                       .isPresent();
    }

    String getRequestMigrationId() {
        return getRequest().getMigrationID();
    }

    boolean isMigrationTargetSelectionInvalid() {
        return getRequestTargetMigrant() == null ^ getRequestTargetMigrantKeySetVersion() == null;
    }

    String getRequestTargetMigrant() {
        return getRequest().getTargetMigrant();
    }

    BigInteger getRequestTargetMigrantKeySetVersion() {
        return Optional.of(getRequest())
                       .map(OASSignedEncryptedPseudonymMigrationRequest::getTargetMigrantKeySetVersion)
                       .map(BigInteger::new)
                       .orElse(null);
    }

}
