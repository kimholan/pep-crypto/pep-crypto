package nl.logius.pepcrypto.application.microprofile.schema.diversifier;

import generated.nl.logius.pepcrypto.openapi.model.OASDiversifier;
import nl.logius.pepcrypto.lib.asn1.diversifier.Asn1Diversifier;
import nl.logius.pepcrypto.lib.asn1.diversifier.Asn1DiversifierKeyValuePair;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Maps data to output model.
 */
public enum MicroprofileDiversifier {
    ;

    public static List<OASDiversifier> newInstance(Asn1Diversifier diversifier) {
        return Optional.ofNullable(diversifier)
                       .map(MicroprofileDiversifier::diversifierKeyValuePair)
                       .orElse(null);
    }

    private static List<OASDiversifier> diversifierKeyValuePair(Asn1Diversifier diversifier) {
        return diversifier.getDiversifierkeyvaluepair().stream()
                          .map(MicroprofileDiversifier::newDiversifierKeyPair)
                          .collect(Collectors.toList());
    }

    private static OASDiversifier newDiversifierKeyPair(Asn1DiversifierKeyValuePair keyValuePair) {
        return new OASDiversifier()
                       .key(keyValuePair.getKey())
                       .value(keyValuePair.getValue());
    }

}

