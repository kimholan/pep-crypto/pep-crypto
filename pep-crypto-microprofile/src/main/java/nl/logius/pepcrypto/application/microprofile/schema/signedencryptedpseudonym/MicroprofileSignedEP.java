package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEP;
import nl.logius.pepcrypto.application.microprofile.schema.extraelement.MicroprofileExtraElement;
import nl.logius.pepcrypto.lib.asn1.Asn1PseudonymEnvelope;
import nl.logius.pepcrypto.lib.asn1.signedencryptedpseudonymv2.Asn1SignedEncryptedPseudonymv2Envelope;

/**
 * Maps data to output model.
 */
enum MicroprofileSignedEP {
    ;

    static OASSignedEP newInstance(Asn1PseudonymEnvelope source) {
        var target = new OASSignedEP();
        target.setExtraElements(null);

        var auditElement = source.asn1AuditElement();
        target.encryptedPseudonym(MicroprofileEncryptedPseudonym.newInstance(source))
              .auditElement(auditElement);

        if (source instanceof Asn1SignedEncryptedPseudonymv2Envelope) {
            var v2Source = (Asn1SignedEncryptedPseudonymv2Envelope) source;
            var asn1SignedBody = v2Source.asn1SignedBody();
            var issuanceDate = asn1SignedBody.getIssuanceDate();

            target.issuanceDate(issuanceDate)
                  .extraElements(MicroprofileExtraElement.newInstance(asn1SignedBody));
        }

        return target;
    }

}
