package nl.logius.pepcrypto.application.microprofile.schema.extraelement;

import generated.nl.logius.pepcrypto.openapi.model.OASExtraElement;
import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyExtraElements;

import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Maps data to output model.
 */
public enum MicroprofileExtraElement {
    ;

    public static List<OASExtraElement> newInstance(Asn1SignedBodyExtraElements extraElements) {
        return Optional.ofNullable(extraElements)
                       .map(Asn1SignedBodyExtraElements::asn1ExtraElementsEntryList)
                       .stream().flatMap(Collection::stream)
                       .map(MicroprofileExtraElement::newOASExtraElement)
                       .collect(Collectors.toList());
    }

    private static OASExtraElement newOASExtraElement(Entry<String, Object> extraElementEntry) {
        return new OASExtraElement()
                       .key(extraElementEntry.getKey())
                       .value(extraElementEntry.getValue());
    }

}
