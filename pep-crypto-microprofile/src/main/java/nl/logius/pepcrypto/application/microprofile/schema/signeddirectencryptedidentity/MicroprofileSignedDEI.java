package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDEI;
import nl.logius.pepcrypto.application.microprofile.schema.extraelement.MicroprofileExtraElement;
import nl.logius.pepcrypto.lib.asn1.signeddirectencryptedidentityv2.Asn1SignedDirectEncryptedIdentityv2Envelope;

/**
 * Maps data to output model.
 */
enum MicroprofileSignedDEI {
    ;

    static OASSignedDEI newInstance(Asn1SignedDirectEncryptedIdentityv2Envelope source) {
        var target = new OASSignedDEI();
        var auditElement = source.asn1AuditElement();
        var signingKeyVersion = source.asn1SigningKeyVersion();

        target.extraElements(null)
              .auditElement(auditElement)
              .directEncryptedIdentity(MicroprofileDirectEncryptedIdentity.newInstance(source))
              .signingKeyVersion(signingKeyVersion.toString())
              .issuanceDate(source.asn1SignedBody().getIssuanceDate())
              .extraElements(MicroprofileExtraElement.newInstance(source.asn1SignedBody()));

        return target;
    }

}
