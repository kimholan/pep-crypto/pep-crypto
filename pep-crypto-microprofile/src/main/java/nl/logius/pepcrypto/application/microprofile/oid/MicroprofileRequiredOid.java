package nl.logius.pepcrypto.application.microprofile.oid;

import nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType;

import javax.validation.Constraint;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE, TYPE_USE})
@Retention(RUNTIME)
@Constraint(validatedBy = {})
public @interface MicroprofileRequiredOid {

    String message() default "OID_NOT_SUPPORTED";

    Class<?>[] groups() default {};

    Class<?>[] payload() default {};

    ApiOIDType[] value();

}
