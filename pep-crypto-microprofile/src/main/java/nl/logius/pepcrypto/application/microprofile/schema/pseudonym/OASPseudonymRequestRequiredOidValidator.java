package nl.logius.pepcrypto.application.microprofile.schema.pseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASPseudonymRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASPseudonymRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASPseudonymRequest target) {
        return target::getPseudonym;
    }

}
