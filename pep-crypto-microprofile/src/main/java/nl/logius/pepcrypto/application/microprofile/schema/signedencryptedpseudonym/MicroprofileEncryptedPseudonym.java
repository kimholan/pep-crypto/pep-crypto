package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASEncryptedPseudonym;
import nl.logius.pepcrypto.application.microprofile.schema.diversifier.MicroprofileDiversifier;
import nl.logius.pepcrypto.lib.asn1.Asn1PseudonymEnvelope;

/**
 * Maps data to output model.
 */
enum MicroprofileEncryptedPseudonym {
    ;

    static OASEncryptedPseudonym newInstance(Asn1PseudonymEnvelope source) {
        var target = new OASEncryptedPseudonym();
        var points = source.asn1EcPointValues();
        var pepRecipientKeyId = source.asn1RecipientKeyId();

        target.notationIdentifier(source.asn1BodyOid())
              .creator(source.asn1Body().getCreator())
              .type(Character.toString((char) source.asn1PseudonymType().intValue()))
              .points(points)
              .diversifier(MicroprofileDiversifier.newInstance(source.asn1PseudonymDiversifier()))
              .recipient(pepRecipientKeyId.getRecipient())
              .recipientKeySetVersion(pepRecipientKeyId.getRecipientKeySetVersion().toString())
              .schemeVersion(pepRecipientKeyId.getSchemeVersion().toString())
              .schemeKeySetVersion(pepRecipientKeyId.getSchemeKeySetVersion().toString());

        return target;
    }

}

