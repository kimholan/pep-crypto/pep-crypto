package nl.logius.pepcrypto.application.microprofile.schema.migrationintermediarypseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASMigrationIntermediaryPseudonym;
import nl.logius.pepcrypto.application.microprofile.schema.diversifier.MicroprofileDiversifier;
import nl.logius.pepcrypto.lib.asn1.migrationintermediarypseudonym.Asn1MigrationIntermediaryPseudonym;

/**
 * Maps data to output model.
 */
public enum MicroprofileMigrationIntermediaryPseudonym {
    ;

    public static OASMigrationIntermediaryPseudonym newInstance(Asn1MigrationIntermediaryPseudonym source) {
        var target = new OASMigrationIntermediaryPseudonym();

        target.notationIdentifier(source.asn1Oid())
              .source(source.getSource())
              .sourceKeySetVersion(source.getSourceKeySetVersion().toString())
              .target(source.getTarget())
              .targetKeySetVersion(source.getTargetKeySetVersion().toString())
              .migrationID(source.getMigrationID())
              .type(Character.toString((char) source.getType().intValue()))
              .pseudonymValue(source.getPseudonymValue())
              .diversifier(MicroprofileDiversifier.newInstance(source.getDiversifier()))
              .schemeVersion(source.getSchemeVersion().toString())
              .schemeKeySetVersion(source.getSchemeKeySetVersion().toString())
        ;

        return target;
    }

}
