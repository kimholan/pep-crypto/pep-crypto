package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonymRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidator;

import javax.enterprise.context.Dependent;
import java.util.function.Supplier;

@Dependent
class OASSignedEncryptedPseudonymRequestRequiredOidValidator
        extends AbstractMicroprofileRequestRequiredOidValidator<OASSignedEncryptedPseudonymRequest> {

    @Override
    protected Supplier<byte[]> getRawInputSupplier(OASSignedEncryptedPseudonymRequest target) {
        return target::getSignedEncryptedPseudonym;
    }

}
