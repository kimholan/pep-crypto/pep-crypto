package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedIdentityRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASSignedEncryptedIdentityRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASSignedEncryptedIdentityRequestRequiredOidValidator, OASSignedEncryptedIdentityRequest> {

    @InjectMocks
    private OASSignedEncryptedIdentityRequestRequiredOidValidator validator;

    @Override
    public OASSignedEncryptedIdentityRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASSignedEncryptedIdentityRequest getRequest() {
        return mock(OASSignedEncryptedIdentityRequest.class);
    }

}
