package nl.logius.pepcrypto.application.microprofile.resource.pseudonymmigrationimport;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationImportRequest;
import nl.logius.pepcrypto.api.decrypted.ApiMigrationImportService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.enterprise.inject.Instance;

import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.ofOid;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofilePseudonymMigrationImportApiTest {

    @InjectMocks
    private MicroprofilePseudonymMigrationImportApi api;

    @Mock
    private Instance<ApiMigrationImportService> serviceInstance;

    @Mock
    private MicroprofilePseudonymMigrationImportResponseMapper responseMapper;

    @Test
    public void processMigrationRequest() {
        var response = mock(OASDecryptedPseudonymResponse.class);
        var oid = "2.16.528.1.1003.10.1.3.3";

        // Mock the request
        var request = mock(OASPseudonymMigrationImportRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(ofOid(oid));

        // Mock the Response
        when(responseMapper.mapToResponse(any())).thenReturn(response);

        var migrationImportService = mock(ApiMigrationImportService.class);
        when(serviceInstance.select(any())).thenReturn(serviceInstance);
        when(serviceInstance.get()).thenReturn(migrationImportService);

        // Perform call
        var actual = api.processRequest(request);

        assertEquals(response, actual);
    }

}
