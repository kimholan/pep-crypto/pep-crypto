package nl.logius.pepcrypto.application.microprofile.schema.verifylink;

import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASVerifyLinkRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASVerifyLinkRequestRequiredOidValidator, OASVerifyLinkRequest> {

    @InjectMocks
    private OASVerifyLinkRequestRequiredOidValidator validator;

    @Override
    public OASVerifyLinkRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASVerifyLinkRequest getRequest() {
        return mock(OASVerifyLinkRequest.class);
    }

}
