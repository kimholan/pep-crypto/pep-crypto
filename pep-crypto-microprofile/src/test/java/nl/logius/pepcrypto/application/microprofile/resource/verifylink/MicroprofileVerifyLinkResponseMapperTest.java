package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationTargetExchange;
import nl.logius.pepcrypto.lib.asn1.signedencryptedidentityv2.Asn1SignedEncryptedIdentityv2Envelope;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import java.util.List;

import static nl.logius.pepcrypto.api.oid.ApiOID.ApiOIDType.OID_2_16_528_1_1003_10_1_2_7_2;
import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.of;
import static nl.logius.pepcrypto.lib.TestResources.resourceToByteArray;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofileVerifyLinkResponseMapperTest {

    @InjectMocks
    private MicroprofileVerifyLinkResponseMapper mapper;

    @Test
    public void mapToResponse() {
        var exchange = mock(MicroprofileVerifyLinkExchange.class);

        //  mappedInput
        var bytes = resourceToByteArray("/v2/happy_flow/tc1_ei_multi_ee_20.asn1");
        var mappedInput = Asn1SignedEncryptedIdentityv2Envelope.fromByteArray(bytes);
        when(exchange.getMappedInput()).thenReturn(mappedInput);

        var request = mock(OASVerifyLinkRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(of(OID_2_16_528_1_1003_10_1_2_7_2));
        when(exchange.getRequest()).thenReturn(request);

        var target = mock(ApiSignedEncryptedLinkVerificationTargetExchange.class);
        when(target.getMappedInput()).thenReturn(mappedInput);
        when(target.getApiOIDType()).thenReturn(of(OID_2_16_528_1_1003_10_1_2_7_2));

        var targets = List.of(target);

        when(exchange.getLinkVerificationTargets()).thenReturn(targets);

        assertNotNull(mapper.mapToResponse(exchange));
    }

}
