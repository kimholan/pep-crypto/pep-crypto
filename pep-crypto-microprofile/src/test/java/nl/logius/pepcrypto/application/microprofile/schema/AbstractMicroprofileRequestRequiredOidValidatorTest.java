package nl.logius.pepcrypto.application.microprofile.schema;

import nl.logius.pepcrypto.api.oid.ApiOID;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileAsn1SequenceDecoder;
import nl.logius.pepcrypto.application.microprofile.oid.MicroprofileRequiredOid;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public abstract class AbstractMicroprofileRequestRequiredOidValidatorTest<T extends AbstractMicroprofileRequestRequiredOidValidator<R>, R extends MicroprofileRequest> {

    @Mock
    private MicroprofileAsn1SequenceDecoder decoder;

    private T target;

    private R request;

    public abstract T getTarget();

    public abstract R getRequest();

    @Before
    public void before() {
        target = getTarget();
        request = getRequest();

        var constraintAnnotation = mock(MicroprofileRequiredOid.class);
        when(constraintAnnotation.value()).thenReturn(new ApiOID.ApiOIDType[0]);
        target.initialize(constraintAnnotation);
    }

    @Test
    public void coverage() {
        var context = mock(ConstraintValidatorContext.class);

        assertFalse(target.isValid(request, context));
    }

}
