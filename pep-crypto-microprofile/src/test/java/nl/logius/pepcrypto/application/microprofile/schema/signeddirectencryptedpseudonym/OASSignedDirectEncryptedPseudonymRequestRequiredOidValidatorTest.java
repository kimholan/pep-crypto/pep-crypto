package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonymRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASSignedDirectEncryptedPseudonymRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASSignedDirectEncryptedPseudonymRequestRequiredOidValidator, OASSignedDirectEncryptedPseudonymRequest> {

    @InjectMocks
    private OASSignedDirectEncryptedPseudonymRequestRequiredOidValidator validator;

    @Override
    public OASSignedDirectEncryptedPseudonymRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASSignedDirectEncryptedPseudonymRequest getRequest() {
        return mock(OASSignedDirectEncryptedPseudonymRequest.class);
    }

}
