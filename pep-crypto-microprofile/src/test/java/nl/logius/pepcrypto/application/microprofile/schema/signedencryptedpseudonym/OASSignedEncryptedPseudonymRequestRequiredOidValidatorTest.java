package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonymRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASSignedEncryptedPseudonymRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASSignedEncryptedPseudonymRequestRequiredOidValidator, OASSignedEncryptedPseudonymRequest> {

    @InjectMocks
    private OASSignedEncryptedPseudonymRequestRequiredOidValidator validator;

    @Override
    public OASSignedEncryptedPseudonymRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASSignedEncryptedPseudonymRequest getRequest() {
        return mock(OASSignedEncryptedPseudonymRequest.class);
    }

}
