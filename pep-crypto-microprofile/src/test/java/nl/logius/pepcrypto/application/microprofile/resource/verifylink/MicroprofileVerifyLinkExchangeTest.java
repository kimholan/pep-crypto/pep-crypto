package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import nl.logius.pepcrypto.lib.asn1.signedencryptedidentityv2.Asn1SignedEncryptedIdentityv2Envelope;
import org.junit.Test;

import java.util.List;

import static java.util.Collections.emptyList;
import static nl.logius.pepcrypto.lib.TestResources.resourceToByteArray;
import static nl.logius.pepcrypto.lib.urn.PepSchemeKeyUrns.SCHEME_KEY_ANY;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MicroprofileVerifyLinkExchangeTest {

    @Test
    public void coverage() {
        var request = mock(OASVerifyLinkRequest.class);
        when(request.getLinkTargetAsn1SequenceDecodables()).thenReturn(List.of());

        var unit = new MicroprofileVerifyLinkExchange(request);

        var bytes = resourceToByteArray("/v2/happy_flow/tc1_ei_multi_ee_20.asn1");
        var mappedInput = Asn1SignedEncryptedIdentityv2Envelope.fromByteArray(bytes);
        unit.setMappedInput(mappedInput);

        unit.getLinkVerificationTargets();
        unit.setLinkSourceSameIds(emptyList());

        assertFalse(unit.containsSameId(null));

        unit.setPepSchemeKeyUrn(SCHEME_KEY_ANY);
        unit.isMatchingSchemeKeyUrn(null);
        assertTrue(unit.isIdentityEnvelope());
        assertFalse(unit.isPseudonymEnvelope());
    }

}
