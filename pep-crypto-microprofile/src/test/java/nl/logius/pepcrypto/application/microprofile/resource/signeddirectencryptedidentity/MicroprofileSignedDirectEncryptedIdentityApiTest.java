package nl.logius.pepcrypto.application.microprofile.resource.signeddirectencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedIdentityResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedIdentityRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedDirectEncryptedIdentityService;
import nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_10_2.ApiSignedDirectEncryptedIdentityv2Decryption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.enterprise.inject.Instance;

import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.ofOid;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofileSignedDirectEncryptedIdentityApiTest {

    @InjectMocks
    private MicroprofileSignedDirectEncryptedIdentityApi api;

    @Mock
    private Instance<ApiSignedDirectEncryptedIdentityService> serviceInstance;

    @Mock
    private MicroprofileSignedDirectEncryptedIdentityResponseMapper responseMapper;

    @Test
    public void processEncryptedIdentityRequest() {
        var response = mock(OASDecryptedIdentityResponse.class);
        var oid = "2.16.528.1.1003.10.1.2.10.2";

        // Mock the request
        var request = mock(OASSignedDirectEncryptedIdentityRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(ofOid(oid));

        // Mock the Response
        when(responseMapper.mapToResponse(any())).thenReturn(response);

        var signedEncryptedIdentityService = mock(ApiSignedDirectEncryptedIdentityv2Decryption.class);
        when(serviceInstance.select(any())).thenReturn(serviceInstance);
        when(serviceInstance.get()).thenReturn(signedEncryptedIdentityService);

        // Perform call
        var actual = api.processRequest(request);

        assertEquals(response, actual);
    }

}



