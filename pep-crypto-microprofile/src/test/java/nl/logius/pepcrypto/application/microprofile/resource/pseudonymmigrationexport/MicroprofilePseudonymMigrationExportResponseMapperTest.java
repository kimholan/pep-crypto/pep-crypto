package nl.logius.pepcrypto.application.microprofile.resource.pseudonymmigrationexport;

import nl.logius.pepcrypto.lib.asn1.decryptedpseudonym.Asn1DecryptedPseudonym;
import nl.logius.pepcrypto.lib.crypto.PepCrypto;
import nl.logius.pepcrypto.lib.crypto.PepRecipientKeyId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import static java.math.BigInteger.ZERO;
import static nl.logius.pepcrypto.lib.TestResources.resourceToByteArray;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofilePseudonymMigrationExportResponseMapperTest {

    @InjectMocks
    private MicroprofilePseudonymMigrationExportResponseMapper mapper;

    @Test
    public void mapToResponse() {
        var exchange = mock(MicroprofilePseudonymMigrationExportExchange.class);
        var ecPoint = PepCrypto.getBasePoint();

        //  mappedInput
        var rawInput = resourceToByteArray("/v1/pseudonyms/p01_div1.asn1");
        var mappedInput = Asn1DecryptedPseudonym.fromByteArray(rawInput);
        when(exchange.getMappedInput()).thenReturn(mappedInput);

        var migrationSourceTargetRecipientKeyId = mock(PepRecipientKeyId.class);
        when(migrationSourceTargetRecipientKeyId.getRecipient()).thenReturn("migrationSourceTarget");
        when(migrationSourceTargetRecipientKeyId.getRecipientKeySetVersion()).thenReturn(ZERO);
        when(exchange.getMigrationSourceTargetRecipientKeyId()).thenReturn(migrationSourceTargetRecipientKeyId);
        when(exchange.getMigrationSourceMigrationId()).thenReturn("migrationID");
        when(exchange.getConvertedEcPoint()).thenReturn(ecPoint);

        mapper.mapToResponse(exchange);
    }

}

