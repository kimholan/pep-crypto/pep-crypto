package nl.logius.pepcrypto.application.microprofile.resource.pseudonymconversion;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymRequest;
import nl.logius.pepcrypto.api.decrypted.ApiClosingKeyConversionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.enterprise.inject.Instance;

import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.ofOid;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofilePseudonymConversionApiTest {

    @InjectMocks
    private MicroprofilePseudonymConversionApi api;

    @Mock
    private Instance<ApiClosingKeyConversionService> serviceInstance;

    @Mock
    private MicroprofilePseudonymConversionResponseMapper responseMapper;

    @Test
    public void processEncryptedPseudonymRequest() {
        var response = mock(OASDecryptedPseudonymResponse.class);
        var oid = "2.16.528.1.1003.10.1.3.2";

        // Mock the request
        var request = mock(OASPseudonymRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(ofOid(oid));

        // Mock the Response
        when(responseMapper.mapToResponse(any())).thenReturn(response);

        var signedEncryptedPseudonymService = mock(ApiClosingKeyConversionService.class);
        when(serviceInstance.select(any())).thenReturn(serviceInstance);
        when(serviceInstance.get()).thenReturn(signedEncryptedPseudonymService);

        // Perform call
        var actual = api.processRequest(request);

        assertEquals(response, actual);
    }

}
