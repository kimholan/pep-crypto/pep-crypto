package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedIdentityRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASSignedDirectEncryptedIdentityRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASSignedDirectEncryptedIdentityRequestRequiredOidValidator, OASSignedDirectEncryptedIdentityRequest> {

    @InjectMocks
    private OASSignedDirectEncryptedIdentityRequestRequiredOidValidator validator;

    @Override
    public OASSignedDirectEncryptedIdentityRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASSignedDirectEncryptedIdentityRequest getRequest() {
        return mock(OASSignedDirectEncryptedIdentityRequest.class);
    }

}
