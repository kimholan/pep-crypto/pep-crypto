package nl.logius.pepcrypto.application.microprofile.resource.pseudonymmigrationexport;

import generated.nl.logius.pepcrypto.openapi.model.OASMigrationIntermediaryPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationExportRequest;
import nl.logius.pepcrypto.api.decrypted.ApiMigrationExportService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.enterprise.inject.Instance;

import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.ofOid;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofilePseudonymMigrationExportApiTest {

    @InjectMocks
    private MicroprofilePseudonymMigrationExportApi api;

    @Mock
    private Instance<ApiMigrationExportService> serviceInstance;

    @Mock
    private MicroprofilePseudonymMigrationExportResponseMapper responseMapper;

    @Test
    public void processMigrationRequest() {
        var response = mock(OASMigrationIntermediaryPseudonymResponse.class);
        var oid = "2.16.528.1.1003.10.1.3.2";

        // Mock the request
        var request = mock(OASPseudonymMigrationExportRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(ofOid(oid));

        // Mock the Response
        when(responseMapper.mapToResponse(any())).thenReturn(response);

        var migrationExportService = mock(ApiMigrationExportService.class);
        when(serviceInstance.select(any())).thenReturn(serviceInstance);
        when(serviceInstance.get()).thenReturn(migrationExportService);

        // Perform call
        var actual = api.processRequest(request);

        assertEquals(response, actual);
    }

}
