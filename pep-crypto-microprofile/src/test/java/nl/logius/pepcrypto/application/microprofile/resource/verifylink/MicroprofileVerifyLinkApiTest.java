package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkResponse;
import nl.logius.pepcrypto.api.encrypted.ApiSignedEncryptedLinkVerificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.enterprise.inject.Instance;

import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.ofOid;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofileVerifyLinkApiTest {

    @InjectMocks
    private MicroprofileVerifyLinkApi api;

    @Mock
    private Instance<ApiSignedEncryptedLinkVerificationService> serviceInstance;

    @Mock
    private MicroprofileVerifyLinkResponseMapper responseMapper;

    @Test
    public void processEncryptedPseudonymRequest() {
        var response = mock(OASVerifyLinkResponse.class);
        var oid = "2.16.528.1.1003.10.1.2.8.2";

        // Mock the Response
        when(responseMapper.mapToResponse(any())).thenReturn(response);

        // Mock the request
        var request = mock(OASVerifyLinkRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(ofOid(oid));

        var service = mock(ApiSignedEncryptedLinkVerificationService.class);
        when(serviceInstance.select(any())).thenReturn(serviceInstance);
        when(serviceInstance.get()).thenReturn(service);

        // Perform call
        var actual = api.processRequest(request);

        assertEquals(response, actual);
    }

}


