package nl.logius.pepcrypto.application.microprofile.schema.pseudonymmigrationexport;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationExportRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASPseudonymMigrationExportRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASPseudonymMigrationExportRequestRequiredOidValidator, OASPseudonymMigrationExportRequest> {

    @InjectMocks
    private OASPseudonymMigrationExportRequestRequiredOidValidator validator;

    @Override
    public OASPseudonymMigrationExportRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASPseudonymMigrationExportRequest getRequest() {
        return mock(OASPseudonymMigrationExportRequest.class);
    }

}
