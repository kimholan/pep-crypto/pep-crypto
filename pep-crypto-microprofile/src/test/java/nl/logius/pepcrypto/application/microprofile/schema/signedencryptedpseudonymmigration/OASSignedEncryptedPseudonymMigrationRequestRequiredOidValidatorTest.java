package nl.logius.pepcrypto.application.microprofile.schema.signedencryptedpseudonymmigration;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonymMigrationRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASSignedEncryptedPseudonymMigrationRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASSignedEncryptedPseudonymMigrationRequestRequiredOidValidator, OASSignedEncryptedPseudonymMigrationRequest> {

    @InjectMocks
    private OASSignedEncryptedPseudonymMigrationRequestRequiredOidValidator validator;

    @Override
    public OASSignedEncryptedPseudonymMigrationRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASSignedEncryptedPseudonymMigrationRequest getRequest() {
        return mock(OASSignedEncryptedPseudonymMigrationRequest.class);
    }

}
