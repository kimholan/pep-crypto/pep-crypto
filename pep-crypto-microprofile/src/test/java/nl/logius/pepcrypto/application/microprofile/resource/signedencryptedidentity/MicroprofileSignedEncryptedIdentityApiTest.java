package nl.logius.pepcrypto.application.microprofile.resource.signedencryptedidentity;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedIdentityResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedIdentityRequest;
import nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_7.ApiSignedEncryptedIdentityDecryption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.enterprise.inject.Instance;

import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.ofOid;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofileSignedEncryptedIdentityApiTest {

    @InjectMocks
    private MicroprofileSignedEncryptedIdentityApi api;

    @Mock
    private Instance<ApiSignedEncryptedIdentityDecryption> serviceInstance;

    @Mock
    private MicroprofileSignedEncryptedIdentityResponseMapper responseMapper;

    @Test
    public void processEncryptedIdentityRequest() {
        var response = mock(OASDecryptedIdentityResponse.class);
        var oid = "2.16.528.1.1003.10.1.2.3";

        // Mock the Response
        when(responseMapper.mapToResponse(any())).thenReturn(response);

        // Mock the request
        var request = mock(OASSignedEncryptedIdentityRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(ofOid(oid));

        var signedEncryptedIdentityService = mock(ApiSignedEncryptedIdentityDecryption.class);
        when(serviceInstance.select(any())).thenReturn(serviceInstance);
        when(serviceInstance.get()).thenReturn(signedEncryptedIdentityService);

        // Perform call
        var actual = api.processRequest(request);

        assertEquals(response, actual);
    }

}
