package nl.logius.pepcrypto.application.microprofile.schema.extraelement;

import nl.logius.pepcrypto.lib.asn1.Asn1SignedBodyExtraElements;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MicroprofileExtraElementTest {

    private static final String KEY = "key";

    private static final String VALUE = "value";

    @Test
    public void newInstance() {
        var extraElements = mock(Asn1SignedBodyExtraElements.class);
        var entry = Map.<String, Object>entry("key", "value");
        var list = List.of(entry);
        when(extraElements.asn1ExtraElementsEntryList()).thenReturn(list);

        var actual = MicroprofileExtraElement.newInstance(extraElements);
        assertEquals(1, actual.size());

        var oasExtraElement = actual.get(0);
        assertEquals(KEY, oasExtraElement.getKey());
        assertEquals(VALUE, oasExtraElement.getValue());
    }

}
