package nl.logius.pepcrypto.application.microprofile.schema.signeddirectencryptedpseudonymmigration;

import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonymMigrationRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASSignedDirectEncryptedPseudonymMigrationRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASSignedDirectEncryptedPseudonymMigrationRequestRequiredOidValidator, OASSignedDirectEncryptedPseudonymMigrationRequest> {

    @InjectMocks
    private OASSignedDirectEncryptedPseudonymMigrationRequestRequiredOidValidator validator;

    @Override
    public OASSignedDirectEncryptedPseudonymMigrationRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASSignedDirectEncryptedPseudonymMigrationRequest getRequest() {
        return mock(OASSignedDirectEncryptedPseudonymMigrationRequest.class);
    }

}
