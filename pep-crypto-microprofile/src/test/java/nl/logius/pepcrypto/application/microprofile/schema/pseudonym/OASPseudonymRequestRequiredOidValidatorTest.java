package nl.logius.pepcrypto.application.microprofile.schema.pseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASPseudonymRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASPseudonymRequestRequiredOidValidator, OASPseudonymRequest> {

    @InjectMocks
    private OASPseudonymRequestRequiredOidValidator validator;

    @Override
    public OASPseudonymRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASPseudonymRequest getRequest() {
        return mock(OASPseudonymRequest.class);
    }

}
