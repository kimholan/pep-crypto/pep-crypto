package nl.logius.pepcrypto.application.microprofile.schema.pseudonymmigrationimport;

import generated.nl.logius.pepcrypto.openapi.model.OASPseudonymMigrationImportRequest;
import nl.logius.pepcrypto.application.microprofile.schema.AbstractMicroprofileRequestRequiredOidValidatorTest;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.mock;

public class OASPseudonymMigrationImportRequestRequiredOidValidatorTest
        extends AbstractMicroprofileRequestRequiredOidValidatorTest<OASPseudonymMigrationImportRequestRequiredOidValidator, OASPseudonymMigrationImportRequest> {

    @InjectMocks
    private OASPseudonymMigrationImportRequestRequiredOidValidator validator;

    @Override
    public OASPseudonymMigrationImportRequestRequiredOidValidator getTarget() {
        return validator;
    }

    @Override
    public OASPseudonymMigrationImportRequest getRequest() {
        return mock(OASPseudonymMigrationImportRequest.class);
    }

}
