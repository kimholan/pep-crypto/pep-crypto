package nl.logius.pepcrypto.application.microprofile.resource.signeddirectencryptedpseudonymmigration;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASSignedDirectEncryptedPseudonymMigrationRequest;
import nl.logius.pepcrypto.api.encrypted.ApiSignedDirectEncryptedPseudonymMigrationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.enterprise.inject.Instance;

import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.ofOid;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofileSignedDirectEncryptedPseudonymMigrationApiTest {

    @InjectMocks
    private MicroprofileSignedDirectEncryptedPseudonymMigrationApi api;

    @Mock
    private Instance<ApiSignedDirectEncryptedPseudonymMigrationService> serviceInstance;

    @Mock
    private MicroprofileSignedDirectEncryptedPseudonymMigrationResponseMapper responseMapper;

    @Test
    public void processDirectEncryptedPseudonymRequest() {
        var response = mock(OASDecryptedPseudonymResponse.class);
        var oid = "2.16.528.1.1003.10.1.2.6";

        // Mock the Response
        when(responseMapper.mapToResponse(any())).thenReturn(response);

        // Mock the request
        var request = mock(OASSignedDirectEncryptedPseudonymMigrationRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(ofOid(oid));

        var signedDirectEncryptedPseudonymService = mock(ApiSignedDirectEncryptedPseudonymMigrationService.class);
        when(serviceInstance.select(any())).thenReturn(serviceInstance);
        when(serviceInstance.get()).thenReturn(signedDirectEncryptedPseudonymService);

        // Perform call
        var actual = api.processRequest(request);

        assertEquals(response, actual);
    }

}
