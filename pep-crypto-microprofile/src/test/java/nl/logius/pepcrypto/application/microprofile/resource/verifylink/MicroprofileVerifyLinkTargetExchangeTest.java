package nl.logius.pepcrypto.application.microprofile.resource.verifylink;

import nl.logius.pepcrypto.api.ApiAsn1SequenceDecodable;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

public class MicroprofileVerifyLinkTargetExchangeTest {

    @Test
    public void coverage() {
        var decodable = mock(ApiAsn1SequenceDecodable.class);
        var unit = new MicroprofileVerifyLinkTargetExchange(decodable);

        assertFalse(unit.isLinked());
        unit.setLinked(true);
        assertNull(unit.getRawInput());
        assertNull(unit.getMappedInput());
        unit.setMappedInput(null);
        assertNull(unit.getEncryptedHash());
        unit.setEncryptedHash(null);
        assertNull(unit.getApiOIDType());
    }

}


