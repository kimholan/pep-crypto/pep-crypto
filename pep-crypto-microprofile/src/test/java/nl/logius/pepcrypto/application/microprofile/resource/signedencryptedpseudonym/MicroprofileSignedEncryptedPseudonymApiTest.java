package nl.logius.pepcrypto.application.microprofile.resource.signedencryptedpseudonym;

import generated.nl.logius.pepcrypto.openapi.model.OASDecryptedPseudonymResponse;
import generated.nl.logius.pepcrypto.openapi.model.OASSignedEncryptedPseudonymRequest;
import nl.logius.pepcrypto.api.oid.oid_2_16_528_1_1003_10_1_2_8.ApiSignedEncryptedPseudonymDecryption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner.StrictStubs;

import javax.enterprise.inject.Instance;

import static nl.logius.pepcrypto.api.oid.ApiOID.Literal.ofOid;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(StrictStubs.class)
public class MicroprofileSignedEncryptedPseudonymApiTest {

    @InjectMocks
    private MicroprofileSignedEncryptedPseudonymApi api;

    @Mock
    private Instance<ApiSignedEncryptedPseudonymDecryption> serviceInstance;

    @Mock
    private MicroprofileSignedEncryptedPseudonymResponseMapper responseMapper;

    @Test
    public void processEncryptedPseudonymRequest() {
        var response = mock(OASDecryptedPseudonymResponse.class);
        var oid = "2.16.528.1.1003.10.1.2.8";

        when(responseMapper.mapToResponse(any())).thenReturn(response);

        // Mock the request
        var request = mock(OASSignedEncryptedPseudonymRequest.class);
        when(request.getAsn1SequenceDecodableOid()).thenReturn(ofOid(oid));

        var signedEncryptedPseudonymService = mock(ApiSignedEncryptedPseudonymDecryption.class);
        when(serviceInstance.select(any())).thenReturn(serviceInstance);
        when(serviceInstance.get()).thenReturn(signedEncryptedPseudonymService);

        // Perform call
        var actual = api.processRequest(request);

        assertEquals(response, actual);
    }

}
