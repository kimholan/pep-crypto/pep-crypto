########################################################################################################################
# Default image for stage builds
#-----------------------------------------------------------------------------------------------------------------------
image: registry.gitlab.com/kimholan/pep-crypto/automation/build-environment:b139d309a77183e05d1ff14828069be700383e4e


########################################################################################################################
# Login/debug environment
#-----------------------------------------------------------------------------------------------------------------------
before_script:
  - docker info
  - git --version
  - mvn -v
  - java --version
  - gauge -v
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

########################################################################################################################
# Environment variables
#-----------------------------------------------------------------------------------------------------------------------
variables:
  PC_RUNTIME_ENVIROMENT_IMAGE: "registry.gitlab.com/kimholan/pep-crypto/automation/build-environment:b139d309a77183e05d1ff14828069be700383e4e"
  PC_DOCKER_REGISTRY: "registry.gitlab.com/kimholan/pep-crypto/pep-crypto"
  PC_IMAGE_NAME: "${PC_DOCKER_REGISTRY}/quarkus:${CI_COMMIT_SHA}"
  GITLAB_RUNNER_M2_REPO: "/gitlab-runner-m2-repo"
  API_URL_DEFAULT: "http://pep-crypto:8080/pep-crypto/api/v1"
  API_URL_MAX_POST_SIZE: "http://pep-crypto-max-post-size:8080/pep-crypto/api/v1"
  JAVA_HOME: "/usr/lib/jvm/default-java"
  M2_HOME: "/usr/share/maven"
  MAVEN_OPTS: "-XX:+UseParallelGC -XX:+UseParallelOldGC -XX:-TieredCompilation -Xmx1G"
  MVN_CMD: "mvn -Dmaven.repo.local=${GITLAB_RUNNER_M2_REPO}"
  PC_VERSION: "${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}"
  JACOCO_CLI_CMD: "${JAVA_HOME}/bin/java -jar /jacoco/lib/jacococli.jar"
  DOCKER_BUILDKIT: "0"

########################################################################################################################
# Stages
#-----------------------------------------------------------------------------------------------------------------------
stages:
  - build
  - test

########################################################################################################################
# Stage: Build
#-----------------------------------------------------------------------------------------------------------------------
build:docker:
  tags:
    - gitlab-vm-runner
  stage: build
  script:
    - rm -Rf ${GITLAB_RUNNER_M2_REPO}/kimholan/pep-crypto
    - NEXT_VERSION=$(echo ${PC_VERSION} | sed -e 's|/|_|g')
    - git grep -l  __NEXT__  | grep -v .gitlab-ci.yml | while read i ; do sed -e "s|__NEXT__|${NEXT_VERSION}|g" -i $i ; done
    - ${MVN_CMD} clean install -pl ':pep-crypto-quarkus' -am
    - mkdir -pv ./docker-image
    - cp -RvfT ./.gitlab-ci/ ./docker-image
    - cp -v /jacoco/lib/jacocoagent.jar ./docker-image
    - mv -v ./pep-crypto-quarkus/target/quarkus-app ./docker-image
    - cd ./docker-image
    - cat Dockerfile
    - docker build --build-arg PC_RUNTIME_ENVIROMENT_IMAGE=${PC_RUNTIME_ENVIROMENT_IMAGE} -t ${PC_IMAGE_NAME} .
    - cd -
    - docker push ${PC_IMAGE_NAME}

quality:jacoco:
  stage: build
  tags:
    - gitlab-vm-runner
  script:
    - rm -Rf ${GITLAB_RUNNER_M2_REPO}/kimholan/pep-crypto
    - NEXT_VERSION=$(echo ${PC_VERSION} | sed -e 's|/|_|g')
    - git grep -l  __NEXT__  | grep -v .gitlab-ci.yml | while read i ; do sed -e "s|__NEXT__|${NEXT_VERSION}|g" -i $i ; done
    - ${MVN_CMD} -Pjacoco clean install org.jacoco:jacoco-maven-plugin:0.8.7:report
  artifacts:
    paths:
      - pep-crypto-lib/pep-crypto-lib-lang/target/site/jacoco
      - pep-crypto-lib/pep-crypto-lib-pem/target/site/jacoco
      - pep-crypto-lib/pep-crypto-lib-asn1/target/site/jacoco
      - pep-crypto-lib/pep-crypto-lib-urn/target/site/jacoco
      - pep-crypto-lib/pep-crypto-lib-crypto/target/site/jacoco
      - pep-crypto-api/target/site/jacoco
      - pep-crypto-microprofile/target/site/jacoco
    reports:
      junit:
        - pep-crypto-lib/pep-crypto-lib-lang/target/surefire-reports/TEST-*.xml
        - pep-crypto-lib/pep-crypto-lib-pem/target/surefire-reports/TEST-*.xml
        - pep-crypto-lib/pep-crypto-lib-asn1/target/surefire-reports/TEST-*.xml
        - pep-crypto-lib/pep-crypto-lib-urn/target/surefire-reports/TEST-*.xml
        - pep-crypto-lib/pep-crypto-lib-crypto/target/surefire-reports/TEST-*.xml
        - pep-crypto-api/target/surefire-reports/TEST-*.xml
        - pep-crypto-microprofile/target/surefire-reports/TEST-*.xml


########################################################################################################################
# Stage: Test
#-----------------------------------------------------------------------------------------------------------------------

test:
  stage: test
  tags:
    - gitlab-vm-runner
  services:
    - name: ${PC_IMAGE_NAME}
      alias: pep-crypto
      command: ["--","quarkus","jacoco"]
    - name: ${PC_IMAGE_NAME}
      alias: pep-crypto-max-post-size
      command: ["--","quarkus-max-post-size","jacoco"]
  script:
    - rm -Rf ${GITLAB_RUNNER_M2_REPO}/kimholan/pep-crypto
    - NEXT_VERSION=$(echo ${PC_VERSION} | sed -e 's|/|_|g')
    - git grep -l  __NEXT__  | grep -v .gitlab-ci.yml | while read i ; do sed -e "s|__NEXT__|${NEXT_VERSION}|g" -i $i ; done
    - sleep 10
    - ${MVN_CMD} clean install -pl ':pep-crypto-quarkus' -am -DskipTests
    - ${MVN_CMD} -Pgauge clean install -pl ':pep-crypto-test' -Dtags='!disabled'
    - ${JACOCO_CLI_CMD} dump --address pep-crypto --reset --destfile pep-crypto.exec
    - ${JACOCO_CLI_CMD} dump --address pep-crypto-max-post-size --reset --destfile pep-crypto-max-post-size.exec
    - ${JACOCO_CLI_CMD} merge pep-crypto.exec pep-crypto-max-post-size.exec --destfile pep-crypto-merged.exec
    - shopt -s globstar; ${JACOCO_CLI_CMD} report pep-crypto-merged.exec
      $(find pep-crypto-{lib,api,microprofile}/**/target/classes -maxdepth 1 -type d -name nl -exec echo --classfiles  {} \;)
      $(find pep-crypto-{lib,api,microprofile}/**/src/main -maxdepth 1 -name "java" -type d -exec echo --sourcefiles {} \; | grep -v generated)
      --html pep-crypto-test-coverage
  artifacts:
    name: pep-crypto-test-report
    reports:
      junit: pep-crypto-test/reports/xml-report/result.xml
    paths:
      - pep-crypto-test/reports/html-report/
      - pep-crypto-test-coverage

########################################################################################################################

