
Running the tests
=================

## Install Gauge

See https://docs.gauge.org/master/installation.html.

### Build the project

From the project root directory:

```bash
$ mvn clean install
```

### Run testspecifications using Gauge

To run all the tests from the project root directory:

```bash
$ mvn -Pgauge -pl ':pep-crypto-test' install
``` 

The 'max-request-size.spec' assumes some endpoints are available with a request body size restriction. 
Supply a tag to disable this test:

```bash
$ mvn -Pgauge -pl ':pep-crypto-test' install -Dtags='!quarkus'
``` 

All other tests should run succesfully since they do not depend on the microprofile runtime specific behavior.

---
