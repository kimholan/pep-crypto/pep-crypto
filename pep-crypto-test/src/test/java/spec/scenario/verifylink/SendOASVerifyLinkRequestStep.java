package spec.scenario.verifylink;

import com.thoughtworks.gauge.Step;
import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import spec.BasicStep;

import java.io.IOException;

public class SendOASVerifyLinkRequestStep
        extends BasicStep {

    @Step("Send OASVerifyLinkRequest")
    public void sendOASVerifyLinkRequest() throws IOException, InterruptedException {
        var sendOASVerifyLinkRequest = getScenarioData(OASVerifyLinkRequest.class);

        defaultSendScenarioRequest(sendOASVerifyLinkRequest);
    }

}
