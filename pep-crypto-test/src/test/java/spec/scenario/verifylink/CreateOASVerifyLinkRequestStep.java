package spec.scenario.verifylink;

import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import generated.nl.logius.pepcrypto.openapi.model.OASVerifyLinkRequest;
import spec.BasicStep;
import spec.SpecDataPropertyTable;

import java.util.List;
import java.util.Map;

public class CreateOASVerifyLinkRequestStep
        extends BasicStep {

    @Step("Create OASVerifyLinkRequest <table>")
    public void createRequest(Table propertyTable) {

        var specData = getSpecDataset();
        var requestData = SpecDataPropertyTable.asMap(specData, propertyTable);

        // Request data
        var serviceProviderKeys = (List<String>) requestData.get("serviceProviderKeys");
        var schemeKeys = (Map<String, byte[]>) requestData.get("schemeKeys");
        var linkSource = (byte[]) requestData.get("linkSource");
        var linkTargets = (List<byte[]>) requestData.get("linkTargets");

        // SEPM
        var request = new OASVerifyLinkRequest();

        request.setServiceProviderKeys(serviceProviderKeys);
        request.setSchemeKeys(schemeKeys);
        request.setLinkSource(linkSource);
        request.setLinkTargets(linkTargets);

        putScenarioData(OASVerifyLinkRequest.class, request);
    }

//    private List<String> filterByMigrationKey(List<String> serviceProviderKeys, String keyType, String migrationID, String sourceMigrant, BigInteger sourceMigrantKeySetVersion, String targetMigrant, String targetMigrantKeySetVersion) {
//        return serviceProviderKeys.stream()
//                                  .filter(filterByHeader("Type", keyType))
//                                  .filter(filterByHeader("SourceMigrant", sourceMigrant))
//                                  .filter(filterByHeader("SourceMigrantKeySetVersion", String.valueOf(sourceMigrantKeySetVersion)))
//                                  .filter(optionalFilterByHeader("TargetMigrant", targetMigrant))
//                                  .filter(optionalFilterByHeader("TargetMigrantKeySetVersion", targetMigrantKeySetVersion))
//                                  .filter(optionalFilterByHeader("MigrationID", migrationID))
//                                  .collect(toList());
//    }
//
//    private Predicate<String> optionalFilterByHeader(String headerName, String headerValue) {
//        return it -> headerValue == null || filterByHeader(headerName, headerValue).test(it);
//    }
//
//    private Predicate<String> filterByHeader(String headerName, String headerValue) {
//        return it -> Optional.of(headerValue)
//                             .map(itt -> headerName + ": " + itt)
//                             .filter(it::contains)
//                             .isPresent();
//    }

}
