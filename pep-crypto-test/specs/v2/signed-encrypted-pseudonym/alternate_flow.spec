# Validation and/or decryption SignedEncryptedPseudonym-v2 fails - Alternate Flow

tags: signed-encrypted-pseudonym, alternate-flow, @bsnkeiddo-5635@

* Load dataset from "/v2/signed-encrypted-pseudonym/alternate_flow.yaml"
* Target default endpoint "/signed-encrypted-pseudonym"

### Given

A SignedEncryptedPseudonym-v2, SP-key, schemekey and closingkey is provided as input for the decryption component

### When

The SignedEncryptedPseudonym-v2 and/or keys are invalid or the combination does not match

### Then

No Pseudonym is returned in the response message with corresponding error status

### Description

Mapping AC tests:

Alternate flow
-- invalid signed EP (signature won't validate)
-- mismatch between key-material and EP:
--- different OINs
--- different schemeKeySetVersion
--- different recipientKeySetVersions
-- input not EP
-- input EP, unknown structure
-- input EP schemeVersion  <> '1'
-- input missing required key


## PTC_5635_1: SignedEncryptedPseudonym invalid signed

tags: @bsnkeiddo-5640@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_1.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_1.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_1.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_1.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_1.expectations.statusCode|string          }}|
* Expect response body contains "SIGNATURE_VERIFICATION_FAILED"
* Fail if expectations are not met

## PTC_5635_2: Mismatch between SignedEncryptedPseudonym OIN and pdd OIN

tags: @bsnkeiddo-5641@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_2.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_2.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_2.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_2.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_2.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Expect response body contains "SERVICE_PROVIDER_KEYS_RECIPIENT_NON_UNIQUE"
* Fail if expectations are not met

## PTC_5635_3: Mismatch between SignedEncryptedPseudonym OIN and pcd OIN

tags: @bsnkeiddo-5642@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_3.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_3.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_3.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_3.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_3.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Expect response body contains "SERVICE_PROVIDER_KEYS_RECIPIENT_NON_UNIQUE"
* Fail if expectations are not met

## PTC_5635_4: Mismatch between SignedEncryptedPseudonym SKSV and pdd SKSV

tags: @bsnkeiddo-5643@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_4.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_4.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_4.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_4.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_4.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5635_5: Mismatch between SignedEncryptedPseudonym SKSV and pcd SKSV

tags: @bsnkeiddo-5644@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_5.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_5.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_5.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_5.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_5.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5635_6: Mismatch between SignedEncryptedPseudonym SKSV and ipp SKSV

tags: @bsnkeiddo-5645@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_6.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_6.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_6.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_6.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_6.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SCHEME_KEY_NOT_FOUND"
* Fail if expectations are not met

## PTC_5635_7: Mismatch between SignedEncryptedPseudonym RKSV and pdd RKSV

tags: @bsnkeiddo-5646@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_7.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_7.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_7.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_7.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_7.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5635_8: Mismatch between SignedEncryptedPseudonym RKSV and pcd RKSV

tags: @bsnkeiddo-5647@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_8.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_8.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_8.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_8.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_8.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5635_9: Target closing key doesn't match the closing key

tags: @bsnkeiddo-5648@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_9.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_9.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_9.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_9.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5635_9.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5635_10: Input is a SignedEncryptedIdentity instead of a SignedEncryptedPseudonym

tags: @bsnkeiddo-5649@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                  |propertyFilter        |
   |------------------------|-----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_10.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_10.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_10.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_10.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5635_10.expectations.statusCode|string          }}|
* Expect response body contains "OID_NOT_SUPPORTED"
* Fail if expectations are not met

## PTC_5635_11: Unknown structure of SignedEncryptedPseudonym

tags: @bsnkeiddo-5650@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                  |propertyFilter        |
   |------------------------|-----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_11.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_11.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_11.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_11.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5635_11.expectations.statusCode|string          }}|
* Expect response body contains "ASN1_SEQUENCE_DECODER"
* Fail if expectations are not met

## PTC_5635_12: Input missing service provider key

tags: @bsnkeiddo-5651@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                  |propertyFilter        |
   |------------------------|-----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_12.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_12.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_12.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_12.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5635_12.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5635_13: Input missing closing key

tags: @bsnkeiddo-5652@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                  |propertyFilter        |
   |------------------------|-----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_13.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_13.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_13.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_13.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5635_13.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5635_14: Input missing scheme key

tags: @bsnkeiddo-5653@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                  |propertyFilter        |
   |------------------------|-----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_14.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_14.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_14.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_14.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5635_14.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SCHEME_KEY_NOT_FOUND"
* Fail if expectations are not met

## PTC_5635_15: SignedEncryptedPseudonym schemeVersion <> 1 (SignedEncryptedPseudonym-v2 with(out) extraElements)

tags: @bsnkeiddo-5654@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                  |propertyFilter        |
   |------------------------|-----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_15.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_15.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_15.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_15.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5635_15.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5635_17: SignedEncryptedPseudonym invalid signed with diversifier

tags: @bsnkeiddo-5707@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                  |propertyFilter        |
   |------------------------|-----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_5635_17.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_5635_17.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_5635_17.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_5635_17.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5635_17.expectations.statusCode|string          }}|
* Expect response body contains "SIGNATURE_VERIFICATION_FAILED"
* Fail if expectations are not met

## PTC_5635_18: Target closing key SKSV doesn't match the closing key

tags: @bsnkeiddo-7192@

* Create OASSignedEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                             |propertyFilter        |
   |-----------------------------------|----------------------------------------------------------|----------------------|
   |signedEncryptedPseudonym           |{{ PTC_5635_18.request.signedEncryptedPseudonym           |binary              }}|
   |schemeKeys                         |{{ PTC_5635_18.request.schemeKeys                         |schemeKeys          }}|
   |serviceProviderKeys                |{{ PTC_5635_18.request.serviceProviderKeys                |serviceProviderKeys }}|
   |targetClosingKey                   |{{ PTC_5635_18.request.targetClosingKey                   |string              }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5635_18.request.targetClosingKeySchemeKeySetVersion|string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5635_18.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met
