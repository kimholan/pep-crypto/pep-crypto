# Successfully validate and decrypt SignedEncryptedPseudonym-v2 -Happy Flow

tags: signed-encrypted-pseudonym, happy-flow, @bsnkeiddo-5634@

* Load dataset from "/v2/signed-encrypted-pseudonym/happy_flow.yaml"
* Target default endpoint "/signed-encrypted-pseudonym"

### Given

A SignedEncryptedPseudonym-v2, SP-key, schemekey and closingkey is provided as input for the decryption component

### When

The SignedEncryptedPseudonym-v2 and keys are validated, the SignedEncryptedPseudonym is succesfully decrypted

### Then

The decrypted Pseudonym and the additional fields from EP/SignedEP are returned in the response message

### Description

Mapping AC tests:

happy flow
-- different OINs
-- different schemeKeySetVersion
-- different recipientKeySetVersions
-- different target recipientKeySetVersion for closingKey
-- ECSDSA signed
-- With/without extraElements


## PTC_5634_1: SchemeKeySetVersion 1 for OIN A, RKSV A, CKSV A - target closingKey = empty - (SignedEncryptedIdentity-v2 without extraElements)

tags: @bsnkeiddo-5636@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter       |
   |------------------------|----------------------------------------------|---------------------|
   |signedEncryptedPseudonym|{{ PTC_5634_1.request.signedEncryptedPseudonym|binary             }}|
   |schemeKeys              |{{ PTC_5634_1.request.schemeKeys              |schemeKeys         }}|
   |serviceProviderKeys     |{{ PTC_5634_1.request.serviceProviderKeys     |serviceProviderKeys}}|
   |targetClosingKey        |{{ PTC_5634_1.request.targetClosingKey        |string             }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_5634_1.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_5634_1.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_5634_1.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_5634_1.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_5634_1.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met

## PTC_5634_2: SchemeKeySetVersion 10 for OIN B, RKSV B, CKSV B - target closingKey = RKSV B - (SignedEncryptedIdentity-v2 with 1 extraElement)

tags: @bsnkeiddo-5637@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter       |
   |------------------------|----------------------------------------------|---------------------|
   |signedEncryptedPseudonym|{{ PTC_5634_2.request.signedEncryptedPseudonym|binary             }}|
   |schemeKeys              |{{ PTC_5634_2.request.schemeKeys              |schemeKeys         }}|
   |serviceProviderKeys     |{{ PTC_5634_2.request.serviceProviderKeys     |serviceProviderKeys}}|
   |targetClosingKey        |{{ PTC_5634_2.request.targetClosingKey        |string             }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_5634_2.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_5634_2.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_5634_2.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_5634_2.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_5634_2.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met

## PTC_5634_3: Additional key material supplied for inapplicable SchemeKeySetVersion or RecipientKeySetVersion - target closingKey = empty (SignedEncryptedIdentity-v2 multiple extraElements)

tags: @bsnkeiddo-5638@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter       |
   |------------------------|----------------------------------------------|---------------------|
   |signedEncryptedPseudonym|{{ PTC_5634_3.request.signedEncryptedPseudonym|binary             }}|
   |schemeKeys              |{{ PTC_5634_3.request.schemeKeys              |schemeKeys         }}|
   |serviceProviderKeys     |{{ PTC_5634_3.request.serviceProviderKeys     |serviceProviderKeys}}|
   |targetClosingKey        |{{ PTC_5634_3.request.targetClosingKey        |string             }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_5634_3.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_5634_3.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_5634_3.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_5634_3.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_5634_3.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met

## PTC_5634_4: RKSV C, CKSV B - target closingKey = RKSV B + 1 diversifierKeyValuePair

tags: @bsnkeiddo-5639@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter       |
   |------------------------|----------------------------------------------|---------------------|
   |signedEncryptedPseudonym|{{ PTC_5634_4.request.signedEncryptedPseudonym|binary             }}|
   |schemeKeys              |{{ PTC_5634_4.request.schemeKeys              |schemeKeys         }}|
   |serviceProviderKeys     |{{ PTC_5634_4.request.serviceProviderKeys     |serviceProviderKeys}}|
   |targetClosingKey        |{{ PTC_5634_4.request.targetClosingKey        |string             }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_5634_4.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_5634_4.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_5634_4.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_5634_4.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_5634_4.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met

## PTC_5634_5: EP in obv SKSV1 en RKSV A, PCD obv SKSV10 en CKV A - targetClosingKey=empty, targetClosingKeySKSV=10

tags: @bsnkeiddo-7188@

* Create OASSignedEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                            |propertyFilter       |
   |-----------------------------------|---------------------------------------------------------|---------------------|
   |signedEncryptedPseudonym           |{{ PTC_5634_5.request.signedEncryptedPseudonym           |binary             }}|
   |schemeKeys                         |{{ PTC_5634_5.request.schemeKeys                         |schemeKeys         }}|
   |serviceProviderKeys                |{{ PTC_5634_5.request.serviceProviderKeys                |serviceProviderKeys}}|
   |targetClosingKey                   |{{ PTC_5634_5.request.targetClosingKey                   |string             }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5634_5.request.targetClosingKeySchemeKeySetVersion|string             }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_5634_5.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_5634_5.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_5634_5.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_5634_5.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_5634_5.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met


## PTC_5634_6: EP in obv SKSV1 en RKSV A, PCD obv SKSV10 en CKV B - targetClosingKey=B, targetClosingKeySKSV=10

tags: @bsnkeiddo-7189@

* Create OASSignedEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                            |propertyFilter       |
   |-----------------------------------|---------------------------------------------------------|---------------------|
   |signedEncryptedPseudonym           |{{ PTC_5634_6.request.signedEncryptedPseudonym           |binary             }}|
   |schemeKeys                         |{{ PTC_5634_6.request.schemeKeys                         |schemeKeys         }}|
   |serviceProviderKeys                |{{ PTC_5634_6.request.serviceProviderKeys                |serviceProviderKeys}}|
   |targetClosingKey                   |{{ PTC_5634_6.request.targetClosingKey                   |string             }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5634_6.request.targetClosingKeySchemeKeySetVersion|string             }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_5634_6.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_5634_6.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_5634_6.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_5634_6.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_5634_6.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met


## PTC_5634_7: EP in obv SKSV10 en RKSV A, PCD obv SKSV1 en CKV A - targetClosingKey=A, targetClosingKeySKSV=1

tags: @bsnkeiddo-7190@

* Create OASSignedEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                            |propertyFilter       |
   |-----------------------------------|---------------------------------------------------------|---------------------|
   |signedEncryptedPseudonym           |{{ PTC_5634_7.request.signedEncryptedPseudonym           |binary             }}|
   |schemeKeys                         |{{ PTC_5634_7.request.schemeKeys                         |schemeKeys         }}|
   |serviceProviderKeys                |{{ PTC_5634_7.request.serviceProviderKeys                |serviceProviderKeys}}|
   |targetClosingKey                   |{{ PTC_5634_7.request.targetClosingKey                   |string             }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5634_7.request.targetClosingKeySchemeKeySetVersion|string             }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_5634_7.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_5634_7.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_5634_7.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_5634_7.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_5634_7.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met


## PTC_5634_8: EP in obv SKSV10 en RKSV A, PCD obv SKSV1 en CKV B - targetClosingKey=B, targetClosingKeySKSV=1

tags: @bsnkeiddo-7191@

* Create OASSignedEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                            |propertyFilter       |
   |-----------------------------------|---------------------------------------------------------|---------------------|
   |signedEncryptedPseudonym           |{{ PTC_5634_8.request.signedEncryptedPseudonym           |binary             }}|
   |schemeKeys                         |{{ PTC_5634_8.request.schemeKeys                         |schemeKeys         }}|
   |serviceProviderKeys                |{{ PTC_5634_8.request.serviceProviderKeys                |serviceProviderKeys}}|
   |targetClosingKey                   |{{ PTC_5634_8.request.targetClosingKey                   |string             }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5634_8.request.targetClosingKeySchemeKeySetVersion|string             }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_5634_8.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_5634_8.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_5634_8.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_5634_8.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_5634_8.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met
