# Validation and/or decryption SignedDirectEncryptedPseudonym-v2 fails - Alternate Flow

tags: signed-direct-encrypted-pseudonym, alternate-flow, @bsnkeiddo-5676@

* Load dataset from "/v2/signed-direct-encrypted-pseudonym/alternate_flows.yaml"
* Target default endpoint "/signed-direct-encrypted-pseudonym"

### Given

A SignedDirectEncryptedPseudonym-v2, SP-key, schemekey and closingkey is provided as input for the decryption component

### When

The SignedDirectEncryptedPseudonym-v2 and/or keys are invalid or the combination does not match

### Then

No Pseudonym is returned in the response message with corresponding error status

### Description

error flows
-- invalid signed DEP (signature won't validate)
-- mismatch between key-material and SDEP:
--- different OINs
--- different schemeKeySetVersion
--- different recipientKeySetVersions
--- different signingKeyVersion
--- different AuthorizedParty
-- input not SDEP
-- input SDEP, unknown structure
-- input missing required key



## PTC_5676_1: SignedDirectEncryptedPseudonym-v2 invalid signed

tags: @bsnkeiddo-5737@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_1.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_1.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_1.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_1.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_1.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_1.expectations.statusCode|string          }}|
* Expect response body contains "SIGNATURE_VERIFICATION_FAILED"
* Fail if expectations are not met

## PTC_5676_2: Mismatch between SignedDirectEncryptedPseudonym-v2 OIN and pdd OIN - with extra elements

tags: @bsnkeiddo-5738@

Only the pdd OIN is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_2.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_2.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_2.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_2.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_2.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_2.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Expect response body contains "SERVICE_PROVIDER_KEYS_RECIPIENT_NON_UNIQUE"
* Fail if expectations are not met

## PTC_5676_3: Mismatch between SignedDirectEncryptedPseudonym-v2 OIN and pcd OIN

tags: @bsnkeiddo-5739@

Only the pcd OIN is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_3.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_3.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_3.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_3.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_3.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_3.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Expect response body contains "SERVICE_PROVIDER_KEYS_RECIPIENT_NON_UNIQUE"
* Fail if expectations are not met

## PTC_5676_4: Mismatch between SignedDirectEncryptedPseudonym-v2 OIN and drk OIN

tags: @bsnkeiddo-5740@

Only the drk OIN is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_4.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_4.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_4.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_4.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_4.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_4.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_DIRECT_RECEIVE_KEY_REQUIRED"
* Expect response body contains "SERVICE_PROVIDER_KEYS_RECIPIENT_NON_UNIQUE"
* Fail if expectations are not met

## PTC_5676_5: Mismatch between SignedDirectEncryptedPseudonym-v2 SKSV and pdd SKSV - with extra elements

tags: @bsnkeiddo-5741@

Only the pdd SchemeKeySetVersion is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_5.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_5.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_5.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_5.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_5.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_5.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_6: Mismatch between SignedDirectEncryptedPseudonym-v2 SKSV and pcd SKSV

tags: @bsnkeiddo-5742@

Only the pcd SchemeKeySetVersion is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_6.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_6.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_6.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_6.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_6.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_6.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_7: Mismatch between SignedDirectEncryptedPseudonym-v2 SKSV and drk SKSV

tags: @bsnkeiddo-5743@

Only the drk SchemeKeySetVersion is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_7.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_7.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_7.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_7.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_7.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_7.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_DIRECT_RECEIVE_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_8: Mismatch between SignedDirectEncryptedPseudonym-v2 SKSV and U-key SKSV

tags: @bsnkeiddo-5744@

Only the U-key SchemeKeySetVersion is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_8.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_8.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_8.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_8.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_8.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_8.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SCHEME_KEY_NOT_FOUND"
* Fail if expectations are not met

## PTC_5676_9: Mismatch between SignedDirectEncryptedPseudonym-v2 RKSV and pdd RKSV

tags: @bsnkeiddo-5745@

Only the pdd RecipientKeySetVersion is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter        |
   |------------------------------|----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_9.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_9.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_9.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_9.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_9.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_5676_9.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_10: Mismatch between SignedDirectEncryptedPseudonym-v2 RKSV and pcd RKSV - with extra elements

tags: @bsnkeiddo-5746@

Only the pcd RecipientKeySetVersion is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_10.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_10.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_10.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_10.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_10.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_10.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_11: Mismatch between SignedDirectEncryptedPseudonym-v2 RKSV and drk RKSV

tags: @bsnkeiddo-5747@

Only the drk RecipientKeySetVersion is different from the rest
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_11.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_11.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_11.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_11.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_11.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_11.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_DIRECT_RECEIVE_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_12: Mismatch between SignedDirectEncryptedPseudonym-v2 authorizedParty and authorizedParty input

tags: @bsnkeiddo-5748@

A different authorizedParty OIN is given as input
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_12.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_12.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_12.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_12.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_12.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_12.expectations.statusCode|string          }}|
* Expect response body contains "DIRECT_TRANSMISSION_DECRYPTION_NOT_AUTHORIZED"
* Fail if expectations are not met

## PTC_5676_13: Target closing key doesn't match the closing key

tags: @bsnkeiddo-5749@

A different targetClosingKey OIN is given as input
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_13.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_13.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_13.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_13.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_13.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_13.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_14: Input is a SignedEncryptedPseudonym-v2 instead of a SignedDirectEncryptedPseudonym-v2

tags: @bsnkeiddo-5750@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_14.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_14.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_14.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_14.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_14.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_14.expectations.statusCode|string          }}|
* Expect response body contains "OID_NOT_SUPPORTED"
* Fail if expectations are not met

## PTC_5676_15: Unknown structure of SignedDirectEncryptedPseudonym-v2

tags: @bsnkeiddo-5751@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_15.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_15.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_15.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_15.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_15.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_15.expectations.statusCode|string          }}|
* Expect response body contains "ASN1_MAPPING_FAILED; PEP_SCHEMA_ASN1_DECODE"
* Fail if expectations are not met

## PTC_5676_16: Input missing pdd service provider key

tags: @bsnkeiddo-5752@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_16.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_16.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_16.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_16.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_16.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_16.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_17: Input missing drk service provider key

tags: @bsnkeiddo-5753@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_17.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_17.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_17.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_17.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_17.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_17.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_DIRECT_RECEIVE_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_18: Input missing closing key

tags: @bsnkeiddo-5754@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_18.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_18.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_18.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_18.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_18.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_18.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_5676_19: Input missing U-key

tags: @bsnkeiddo-5755@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_19.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_19.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_19.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_19.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_19.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_19.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SCHEME_KEY_NOT_FOUND"
* Fail if expectations are not met

## PTC_5676_20: Input missing authorizedParty - with extra elements

tags: @bsnkeiddo-5756@

AuthorizedParty as separate input field is empty
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_20.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_20.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_20.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_20.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_20.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_20.expectations.statusCode|string          }}|
* Expect response body contains "DIRECT_TRANSMISSION_DECRYPTION_NOT_AUTHORIZED"
* Fail if expectations are not met

## PTC_5676_21: Input authorizedParty malformed

tags: @bsnkeiddo-5757@

AuthorizedParty as separate input field doens't contains 20 digits
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_21.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_21.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_21.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_21.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_21.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_21.expectations.statusCode|string          }}|
* Expect response body contains "processRequest.request.authorizedParty"
* Fail if expectations are not met

## PTC_5676_22: Key version from URN is applied for selecting the U-key

tags: @bsnkeiddo-5758@

No URN for U-key matches signedDirectEncryptedPseudonym, correct U-key available for URN with different key version.
* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_22.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_22.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_22.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_22.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_22.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_22.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SCHEME_KEY_NOT_FOUND"
* Fail if expectations are not met

## PTC_5676_23: Key selection restricted to URN

tags: @bsnkeiddo-5759@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_23.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_23.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_23.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_23.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_23.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_23.expectations.statusCode|string          }}|
* Expect response body contains "SIGNATURE_VERIFICATION_FAILED"
* Fail if expectations are not met

## PTC_5676_24: DEP-v2 contains diversifier, DRKi has no diversifier - with extra elements

tags: @bsnkeiddo-5760@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_24.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_24.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_24.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_24.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_24.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_24.expectations.statusCode|string          }}|
* Expect response body contains "NO_MATCHES_DIRECT_RECEIVE_KEY"
* Fail if expectations are not met

## PTC_5676_25: DRKi contains diversifier, has no diversifier - without extra elements

tags: @bsnkeiddo-5761@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_25.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_25.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_25.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_25.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_25.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_25.expectations.statusCode|string          }}|
* Expect response body contains "NO_MATCHES_DIRECT_RECEIVE_KEY"
* Fail if expectations are not met

## PTC_5676_26: DEP contains diversifier different from DRKi diversifier - with extra elements

tags: @bsnkeiddo-5762@

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                        |propertyFilter        |
   |------------------------------|-----------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5676_26.request.signedDirectEncryptedPseudonym|binary              }}|
   |schemeKeys                    |{{ PTC_5676_26.request.schemeKeys                    |schemeKeys          }}|
   |serviceProviderKeys           |{{ PTC_5676_26.request.serviceProviderKeys           |serviceProviderKeys }}|
   |targetClosingKey              |{{ PTC_5676_26.request.targetClosingKey              |string              }}|
   |authorizedParty               |{{ PTC_5676_26.request.authorizedParty               |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_26.expectations.statusCode|string          }}|
* Expect response body contains "NO_MATCHES_DIRECT_RECEIVE_KEY"
* Fail if expectations are not met

## PTC_5676_27: Target closing key SKSV doesn't match the closing key

tags: @bsnkeiddo-7198@

A different targetClosingKey SKSV is given as input
* Create OASSignedDirectEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                             |propertyFilter        |
   |-----------------------------------|----------------------------------------------------------|----------------------|
   |signedDirectEncryptedPseudonym     |{{ PTC_5676_27.request.signedDirectEncryptedPseudonym     |binary              }}|
   |schemeKeys                         |{{ PTC_5676_27.request.schemeKeys                         |schemeKeys          }}|
   |serviceProviderKeys                |{{ PTC_5676_27.request.serviceProviderKeys                |serviceProviderKeys }}|
   |targetClosingKey                   |{{ PTC_5676_27.request.targetClosingKey                   |string              }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5676_27.request.targetClosingKeySchemeKeySetVersion|string              }}|
   |authorizedParty                    |{{ PTC_5676_27.request.authorizedParty                    |string              }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_5676_27.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_CLOSING_KEY_REQUIRED"
* Fail if expectations are not met
