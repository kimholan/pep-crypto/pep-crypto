# Successfully validate and decrypt SignedDirectEncryptedPseudonym-v2 - Happy Flow

tags: signed-direct-encrypted-pseudonym, happy-flow, @bsnkeiddo-5675@

* Load dataset from "/v2/signed-direct-encrypted-pseudonym/happy_flow.yaml"
* Target default endpoint "/signed-direct-encrypted-pseudonym"

### Given

A SignedDirectEncryptedPseudonym-v2, SP-key, schemekey and closingkey is provided as input for the decryption component

### When

The SignedDirectEncryptedPseudonym-v2 and keys are validated, the SignedDirectEncryptedPseudonym is succesfully decrypted

### Then

The decrypted Pseudonym and the additional fields from DEP/SignedDEP are returned in the response message

### Description

Mapping AC tests:

happy flow
-- different OINs
-- different schemeKeySetVersion
-- different recipientKeySetVersions
-- ECDSA signed
-- With/without extraElements
-- different signingKeyVersion
-- different AuthorizedParty
-- with/without diversifier




## PTC_5675_1: DEP-v2 decryption - SKSV 1 for OIN A, RKSV A, authorizedPary A - target closingKey = empty, no extraElements

tags: @bsnkeiddo-5732@

Decryption applies the closing key for the recipient key set version matching the SignedDirectEncryptedPseudonym
if no specific closing key version is specified separately by 'targetClosingKey'-property.

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter           |
   |------------------------------|----------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5675_1.request.signedDirectEncryptedPseudonym|binary                 }}|
   |schemeKeys                    |{{ PTC_5675_1.request.schemeKeys                    |schemeKeys             }}|
   |serviceProviderKeys           |{{ PTC_5675_1.request.serviceProviderKeys           |serviceProviderKeys    }}|
   |targetClosingKey              |{{ PTC_5675_1.request.targetClosingKey              |string                 }}|
   |authorizedParty               |{{ PTC_5675_1.request.authorizedParty               |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_1.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_1.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_1.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_1.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_1.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met

## PTC_5675_2: DEP-v2 decryption - SKSV10 for OIN B, RKSV B, CKSV B, DRK B, authorizedParty B - target closingKey B- 1 extraElements of each type + 2 DiversifierKeyValuePairs

tags: @bsnkeiddo-5733@

Decryption applies the closing key for the recipient key set version differing from the SignedDirectEncryptedPseudonym
if such a closing key is present in the 'serviceProviderKeys'-property and the differing version is
specified by 'targetClosingKey'-property.

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter           |
   |------------------------------|----------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5675_2.request.signedDirectEncryptedPseudonym|binary                 }}|
   |schemeKeys                    |{{ PTC_5675_2.request.schemeKeys                    |schemeKeys             }}|
   |serviceProviderKeys           |{{ PTC_5675_2.request.serviceProviderKeys           |serviceProviderKeys    }}|
   |targetClosingKey              |{{ PTC_5675_2.request.targetClosingKey              |string                 }}|
   |authorizedParty               |{{ PTC_5675_2.request.authorizedParty               |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_2.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_2.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_2.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_2.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_2.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met

## PTC_5675_3: DEP-v2 decryption - SKSV 1 for OIN A, RKSV A, authorizedPary A - multiple extraElements of each type + duplicate keys

tags: @bsnkeiddo-5734@

Key selection is not affected by supplying one or more extra U-key, PD_D-key, DRKi- or PC_D-keys belonging
to the same service provider.

The decryption result for recipientKeySetVersion A with targetClosingKey C should match the
second decryption for recipientKeySetVersion C and targetClosingKey C, even though the
decryption keys have differing recipientKeySetVersion values.

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter           |
   |------------------------------|----------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5675_3.request.signedDirectEncryptedPseudonym|binary                 }}|
   |schemeKeys                    |{{ PTC_5675_3.request.schemeKeys                    |schemeKeys             }}|
   |serviceProviderKeys           |{{ PTC_5675_3.request.serviceProviderKeys           |serviceProviderKeys    }}|
   |targetClosingKey              |{{ PTC_5675_3.request.targetClosingKey              |string                 }}|
   |authorizedParty               |{{ PTC_5675_3.request.authorizedParty               |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_3.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_3.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_3.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_3.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_3.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met

## PTC_5675_4: DEP-v2 decryption- additional SP-/schemeKey of each type and different closingKeyVerison + 1 extraElements + 1 DiversifierKeyValuePair

tags: @bsnkeiddo-5735@

Decryption applies the closing key for the recipient key set version differing from the SignedDirectEncryptedPseudonym
if such a closing key is present in the 'serviceProviderKeys'-property and the differing version is
specified by 'targetClosingKey'-property.

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter           |
   |------------------------------|----------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5675_4.request.signedDirectEncryptedPseudonym|binary                 }}|
   |schemeKeys                    |{{ PTC_5675_4.request.schemeKeys                    |schemeKeys             }}|
   |serviceProviderKeys           |{{ PTC_5675_4.request.serviceProviderKeys           |serviceProviderKeys    }}|
   |targetClosingKey              |{{ PTC_5675_4.request.targetClosingKey              |string                 }}|
   |authorizedParty               |{{ PTC_5675_4.request.authorizedParty               |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_4.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_4.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_4.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_4.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_4.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met

## PTC_5675_5: DEP-v2 decryption - multiple U-schemekeys supplied with identical schemeKeySetVersion + multiple extraElements of each type

tags: @bsnkeiddo-5736@

Key selection is not affected by supplying 1 extra U-key with identical schemeKeySetVersion and a different keyVersion.

* Create OASSignedDirectEncryptedPseudonymRequest 

   |propertyName                  |propertyValue                                       |propertyFilter           |
   |------------------------------|----------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym|{{ PTC_5675_5.request.signedDirectEncryptedPseudonym|binary                 }}|
   |schemeKeys                    |{{ PTC_5675_5.request.schemeKeys                    |schemeKeys             }}|
   |serviceProviderKeys           |{{ PTC_5675_5.request.serviceProviderKeys           |serviceProviderKeys    }}|
   |targetClosingKey              |{{ PTC_5675_5.request.targetClosingKey              |string                 }}|
   |authorizedParty               |{{ PTC_5675_5.request.authorizedParty               |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_5.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_5.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_5.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_5.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_5.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met


## PTC_5675_6: DEP in obv SKSV1 en RKSV A, PCD obv SKSV10 en CKV A - target closingKey = empty, targetClosingKeySKSV=10


Decryption applies the closing key for the recipient key set version differing from the SignedDirectEncryptedPseudonym
if such a closing key is present in the 'serviceProviderKeys'-property and the differing version is
specified by 'targetClosingKey' and 'targetClosingKeySchemeKeySet' properties.

tags: @bsnkeiddo-7194@

* Create OASSignedDirectEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                            |propertyFilter           |
   |-----------------------------------|---------------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym     |{{ PTC_5675_6.request.signedDirectEncryptedPseudonym     |binary                 }}|
   |schemeKeys                         |{{ PTC_5675_6.request.schemeKeys                         |schemeKeys             }}|
   |serviceProviderKeys                |{{ PTC_5675_6.request.serviceProviderKeys                |serviceProviderKeys    }}|
   |targetClosingKey                   |{{ PTC_5675_6.request.targetClosingKey                   |string                 }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5675_6.request.targetClosingKeySchemeKeySetVersion|string                 }}|
   |authorizedParty                    |{{ PTC_5675_6.request.authorizedParty                    |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_6.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_6.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_6.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_6.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_6.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met

## PTC_5675_7: DEP in obv SKSV1 en RKSV A, PCD obv SKSV10 en CKV B - target closingKey=B, targetClosingKeySKSV=10


Decryption applies the closing key for the recipient key set version differing from the SignedDirectEncryptedPseudonym
if such a closing key is present in the 'serviceProviderKeys'-property and the differing version is
specified by 'targetClosingKey' and 'targetClosingKeySchemeKeySet' properties.

tags: @bsnkeiddo-7195@

* Create OASSignedDirectEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                            |propertyFilter           |
   |-----------------------------------|---------------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym     |{{ PTC_5675_7.request.signedDirectEncryptedPseudonym     |binary                 }}|
   |schemeKeys                         |{{ PTC_5675_7.request.schemeKeys                         |schemeKeys             }}|
   |serviceProviderKeys                |{{ PTC_5675_7.request.serviceProviderKeys                |serviceProviderKeys    }}|
   |targetClosingKey                   |{{ PTC_5675_7.request.targetClosingKey                   |string                 }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5675_7.request.targetClosingKeySchemeKeySetVersion|string                 }}|
   |authorizedParty                    |{{ PTC_5675_7.request.authorizedParty                    |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_7.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_7.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_7.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_7.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_7.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met

## PTC_5675_8: DEP in obv SKSV10 en RKSV A, PCD obv SKSV1 en CKV A - target closingKey=A, targetClosingKeySKSV=1


Decryption applies the closing key for the recipient key set version differing from the SignedDirectEncryptedPseudonym
if such a closing key is present in the 'serviceProviderKeys'-property and the differing version is
specified by 'targetClosingKey' and 'targetClosingKeySchemeKeySet' properties.

tags: @bsnkeiddo-7196@

* Create OASSignedDirectEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                            |propertyFilter           |
   |-----------------------------------|---------------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym     |{{ PTC_5675_8.request.signedDirectEncryptedPseudonym     |binary                 }}|
   |schemeKeys                         |{{ PTC_5675_8.request.schemeKeys                         |schemeKeys             }}|
   |serviceProviderKeys                |{{ PTC_5675_8.request.serviceProviderKeys                |serviceProviderKeys    }}|
   |targetClosingKey                   |{{ PTC_5675_8.request.targetClosingKey                   |string                 }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5675_8.request.targetClosingKeySchemeKeySetVersion|string                 }}|
   |authorizedParty                    |{{ PTC_5675_8.request.authorizedParty                    |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_8.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_8.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_8.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_8.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_8.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met

## PTC_5675_9: DEP in obv SKSV10 en RKSV A, PCD obv SKSV1 en CKV B  - target closingKey=B, targetClosingKeySKSV=1


Decryption applies the closing key for the recipient key set version differing from the SignedDirectEncryptedPseudonym
if such a closing key is present in the 'serviceProviderKeys'-property and the differing version is
specified by 'targetClosingKey' and 'targetClosingKeySchemeKeySet' properties.

tags: @bsnkeiddo-7197@

* Create OASSignedDirectEncryptedPseudonymRequest

   |propertyName                       |propertyValue                                            |propertyFilter           |
   |-----------------------------------|---------------------------------------------------------|-------------------------|
   |signedDirectEncryptedPseudonym     |{{ PTC_5675_9.request.signedDirectEncryptedPseudonym     |binary                 }}|
   |schemeKeys                         |{{ PTC_5675_9.request.schemeKeys                         |schemeKeys             }}|
   |serviceProviderKeys                |{{ PTC_5675_9.request.serviceProviderKeys                |serviceProviderKeys    }}|
   |targetClosingKey                   |{{ PTC_5675_9.request.targetClosingKey                   |string                 }}|
   |targetClosingKeySchemeKeySetVersion|{{ PTC_5675_9.request.targetClosingKeySchemeKeySetVersion|string                 }}|
   |authorizedParty                    |{{ PTC_5675_9.request.authorizedParty                    |string                 }}|
* Send OASSignedDirectEncryptedPseudonymRequest
* Expect response matches

   |actualValue                             |expectedValue                            |expectedValueType        |
   |----------------------------------------|-----------------------------------------|-------------------------|
   |statusCode                              |{{ PTC_5675_9.expectations.statusCode    |string                 }}|
   |body                                    |{{ PTC_5675_9.expectations.responseBody  |json                   }}|
   |json.decodedInput.signedDEP.auditElement|{{ PTC_5675_9.expectations.auditValue    |string_resource        }}|
   |json.decodedInput.signatureValue        |{{ PTC_5675_9.expectations.signatureValue|string_signature       }}|
   |json.decodedPseudonym.pseudonymValue    |{{ PTC_5675_9.expectations.pseudonymValue|string_resource        }}|
* Fail if expectations are not met
