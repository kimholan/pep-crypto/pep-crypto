# Verify Link: SignedEncryptedPseudonym-v2 - Happy Flow

tags: verify-link, signed-encrypted-pseudonym-v2, happy-flow, @bsnkeiddo-6106@

* Load dataset from "/v2/verify-link/signed-encrypted-pseudonym-v2-hf.yaml"
* Target default endpoint "/verify-link"

### Given

A SignedEncryptedPseudonym-v2 with SameID hash of one ore more related SignedEncryptedIdentities-v2
or SignedEncryptedPseudonym-v2s, SP-key and schemekey are provided as input for the decryption component

### When

The SignedEncryptedIdentity-v2/SignedEncryptedPseudonym-v2 and keys are valid and all the provided identies have an verified link

### Then

Link verification successful, corresponding linked OINS are returned

### Description

Happy flow:
-- EI met gelinkte EP en/of EI (1 of meerdere, met of zonder extraElements)
-- EP met gelinkte EI en/of EP (1 of meerdere, met of zonder extraElements)
-- EP met gelinkte EP met diversifier (1 of meerdere, met of zonder extraElements)
-- HoofdIdentiteit met X SameID's, en < X GerelateerdeIdentiteiten


## PTC_6106_1: SignedEncryptedPseudonym-v2 with sameID hash of linked SEI-v2

tags: @bsnkeiddo-6121@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_1.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_1.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_1.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_1.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_1.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_1.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_2: SignedEncryptedPseudonym-v2 with sameID hashes of 3 linked SEI-v2s from different recepients

tags: @bsnkeiddo-6122@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_2.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_2.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_2.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_2.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_2.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_2.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_3: SignedEncryptedPseudonym-v2 with sameID hash of linked SEP-v2

tags: @bsnkeiddo-6123@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_3.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_3.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_3.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_3.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_3.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_3.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_4: SignedEncryptedPseudonym-v2 with sameID hashes of 3 linked SEP-v2s from the same recepient

tags: @bsnkeiddo-6124@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_4.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_4.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_4.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_4.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_4.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_4.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_5: SignedEncryptedPseudonym-v2 with sameID hashes of linked SEI-v2 and linked SEP-v2

tags: @bsnkeiddo-6125@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_5.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_5.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_5.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_5.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_5.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_5.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_6: SignedEncryptedPseudonym-v2 with Diversifier and sameID hash of linked SEP-v2 with diversifier

tags: @bsnkeiddo-6126@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_6.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_6.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_6.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_6.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_6.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_6.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_7: SignedEncryptedPseudonym-v2 with Diversifier and sameID hashes of 2 linked SEP-v2s with diversifier and 1 linked SEI-v2

tags: @bsnkeiddo-6127@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_7.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_7.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_7.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_7.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_7.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_7.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_8: SignedEncryptedPseudonym-v2 with extraElements and sameID hash of linked SEI-v2

tags: @bsnkeiddo-6128@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_8.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_8.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_8.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_8.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_8.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_8.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_9: SignedEncryptedPseudonym-v2 with extraElements and sameID hash of linked SEP-v2

tags: @bsnkeiddo-6129@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_9.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_9.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_9.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_9.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6106_9.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_9.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_10: SignedEncryptedPseudonym-v2 with sameID hash of linked SEI-v2 with extraElements

tags: @bsnkeiddo-6130@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_10.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_10.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_10.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_10.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6106_10.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_10.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_11: SignedEncryptedPseudonym-v2 with sameID hash of linked SEP-v2 with extraElements

tags: @bsnkeiddo-6131@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_11.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_11.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_11.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_11.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6106_11.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_11.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_12: SignedEncryptedPseudonym-v2 with extraElements and sameID hash of linked SEP-v2 with extraElements

tags: @bsnkeiddo-6132@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_12.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_12.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_12.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_12.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6106_12.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_12.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_13: SignedEncryptedPseudonym-v2 with extraElements and sameID hashes of 2 linked SEI-v2s with extraElement

tags: @bsnkeiddo-6133@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_13.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_13.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_13.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_13.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6106_13.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_13.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6106_14: SignedEncryptedPseudonym-v2 with 3 sameID hashes and 2 linked SEP-v2s + multiple schemeKeys and serviceProvederKeys

tags: @bsnkeiddo-6134@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6106_14.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6106_14.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6106_14.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6106_14.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6106_14.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6106_14.expectations.responseBody|json            }}|
* Fail if expectations are not met
