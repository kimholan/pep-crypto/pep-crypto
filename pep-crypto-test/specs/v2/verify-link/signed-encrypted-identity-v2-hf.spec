# Verify Link: SignedEncryptedIdentity-v2 - Happy Flow

tags: verify-link, signed-encrypted-identity-v2, happy-flow, @bsnkeiddo-6105@

* Load dataset from "/v2/verify-link/signed-encrypted-identity-v2-hf.yaml"
* Target default endpoint "/verify-link"

### Given

A SignedEncryptedIdentity-v2 with SameID hash of one ore more related SignedEncryptedIdentities
or SignedEncryptedPseudonym-v2s, SP-key and schemekey are provided as input for the decryption component

### When

The SignedEncryptedIdentity-v2/SignedEncryptedPseudonym-v2 and keys are valid and all the provided identies have an verified link

### Then

Link verification successful, corresponding linked OINS are returned

### Description

Happy flow:
-- EI met gelinkte EP en/of EI (1 of meerdere, met of zonder extraElements)
-- EP met gelinkte EI en/of EP (1 of meerdere, met of zonder extraElements)
-- EP met gelinkte EP met diversifier (1 of meerdere, met of zonder extraElements)
-- HoofdIdentiteit met X SameID's, en < X GerelateerdeIdentiteiten



## PTC_6105_1: SignedEncryptedIdentity-v2 with sameID hash of linked SEI-v2

tags: @bsnkeiddo-6108@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_1.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_1.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_1.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_1.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_1.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_1.expectations.responseBody|json            }}|

* Fail if expectations are not met

## PTC_6105_2: SignedEncryptedIdentity-v2 with sameID hashes of 3 linked SEI-v2s from the same recepient

tags: @bsnkeiddo-6109@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_2.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_2.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_2.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_2.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_2.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_2.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_3: SignedEncryptedIdentity-v2 with sameID hash of linked SEP-v2

tags: @bsnkeiddo-6110@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_3.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_3.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_3.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_3.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_3.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_3.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_4: SignedEncryptedIdentity-v2 with sameID hashes of 3 linked SEP-v2s from different recepients

tags: @bsnkeiddo-6111@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_4.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_4.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_4.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_4.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_4.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_4.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_5: SignedEncryptedIdentity-v2 with sameID hashes of linked SEI-v2 and linked SEP-v2

tags: @bsnkeiddo-6112@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_5.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_5.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_5.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_5.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_5.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_5.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_6: SignedEncryptedIdentity-v2 with extraElements and sameID hash of linked SEI-v2

tags: @bsnkeiddo-6113@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_6.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_6.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_6.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_6.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_6.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_6.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_7: SignedEncryptedIdentity-v2 with extraElements and sameID hash of linked SEP-v2

tags: @bsnkeiddo-6114@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_7.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_7.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_7.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_7.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_7.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_7.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_8: SignedEncryptedIdentity-v2 with sameID hash of linked SEI-v2 with extraElements

tags: @bsnkeiddo-6115@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_8.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_8.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_8.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_8.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_8.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_8.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_9: SignedEncryptedIdentity-v2 with sameID hash of linked SEP-v2 with extraElements

tags: @bsnkeiddo-6116@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_9.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_9.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_9.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_9.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6105_9.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_9.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_10: SignedEncryptedIdentity-v2 with extraElements and sameID hashes of 2 linked SEP-v2 with extraElements

tags: @bsnkeiddo-6117@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_10.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_10.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_10.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_10.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6105_10.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_10.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_11: SignedEncryptedIdentity-v2 with extraElements and sameID hash of linked SEI-v2 with extraElement

tags: @bsnkeiddo-6118@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_11.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_11.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_11.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_11.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6105_11.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_11.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_12: SignedEncryptedIdentity-v2 with sameID hash of linked SEP-v2 with diversifier

tags: @bsnkeiddo-6119@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_12.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_12.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_12.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_12.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6105_12.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_12.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6105_13: SignedEncryptedIdentity-v2 with 3 sameID hashes and 2 linked SEI-v2s + multiple schemeKeys and serviceProvederKeys

tags: @bsnkeiddo-6120@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6105_13.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6105_13.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6105_13.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6105_13.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6105_13.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6105_13.expectations.responseBody|json            }}|
* Fail if expectations are not met
