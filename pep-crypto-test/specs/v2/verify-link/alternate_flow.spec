# Verify Link: Alternate Flow
=============================

tags: verify-link, alternate-flow, @bsnkeiddo-6107@

* Load dataset from "/v2/verify-link/alternate_flow.yaml"
* Target default endpoint "/verify-link"

### Given

A SignedEncryptedIdentity-v2 or SignedEncryptedPseudonym-v2 with SameID hash of one ore more releated SignedEncryptedIdentity-v2's
or SignedEncryptedPseudonym-v2s, SP-key and schemekey are provided as input for the decryption component

### When

The SignedEncryptedIdentity-v2/SignedEncryptedPseudonym-v2 and/or keys are invalid or the combination does not match

### Then

Link verification fails, corresponding error status is returned

### Description

Alternate flows:
-- EI met link in extraElement + niet gerelateerde EP
-- EP met link in extraElement + niet gerelateerde EI
-- Input V1 structuur


## PTC_6107_1: DeprecatedSignedEncryptedPseudonym no linked identities

tags: @bsnkeiddo-6135@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_1.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_1.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_1.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_1.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_6107_1.expectations.statusCode|string          }}|
* Expect response body contains "processRequest.request.linkTargets"
* Expect response body contains "size must be between 1 and 2147483647"
* Fail if expectations are not met

## PTC_6107_2: DeprecatedSignedEncryptedIdentity no linked identities

tags: @bsnkeiddo-6136@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_2.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_2.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_2.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_2.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_6107_2.expectations.statusCode|string          }}|
* Expect response body contains "processRequest.request.linkTargets"
* Expect response body contains "size must be between 1 and 2147483647"
* Fail if expectations are not met

## PTC_6107_3: SignedEncryptedPseudonym-v2 without sameID hash

tags: @bsnkeiddo-6137@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_3.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_3.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_3.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_3.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_6107_3.expectations.statusCode|string          }}|

* Expect response body contains "size must be between 1 and 2147483647"
* Expect response body contains "processRequest.request.linkTargets"
* Fail if expectations are not met

## PTC_6107_4: SignedEncryptedIdentity-v2 without sameID hash

tags: @bsnkeiddo-6138@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_4.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_4.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_4.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_4.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_6107_4.expectations.statusCode|string          }}|

* Expect response body contains "size must be between 1 and 2147483647"
* Expect response body contains "processRequest.request.linkTargets"
* Fail if expectations are not met

## PTC_6107_5: 2 SignedEncryptedPseudonym-v2s, both without sameID hashes

tags: @bsnkeiddo-6139@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_5.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_5.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_5.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_5.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6107_5.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6107_5.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6107_6: 2 SignedEncryptedIdentity-v2's, both without sameID hashes

tags: @bsnkeiddo-6140@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_6.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_6.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_6.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_6.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6107_6.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6107_6.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6107_7: SignedEncryptedPseudonym-v2 with sameID hash which differs from the provided EP

tags: @bsnkeiddo-6143@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_7.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_7.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_7.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_7.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6107_7.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6107_7.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6107_8: SignedEncryptedIdentity-v2 with sameID hash which differs from the provided EI

tags: @bsnkeiddo-6144@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_8.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_8.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_8.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_8.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6107_8.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6107_8.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6107_9: SignedEncryptedIdentity-v2 with sameID hash of the provided EP + extra not related EP

tags: @bsnkeiddo-6145@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                            |propertyFilter        |
   |-------------------|-----------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_9.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_9.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_9.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_9.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                          |expectedValueType |
   |-----------|---------------------------------------|------------------|
   |statusCode |{{ PTC_6107_9.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6107_9.expectations.responseBody|json            }}|
* Fail if expectations are not met

## PTC_6107_10: SignedEncryptedPseudonym-v2 with sameID hash of the provided EI + extra not related EI

tags: @bsnkeiddo-6146@

* Create OASVerifyLinkRequest 

   |propertyName       |propertyValue                             |propertyFilter        |
   |-------------------|------------------------------------------|----------------------|
   |linkSource         |{{ PTC_6107_10.request.linkSource         |binary              }}|
   |linkTargets        |{{ PTC_6107_10.request.linkTargets        |binaryList          }}|
   |schemeKeys         |{{ PTC_6107_10.request.schemeKeys         |schemeKeys          }}|
   |serviceProviderKeys|{{ PTC_6107_10.request.serviceProviderKeys|serviceProviderKeys }}|
* Send OASVerifyLinkRequest
* Expect response matches 

   |actualValue|expectedValue                           |expectedValueType |
   |-----------|----------------------------------------|------------------|
   |statusCode |{{ PTC_6107_10.expectations.statusCode  |string          }}|
   |body       |{{ PTC_6107_10.expectations.responseBody|json            }}|
* Fail if expectations are not met
