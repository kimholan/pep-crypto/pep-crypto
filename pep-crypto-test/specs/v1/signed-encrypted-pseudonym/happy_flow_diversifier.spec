# Successfully validate and decrypt SignedEncryptedPseudonym with diversifier - Happy Flow

tags: signed-encrypted-pseudonym, diversifier, happy-flow, @bsnkeiddo-4786@

* Load dataset from "/v1/signed-encrypted-pseudonym/happy_flow_diversifier.yaml"
* Target default endpoint "/signed-encrypted-pseudonym"

### Given

A SignedEncryptedPseudonym with diversifier, SP-key, schemekey and closingkey is provided as input for the decryption component

### When

The SignedEncryptedPseudonym and keys are validated, the SignedEncryptedPseudonym is succesfully decrypted

### Then

The decrypted Pseudonym and the additional fields from EP/SignedEP are returned in the response message

### Description

Mapping AC tests:

happy flow
-- EP diversifier with 1 DiversifierKeyValuePair
-- EP diversifier with 2 DiversifierKeyValuePairs
-- EP diversifier with x DiversifierKeyValuePairs

The *auditElement* and *signatureValue* have no static values and will change when new test data is generated.
When new test data is generated by Logius, the corresponding expected test results are produced as well.



## PTC_4786_1: SignedEncryptedPseudonym - diversifier with 1 DiversifierKeyValuePair

tags: @bsnkeiddo-4803@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_4786_1.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_4786_1.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_4786_1.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_4786_1.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_4786_1.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_4786_1.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_4786_1.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_4786_1.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_4786_1.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met

## PTC_4786_2: SignedEncryptedPseudonym - diversifier with 2 DiversifierKeyValuePairs

tags: @bsnkeiddo-4804@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_4786_2.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_4786_2.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_4786_2.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_4786_2.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                            |expectedValue                            |expectedValueType  |
   |---------------------------------------|-----------------------------------------|-------------------|
   |statusCode                             |{{ PTC_4786_2.expectations.statusCode    |string           }}|
   |body                                   |{{ PTC_4786_2.expectations.responseBody  |json             }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_4786_2.expectations.auditValue    |string_resource  }}|
   |json.decodedInput.signatureValue       |{{ PTC_4786_2.expectations.signatureValue|string_signature }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_4786_2.expectations.pseudonymValue|string_resource  }}|
* Fail if expectations are not met

## PTC_4786_3: SignedEncryptedPseudonym - diversifier with 3 DiversifierKeyValuePairs

tags: @bsnkeiddo-4805@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_4786_3.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_4786_3.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_4786_3.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_4786_3.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue                            |expectedValue                            |expectedValueType   |
   |---------------------------------------|-----------------------------------------|--------------------|
   |statusCode                             |{{ PTC_4786_3.expectations.statusCode    |string            }}|
   |body                                   |{{ PTC_4786_3.expectations.responseBody  |json              }}|
   |json.decodedInput.signedEP.auditElement|{{ PTC_4786_3.expectations.auditValue    |string_resource   }}|
   |json.decodedInput.signatureValue       |{{ PTC_4786_3.expectations.signatureValue|string_signature  }}|
   |json.decodedPseudonym.pseudonymValue   |{{ PTC_4786_3.expectations.pseudonymValue|string_resource   }}|
* Fail if expectations are not met

