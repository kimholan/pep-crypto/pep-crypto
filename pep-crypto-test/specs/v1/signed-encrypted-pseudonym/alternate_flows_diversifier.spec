# Validation and/or decryption SignedEncryptedPseudonym with diversifier fails - Alternate Flow

tags: signed-encrypted-pseudonym, diversifier, alternate-flow, @bsnkeiddo-4788@

* Load dataset from "/v1/signed-encrypted-pseudonym/alternate_flows_diversifier.yaml"
* Target default endpoint "/signed-encrypted-pseudonym"

### Given

A SignedEncryptedPseudonym with diversifier, SP-key, schemekey and closingkey is provided as input for the decryption component

### When

The SignedEncryptedPseudonym and/or keys are invalid or the combination does not match

### Then

No Pseudonym is returned in the response message with corresponding error status

### Description

error flows
-- EP with diversifier - invalid signature

The *auditElement* and *signatureValue* have no static values and will change when new
test data is generated. When new test data is generated by Logius, the corresponding expected
test results are produced as well.


## PTC_4788_1: SignedEncryptedPseudonym with diversifier 1 invalid signed

tags: @bsnkeiddo-4802@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_4788_1.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_4788_1.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_4788_1.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_4788_1.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4788_1.expectations.statusCode|string          }}|
* Expect response body contains "SIGNATURE_VERIFICATION_FAILED"
* Fail if expectations are not met

## PTC_4788_2: SignedEncryptedPseudonym with diversifier 2 invalid signed

tags: @bsnkeiddo-4848@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_4788_1.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_4788_1.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_4788_1.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_4788_1.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4788_1.expectations.statusCode|string          }}|
* Expect response body contains "SIGNATURE_VERIFICATION_FAILED"
* Fail if expectations are not met

## PTC_4788_3: SignedEncryptedPseudonym with diversifier 3 invalid signed

tags: @bsnkeiddo-4849@

* Create OASSignedEncryptedPseudonymRequest 

   |propertyName            |propertyValue                                 |propertyFilter        |
   |------------------------|----------------------------------------------|----------------------|
   |signedEncryptedPseudonym|{{ PTC_4788_1.request.signedEncryptedPseudonym|binary              }}|
   |schemeKeys              |{{ PTC_4788_1.request.schemeKeys              |schemeKeys          }}|
   |serviceProviderKeys     |{{ PTC_4788_1.request.serviceProviderKeys     |serviceProviderKeys }}|
   |targetClosingKey        |{{ PTC_4788_1.request.targetClosingKey        |string              }}|
* Send OASSignedEncryptedPseudonymRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4788_1.expectations.statusCode|string          }}|
* Expect response body contains "SIGNATURE_VERIFICATION_FAILED"
* Fail if expectations are not met
