# Validation and/or decryption SignedEncryptedIdentity fails - Alternate Flow

tags: signed-encrypted-identity, alternate-flow, @bsnkeiddo-4603@

* Load dataset from "/v1/signed-encrypted-identity/alternate_flows.yaml"
* Target default endpoint "/signed-encrypted-identity"

### Given

A SignedEncryptedIdentity, SP-key and schemekey is provided as input for the decryption component

### When

The SignedEncryptedIdentity and/or keys are invalid or the combination does not match

### Then

No decrypted identity (BSN) is returned in the response message with corresponding error status

### Description

Alternate flows:
-- invalid signed EI (signature won't validate)
-- mismatch between key-material and EI:
--- different OINs
--- different schemeKeySetVersion
--- different recipientKeySetVersions
-- input not EI
-- input EI, unknown structure
-- input EI schemeVersion  <> '1'
-- input missing required key

NB: Diagnostic information will be tested in the specific logging story BSNKEIDDO-4533


## PTC_4603_1: Unknown schemeVersion of SignedEncryptedIdentity

tags: @bsnkeiddo-4808@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                |propertyFilter        |
   |-----------------------|---------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_1.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_1.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_1.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_1.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4603_1.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_4603_3: Mismatch between idd schemeKeySetVersion and SignedEncryptedIdentity schemeKeySetVersion

tags: @bsnkeiddo-4809@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                |propertyFilter        |
   |-----------------------|---------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_3.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_3.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_3.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_3.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4603_3.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_4603_4: Mismatch between ipp schemeKeySetVersion and SignedEncryptedIdentity schemeKeySetVersion

tags: @bsnkeiddo-4810@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                |propertyFilter        |
   |-----------------------|---------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_4.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_4.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_4.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_4.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4603_4.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SCHEME_KEY_NOT_FOUND"
* Fail if expectations are not met

## PTC_4603_5: Input is an EncryptedPseudonym instead of SignedEncryptedIdentity

tags: @bsnkeiddo-4811@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                |propertyFilter        |
   |-----------------------|---------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_5.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_5.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_5.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_5.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4603_5.expectations.statusCode|string          }}|
* Expect response body contains "OID_NOT_SUPPORTED"
* Fail if expectations are not met

## PTC_4603_6: Unknown structure of SignedEncryptedIdentity

tags: @bsnkeiddo-4812@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                |propertyFilter        |
   |-----------------------|---------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_6.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_6.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_6.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_6.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4603_6.expectations.statusCode|string          }}|
* Expect response body contains "ASN1_SEQUENCE_DECODER"
* Fail if expectations are not met

## PTC_4603_7: Missing idd-key input

tags: @bsnkeiddo-4813@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                |propertyFilter        |
   |-----------------------|---------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_7.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_7.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_7.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_7.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4603_7.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

## PTC_4603_8: Missing ipp-key input

tags: @bsnkeiddo-4814@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                |propertyFilter        |
   |-----------------------|---------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_8.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_8.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_8.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_8.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4603_8.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SCHEME_KEY_NOT_FOUND"
* Fail if expectations are not met

## PTC_4603_9: Invalid signature of SignedEncryptedIdentity

tags: @bsnkeiddo-4815@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                |propertyFilter        |
   |-----------------------|---------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_9.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_9.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_9.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_9.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                        |expectedValueType |
   |-----------|-------------------------------------|------------------|
   |statusCode |{{ PTC_4603_9.expectations.statusCode|string          }}|
* Expect response body contains "SIGNATURE_VERIFICATION_FAILED"
* Fail if expectations are not met

## PTC_4603_10: Mismatch between idd recipientKeySetVersion  and SignedEncryptedIdentity recipientKeySetVersion

tags: @bsnkeiddo-4816@

* Create OASSignedEncryptedIdentityRequest 

   |propertyName           |propertyValue                                 |propertyFilter        |
   |-----------------------|----------------------------------------------|----------------------|
   |signedEncryptedIdentity|{{ PTC_4603_10.request.signedEncryptedIdentity|binary              }}|
   |schemeKeys             |{{ PTC_4603_10.request.schemeKeys             |schemeKeys          }}|
   |serviceProviderKeys    |{{ PTC_4603_10.request.serviceProviderKeys    |serviceProviderKeys }}|
   |targetClosingKey       |{{ PTC_4603_10.request.targetClosingKey       |string              }}|
* Send OASSignedEncryptedIdentityRequest
* Expect response matches 

   |actualValue|expectedValue                         |expectedValueType |
   |-----------|--------------------------------------|------------------|
   |statusCode |{{ PTC_4603_10.expectations.statusCode|string          }}|
* Expect response body contains "MATCHING_SERVICE_PROVIDER_KEY_REQUIRED"
* Fail if expectations are not met

