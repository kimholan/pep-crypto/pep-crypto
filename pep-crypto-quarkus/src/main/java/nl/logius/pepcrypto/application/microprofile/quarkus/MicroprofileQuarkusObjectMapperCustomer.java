package nl.logius.pepcrypto.application.microprofile.quarkus;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.jackson.ObjectMapperCustomizer;
import nl.logius.pepcrypto.application.microprofile.MicroprofileResteasyJackson2Provider;

import javax.inject.Singleton;

@Singleton
public class MicroprofileQuarkusObjectMapperCustomer
        implements ObjectMapperCustomizer {

    @Override
    public void customize(ObjectMapper objectMapper) {
        MicroprofileResteasyJackson2Provider.customize(objectMapper);
    }

}
